COMP3860: Research Project
Jordan O'Brien (ID: 200771741)


To run the code in a Linux terminal, do the following:


Find all java files under the directory path:  

../third_year_project/src/third_year_project


Enter the following commands:

javac -d . *.java
java third_year_project.JaipurMain



To play the game against an artificial player, follow the instructions within the text interface.
For more information about the main setup and rules of 'Jaipur', please refer to Sections 2.3.1 and 2.3.2 of the report.
For specific details about each selectable AI strategy, please refer to Sections 4.2.3 and 4.3.5 of the report.