package third_year_project;

import java.util.ArrayList;
import java.util.Collections;

public class MainDeck {
	
	private ArrayList<String> cards;
	
	//constructor
	public MainDeck() {
		
		cards = new ArrayList<String>();
		
		//adding all relevant card names to the deck, in the correct quantities
		for (int i = 0; i < 11; i++) {

            if (i < 6) {
            	cards.add("D"); //diamonds
            	cards.add("G"); //gold
            	cards.add("Sil"); //silver
            }

            if (i < 8) {
            	cards.add("Clo"); //cloth
            	cards.add("Spi"); //spice
            }

            if (i < 10) {
            	cards.add("L"); //leather
            }

            if (i < 11) {
            	cards.add("Cam"); //camels
            }
        }
		
		//cards initialised for game setup
		remove_three_camels();
		shuffle_cards();
	}
		
	//method to output all cards in the deck (not to be used in final game)
	public void print_cards() {
		
		for(int i = 0; i < cards.size(); i++) {
			System.out.print(cards.get(i) + " ");
		}
		System.out.print("\n");
	}
	
	//method to shuffle cards
	public void shuffle_cards() {
		
		Collections.shuffle(cards);
	}
	
	//method to evaluate how many cards are left in the main deck
	public int get_remaining_card_num() {
		
		int cards_left = cards.size();
		return cards_left;
	}
	
	//method to evaluate if the main deck is empty
	public boolean is_deck_empty() {
		
		if (get_remaining_card_num() == 0) {
			return true;
		}
		
		else {
			return false;
		}
	}
	
	//method to take one card from the top of the pile
	public String take_card() {
		
		String chosen_card;

		//to avoid index errors at the end of a round
	    if (is_deck_empty()) {
	    	
	    	chosen_card = "N/A";
			return chosen_card;
	    }
	    
	    else {
			chosen_card = cards.get(0);

			cards.remove(0);
			return chosen_card;
	    }
	}
	
	//method to remove three camel cards from main deck (needed for initial game setup)
	public void remove_three_camels() {
		
		if (get_remaining_card_num() != 55) {
			throw new IllegalArgumentException("ERROR: This method should only be used when all"
					+ "cards are present within the deck.");
		}
		
		int camels_found = 0;
		
		//loops over the main deck and removes first three camels
		for (int i = 0; i < cards.size(); i++) {
			
			if (cards.get(i).equals("Cam")) {
				cards.remove(i);
				camels_found++;
				i--;
			}
			
			if (camels_found == 3) {
				break;
			}
		}
	}	
}