package third_year_project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.Random;

public class ComputerPlayer extends Player {
	
	private int opposing_player_goods_total, opposing_player_camel_total, strategy_num;
	
	private double selected_hand_k1, selected_hand_k2, selected_hand_k3, 
					selected_market_k1, selected_market_k2, selected_market_k3;

	public ComputerPlayer(String name, int strategy_num) {
				
		super(name);
		
		opposing_player_goods_total = 0;
		opposing_player_camel_total = 0;
		
		if (strategy_num < 1 || strategy_num > 6) {
			throw new IllegalArgumentException("ERROR: Strategy number can only be 1, 2, 3, 4, 5 or 6.");
		}
		
		else {
			this.strategy_num = strategy_num;
		}	
		
		//hand heuristic default values
		selected_hand_k1 = 1;
		selected_hand_k2 = 1;
		selected_hand_k3 = 1;
		
		//market heuristic default values
		selected_market_k1 = 1;
		selected_market_k2 = 1;
		selected_market_k3 = 1;
	}
	
	//methods to retrieve number of goods and camel cards believed to be held by other player
	
	public int get_opposing_player_goods_total() {
		
		return opposing_player_goods_total;
	}
	
	public int get_opposing_player_camel_total() {
		
		return opposing_player_camel_total;
	}
	
	//method to obtain strategy number of player
	public int get_strategy_num() {
		
		return strategy_num;
	}

	//methods to append number of goods and camel cards believed to be held by other player

	public void set_opposing_player_goods_total(int new_goods_total) {
		
		this.opposing_player_goods_total = new_goods_total;
	}
	
	public void set_opposing_player_camel_total(int new_camel_total) {
		
		this.opposing_player_camel_total = new_camel_total;
	}

	//methods to append the parameters in the main state value heuristic
	
	public void set_selected_hand_k1(double new_hand_k1) {
		
		this.selected_hand_k1 = new_hand_k1;
	}
	
	public void set_selected_hand_k2(double new_hand_k2) {
		
		this.selected_hand_k2 = new_hand_k2;
	}

	public void set_selected_hand_k3(double new_hand_k3) {
		
		this.selected_hand_k3 = new_hand_k3;
	}
	
	public void set_selected_market_k1(double new_market_k1) {
		
		this.selected_market_k1 = new_market_k1;
	}

	public void set_selected_market_k2(double new_market_k2) {
		
		this.selected_market_k2 = new_market_k2;
	}

	public void set_selected_market_k3(double new_market_k3) {
		
		this.selected_market_k3 = new_market_k3;
	}

	//method to obtain all possible moves the player can take, given the current state of the round
	public ArrayList<ArrayList<String>> get_all_possible_moves(ArrayList<String> current_market, ArrayList<String> current_goods_hand, 
			ArrayList<String> current_camels) {
		
		//initialise collection of possible actions to take, and action container
		ArrayList<ArrayList<String>> all_possible_actions = new ArrayList<ArrayList<String>>();
		ArrayList<String> possible_action = new ArrayList<String>();
				
		//loop over market to find camels, if one is found, take_camels action 
		//valid and therefore added
		for (int i = 0; i < current_market.size(); i++) {
			
			if (current_market.get(i).equals("Cam")) {
				
				possible_action.add("take_camels");
				all_possible_actions.add(possible_action);
				possible_action = new ArrayList<String>();
				
				break;
			}
		}
		
		//loop over market to find goods cards, if one is found, take_one_good action
		//valid and therefore added
		for (int i = 0; i < current_market.size(); i++) {
			
			if (!current_market.get(i).equals("Cam") && current_goods_hand.size() < 7) {
				
				possible_action.add("take_one_good");
				possible_action.add(current_market.get(i));
				
				//avoids repetition by not adding existing actions to the ArrayList
				if (!all_possible_actions.contains(possible_action)) {
					all_possible_actions.add(possible_action);
				}
				
				possible_action = new ArrayList<String>();
			}
		}

		//initialising number of goods in current hand
		int diamond_num = 0, gold_num = 0, silver_num = 0, cloth_num = 0, spice_num = 0, leather_num = 0;
		
		//count number of goods in current hand to sell
		for (int i = 0; i < current_goods_hand.size(); i++) {
					
			if (current_goods_hand.get(i).equals("D")) {
				diamond_num += 1;
			}
			
			if (current_goods_hand.get(i).equals("G")) {
				gold_num += 1;
			}

			if (current_goods_hand.get(i).equals("Sil")) {
				silver_num += 1;
			}

			if (current_goods_hand.get(i).equals("Clo")) {
				cloth_num += 1;
			}

			if (current_goods_hand.get(i).equals("Spi")) {
				spice_num += 1;
			}
			
			if (current_goods_hand.get(i).equals("L")) {
				leather_num += 1;
			}
		}
		
		//looping over current goods hand to find all possible sales that can be made
		//with the current quantities
		
		while (diamond_num > 1) { //diamond sales have to have at least two to sell
			
			possible_action.add("sell_cards");
			
			for (int i = 0; i < diamond_num; i++) {
				possible_action.add("D");
			}
			
			possible_action.add("--"); //this signifies the end of the sale
			all_possible_actions.add(possible_action);
			possible_action = new ArrayList<String>();
			diamond_num -= 1; //subtract 1 each time to find all possible combinations of sales
		}
		
		while (gold_num > 1) { //gold sales have to have at least two to sell
			
			possible_action.add("sell_cards");
			
			for (int i = 0; i < gold_num; i++) {
				possible_action.add("G");
			}
			
			possible_action.add("--"); //this signifies the end of the sale
			all_possible_actions.add(possible_action);
			possible_action = new ArrayList<String>();
			gold_num -= 1; //subtract 1 each time to find all possible combinations of sales
		}
		
		while (silver_num > 1) { //silver sales have to have at least two to sell
			
			possible_action.add("sell_cards");
			
			for (int i = 0; i < silver_num; i++) {
				possible_action.add("Sil"); 
			}
			
			possible_action.add("--"); //this signifies the end of the sale
			all_possible_actions.add(possible_action);
			possible_action = new ArrayList<String>(); 
			silver_num -= 1; //subtract 1 each time to find all possible combinations of sales
		}

		while (cloth_num > 0) { //cloth sales have to only have one to sell
			
			possible_action.add("sell_cards");
			
			for (int i = 0; i < cloth_num; i++) {
				possible_action.add("Clo");
			}
			
			possible_action.add("--"); //this signifies the end of the sale
			all_possible_actions.add(possible_action);
			possible_action = new ArrayList<String>();
			cloth_num -= 1; //subtract 1 each time to find all possible combinations of sales
		}
		
		while (spice_num > 0) { //spice sales have to only have one to sell
			
			possible_action.add("sell_cards");
			
			for (int i = 0; i < spice_num; i++) {
				possible_action.add("Spi");
			}
			
			possible_action.add("--"); //this signifies the end of the sale
			all_possible_actions.add(possible_action);
			possible_action = new ArrayList<String>();
			spice_num -= 1; //subtract 1 each time to find all possible combinations of sales
		}

		while (leather_num > 0) { //leather sales have to only have one to sell
			
			possible_action.add("sell_cards");
			
			for (int i = 0; i < leather_num; i++) {
				possible_action.add("L");
			}
			
			possible_action.add("--"); //this signifies the end of the sale
			all_possible_actions.add(possible_action);
			possible_action = new ArrayList<String>();
			leather_num -= 1; //subtract 1 each time to find all possible combinations of sales
		}
		
		
		//looping over current goods/camel cards and current market to find all possible 
		//trades that can be made		

		//placing all current goods and camel cards in a single container
		ArrayList<String> all_current_cards = new ArrayList<String>();
		all_current_cards.addAll(current_goods_hand);
		all_current_cards.addAll(current_camels);
		
		//initialising containers for all possible combinations of cards in current
		//hand and market place
		ArrayList<ArrayList<String>> all_current_cards_combinations = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> all_market_combinations = new ArrayList<ArrayList<String>>();
		ArrayList<String> possible_grouping = new ArrayList<String>();

		//looping over all current goods and camel cards to find all combinations
		for (int i = 0; i < all_current_cards.size(); i++) {
						
			for (int j = 0; j < all_current_cards.size(); j++) {
				
				//find all combinations in groups of two
				if (i != j) {
					
					possible_grouping.add(all_current_cards.get(i));
					possible_grouping.add(all_current_cards.get(j));
					
					all_current_cards_combinations.add(possible_grouping);
					possible_grouping = new ArrayList<String>();
				}
				
				//find all combinations in groups of three
				if ((i != j) && (i != j + 1) && (j != all_current_cards.size() - 1)) {
					
					possible_grouping.add(all_current_cards.get(i));
					possible_grouping.add(all_current_cards.get(j));
					possible_grouping.add(all_current_cards.get(j+1));

					all_current_cards_combinations.add(possible_grouping);
					possible_grouping = new ArrayList<String>();
				}
				
				//find all combinations in groups of four
				if ((i != j) && (i != j + 1) && (i != j + 2) && (j != all_current_cards.size() - 1) && 
						(j != all_current_cards.size() - 2)) {
					
					possible_grouping.add(all_current_cards.get(i));
					possible_grouping.add(all_current_cards.get(j));
					possible_grouping.add(all_current_cards.get(j+1));
					possible_grouping.add(all_current_cards.get(j+2));

					all_current_cards_combinations.add(possible_grouping);
					possible_grouping = new ArrayList<String>();
				}
				
				//find all combinations in groups of five
				if ((i != j) && (i != j + 1) && (i != j + 2) && (i != j + 3) && (j != all_current_cards.size() - 1) && 
						(j != all_current_cards.size() - 2) && (j != all_current_cards.size() - 3)) {
					
					possible_grouping.add(all_current_cards.get(i));
					possible_grouping.add(all_current_cards.get(j));
					possible_grouping.add(all_current_cards.get(j+1));
					possible_grouping.add(all_current_cards.get(j+2));
					possible_grouping.add(all_current_cards.get(j+3));

					all_current_cards_combinations.add(possible_grouping);
					possible_grouping = new ArrayList<String>();
				}
			}
		}
		
		//card combinations need to be sorted in order to delete repetition
		ArrayList<ArrayList<String>> all_current_cards_combinations_sorted = new ArrayList<ArrayList<String>>();
		
		//loop over all_current_cards_combinations list
		for (int i = 0; i < all_current_cards_combinations.size(); i++) {
			
			possible_grouping = new ArrayList<String>();
			possible_grouping.addAll(all_current_cards_combinations.get(i));
			Collections.sort(possible_grouping); // sort each possible combination
			all_current_cards_combinations_sorted.add(possible_grouping);
		}
		
		//add all combinations to a set to remove any duplication
		Set<ArrayList<String>> all_current_cards_combinations_temp = new HashSet<ArrayList<String>>();

		all_current_cards_combinations_temp.addAll(all_current_cards_combinations_sorted);
		all_current_cards_combinations.clear(); //clear old list to add sorted combinations
		all_current_cards_combinations.addAll(all_current_cards_combinations_temp);
		
		
		//re-initialise array for finding all possible groupings with market cards
		possible_grouping = new ArrayList<String>();

		//looping over all market cards to find all combinations
		for (int i = 0; i < current_market.size(); i++) {
						
			for (int j = 0; j < current_market.size(); j++) {
				
				//find all combinations in groups of two
				if (i != j) {
					possible_grouping.add(current_market.get(i));
					possible_grouping.add(current_market.get(j));
					
					all_market_combinations.add(possible_grouping);
					possible_grouping = new ArrayList<String>();
				}
				
				//find all combinations in groups of three
				if ((i != j) && (i != j + 1) && (j != current_market.size() - 1)) {
					
					possible_grouping.add(current_market.get(i));
					possible_grouping.add(current_market.get(j));
					possible_grouping.add(current_market.get(j+1));

					all_market_combinations.add(possible_grouping);
					possible_grouping = new ArrayList<String>();
				}
				
				//find all combinations in groups of four
				if ((i != j) && (i != j + 1) && (i != j + 2) && (j != current_market.size() - 1) && 
						(j != current_market.size() - 2)) {
					
					possible_grouping.add(current_market.get(i));
					possible_grouping.add(current_market.get(j));
					possible_grouping.add(current_market.get(j+1));
					possible_grouping.add(current_market.get(j+2));

					all_market_combinations.add(possible_grouping);
					possible_grouping = new ArrayList<String>();
				}
				
				//find all combinations in groups of five
				if ((i != j) && (i != j + 1) && (i != j + 2) && (i != j + 3) && (j != current_market.size() - 1) && 
						(j != current_market.size() - 2) && (j != current_market.size() - 3)) {
					
					possible_grouping.add(current_market.get(i));
					possible_grouping.add(current_market.get(j));
					possible_grouping.add(current_market.get(j+1));
					possible_grouping.add(current_market.get(j+2));
					possible_grouping.add(current_market.get(j+3));

					all_market_combinations.add(possible_grouping);
					possible_grouping = new ArrayList<String>();
				}
			}
		}
		
		ArrayList<ArrayList<String>> all_market_combinations_sorted = new ArrayList<ArrayList<String>>();
		
		//loop over all_market_combinations list
		for (int i = 0; i < all_market_combinations.size(); i++) {
			possible_grouping = new ArrayList<String>();
			possible_grouping.addAll(all_market_combinations.get(i));
			Collections.sort(possible_grouping); //sort each possible combination
			all_market_combinations_sorted.add(possible_grouping);
		}
		
		//add all combinations to a set to remove any duplication
		Set<ArrayList<String>> all_market_combinations_temp = new HashSet<ArrayList<String>>();

		all_market_combinations_temp.addAll(all_market_combinations_sorted);
		all_market_combinations.clear(); //clear old list to add sorted combinations
		all_market_combinations.addAll(all_market_combinations_temp);

		
		//boolean to detect when there is the same card within the player's current
		//hand and the current market
		boolean duplicate_found;
		
		//loop over all possible combinations in the current hand and the market
		//to find valid trades
		for (int i = 0; i < all_market_combinations.size(); i++) {
			
			for (int j = 0; j < all_current_cards_combinations.size(); j++) {
				
				//only find combinations that are of the same size and where the market cards
				//are not camels, as these will be valid trades
				if (all_market_combinations.get(i).size() == all_current_cards_combinations.get(j).size() && 
						!all_market_combinations.get(i).contains("Cam")) {
					
					duplicate_found = false;
					
					//looping over combination pairs to check if they contain any of
					//the same cards
					for (int k = 0; k < all_market_combinations.get(i).size(); k++) {
						
						for (int m = 0; m < all_current_cards_combinations.get(j).size(); m++) {
							
							//if combination pair contains the same card, trade is invalid, and the
							//duplicate flag is raised
							if (all_market_combinations.get(i).get(k) == all_current_cards_combinations.get(j).get(m)) {
								duplicate_found = true;
							}
						}
					}
					
					int camels_found = 0, hand_limit = 7;
					
					//counting number of camels within current combination
					for (int k = 0; k < all_current_cards_combinations.get(j).size(); k++) {
						if (all_current_cards_combinations.get(j).get(k).equals("Cam")) {
							camels_found++;
						}
					}
					
					//changes the limit of each new hand, given the amount of camels
					//wanted to be traded with
					if (camels_found == 1) { 
						hand_limit = 6; 
					}

					if (camels_found == 2) { 
						hand_limit = 5; 
					}

					if (camels_found == 3) { 
						hand_limit = 4; 
					}

					if (camels_found == 4) { 
						hand_limit = 3; 
					}

					if (camels_found == 5) {
						hand_limit = 2; 
					}

					//if no duplicate is found, it means a trade action is valid
					if (!duplicate_found && (hand_limit >= current_goods_hand.size())) {
						
						//add possible trade_cards action
						possible_action.add("trade_cards");
						
						//add goods/camel cards for trading
						for (int k = 0; k < all_current_cards_combinations.get(j).size(); k++) {
							possible_action.add(all_current_cards_combinations.get(j).get(k));
						}
						possible_action.add("--"); // this signifies the end of goods/camel cards
						
						//add market cards for trading
						for (int k = 0; k < all_market_combinations.get(i).size(); k++) {
							possible_action.add(all_market_combinations.get(i).get(k));
						}
						possible_action.add("--"); // this signifies the end of market cards

						all_possible_actions.add(possible_action);
						possible_action = new ArrayList<String>();
					}
				}
			}
		} 
				
		return all_possible_actions;
	}
	
	//method to return all known cards that are not currently in the deck
	public ArrayList<String> get_all_known_cards(ArrayList<String> current_goods_hand, ArrayList<String> current_camels_hand,
			ArrayList<String> current_market, ArrayList<String> sold_cards) {

		ArrayList<String> all_known_cards = new ArrayList<String>();
		
		//adding cards from the player's current hand (goods and camels), the 
		//current market and the cards which have been sold
		all_known_cards.addAll(current_goods_hand);
		all_known_cards.addAll(current_camels_hand);
		all_known_cards.addAll(current_market);
		all_known_cards.addAll(sold_cards);
		
		return all_known_cards;
	}
	
	//method to return the quantities of each card of a certain type from the deck
	public ArrayList<Double> get_remaining_card_quantities(ArrayList<String> all_known_cards) {
		
		ArrayList<Double> remaining_card_quantities = new ArrayList<Double>();
		
		//adding initial quantities of each card type
		remaining_card_quantities.add((double) 6); //diamond
		remaining_card_quantities.add((double) 6); //gold
		remaining_card_quantities.add((double) 6); //silver
		remaining_card_quantities.add((double) 8); //cloth
		remaining_card_quantities.add((double) 8); //spice
		remaining_card_quantities.add((double) 10); //leather
		remaining_card_quantities.add((double) 11); //camels

		double new_num;
		
		//loop over all known cards and subtract 1 whenever one is found
		for (int i = 0; i < all_known_cards.size(); i++) {
			
			//find if known cards equals a certain card type
			
			//find all diamond cards
			if (all_known_cards.get(i).equals("D")) {
				new_num = remaining_card_quantities.get(0) - 1; //subtract quantity by 1
				remaining_card_quantities.set(0, new_num); //add new quantity to list
			}
			
			//find all gold cards
			if (all_known_cards.get(i).equals("G")) {
				new_num = remaining_card_quantities.get(1) - 1; //subtract quantity by 1 
				remaining_card_quantities.set(1, new_num); //add new quantity to list
			}
			
			//find all silver cards
			if (all_known_cards.get(i).equals("Sil")) {
				new_num = remaining_card_quantities.get(2) - 1; //subtract quantity by 1
				remaining_card_quantities.set(2, new_num); //add new quantity to list
			}

			//find all cloth cards
			if (all_known_cards.get(i).equals("Clo")) {
				new_num = remaining_card_quantities.get(3) - 1; //subtract quantity by 1
				remaining_card_quantities.set(3, new_num); //add new quantity to list
			}
			
			//find all spice cards
			if (all_known_cards.get(i).equals("Spi")) {
				new_num = remaining_card_quantities.get(4) - 1; //subtract quantity by 1
				remaining_card_quantities.set(4, new_num); //add new quantity to list
			}
			
			//find all leather cards			
			if (all_known_cards.get(i).equals("L")) {
				new_num = remaining_card_quantities.get(5) - 1; //subtract quantity by 1
				remaining_card_quantities.set(5, new_num); //add new quantity to list
			}

			//find all camel cards
			if (all_known_cards.get(i).equals("Cam")) {
				new_num = remaining_card_quantities.get(6) - 1; //subtract quantity by 1
				remaining_card_quantities.set(6, new_num); //add new quantity to list
			}
		}
		
		return remaining_card_quantities;
	}
	
	//method to return the probabilities of picking each card of a certain type from the deck
	public ArrayList<Double> get_remaining_card_probabilities(ArrayList<Double> remaining_card_quantities) {
		
		//loop over all remaining card quantities to find total amount of remaining cards
		//(in deck, or other player's hand)
		
		int remaining_card_total = 0;
		
		for (int i = 0; i < remaining_card_quantities.size(); i++) {
			remaining_card_total += remaining_card_quantities.get(i);
		}
		
		//calculating probabilities of taken each card of a certain type
		double diamond_probability = remaining_card_quantities.get(0) / remaining_card_total;
		double gold_probability = remaining_card_quantities.get(1) / remaining_card_total;
		double silver_probability = remaining_card_quantities.get(2) / remaining_card_total;
		double cloth_probability = remaining_card_quantities.get(3) / remaining_card_total;
		double spice_probability = remaining_card_quantities.get(4) / remaining_card_total;
		double leather_probability = remaining_card_quantities.get(5) / remaining_card_total;
		double camel_probability = remaining_card_quantities.get(6) / remaining_card_total;

		ArrayList<Double> remaining_card_probabilities = new ArrayList<Double>();
		
		//adding card probabilities to ArrayList to return
		remaining_card_probabilities.add(diamond_probability);
		remaining_card_probabilities.add(gold_probability);
		remaining_card_probabilities.add(silver_probability);
		remaining_card_probabilities.add(cloth_probability);
		remaining_card_probabilities.add(spice_probability);
		remaining_card_probabilities.add(leather_probability);
		remaining_card_probabilities.add(camel_probability);
		
		return remaining_card_probabilities;
	}
	
	//method to return array off all quantities of each card type in the current market or
	//the player's current hand
	public ArrayList<Double> get_current_card_quantities(ArrayList<String> current_cards) {
		
		ArrayList<Double> current_card_quantities = new ArrayList<Double>();
		
		double new_num;
		
		//initialise current list quantities all to 0
		for(int i = 0; i < 7; i++) {
			current_card_quantities.add((double) 0);
		}
		
		//loop over current market cards and add 1 whenever a certain card is found
		for (int i = 0; i < current_cards.size(); i++) {
			
			//if certain card types are found, their quantities are added by 1
			
			//add diamond cards
			if (current_cards.get(i).equals("D")) {
				new_num = current_card_quantities.get(0) + 1; //add quantity by 1
				current_card_quantities.set(0, new_num); //add new quantity to list
			}
			
			//add gold cards
			if (current_cards.get(i).equals("G")) {
				new_num = current_card_quantities.get(1) + 1; //add quantity by 1
				current_card_quantities.set(1, new_num); //add new quantity to list
			}
			
			//add silver cards
			if (current_cards.get(i).equals("Sil")) {
				new_num = current_card_quantities.get(2) + 1; //add quantity by 1
				current_card_quantities.set(2, new_num); //add new quantity to list
			}

			//add cloth cards
			if (current_cards.get(i).equals("Clo")) {
				new_num = current_card_quantities.get(3) + 1; //add quantity by 1
				current_card_quantities.set(3, new_num); //add new quantity to list
			}
			
			//add spice cards
			if (current_cards.get(i).equals("Spi")) {
				new_num = current_card_quantities.get(4) + 1; //add quantity by 1
				current_card_quantities.set(4, new_num); //add new quantity to list
			}
			
			//add leather cards			
			if (current_cards.get(i).equals("L")) {
				new_num = current_card_quantities.get(5) + 1; //add quantity by 1
				current_card_quantities.set(5, new_num); //add new quantity to list
			}
			
			//add camel cards (for market quantities)	
			if (current_cards.get(i).equals("Cam")) {
				new_num = current_card_quantities.get(6) + 1; //add quantity by 1
				current_card_quantities.set(6, new_num); //add new quantity to list
			}
		}
		
		return current_card_quantities;
	}	
	
	//method to get the next market quantities in the next state (not including new card probabilities)
	public ArrayList<Double> get_next_market_quantities(ArrayList<Double> current_market_quantities, ArrayList<String> next_action) {
		
		//ArrayLists for cards taken and received by the market place
		ArrayList<String> cards_taken, cards_recieved;
		cards_taken = new ArrayList<String>();
		cards_recieved = new ArrayList<String>();
		
		//the action sell_cards does not affect the market
		if (next_action.get(0).equals("sell_cards")) {
			return current_market_quantities;
		}
		
		else {
			
			//checks if the take_camels actions has been chosen
			if (next_action.get(0).equals("take_camels")) {
				
				//add all camel cards to cards_taken list
				for (int i = 0; i < current_market_quantities.get(6); i++) {
					cards_taken.add("Cam");
				}
			}
			
			//checks if the take_one_good actions has been chosen
			if (next_action.get(0).equals("take_one_good")) {
				
				//add goods card to cards_taken list
				cards_taken.add(next_action.get(1));
			}
			
			//checks if the trade_cards actions has been chosen
			if (next_action.get(0).equals("trade_cards")) {
				
				int index_num = 1;
				
				//find all cards that are received from the player and add
				//to cards_recieved list
				while (!next_action.get(index_num).equals("--")) {
					
					cards_recieved.add(next_action.get(index_num));
					index_num++;
				}
				
				index_num++;
				
				//find all cards that are taken from the market and add
				//to cards_taken list
				while (!next_action.get(index_num).equals("--")) {
					
					cards_taken.add(next_action.get(index_num));
					index_num++;
				}
			}
		}
		
		ArrayList<Double> next_market_quantities = new ArrayList<Double>();
		next_market_quantities.addAll(current_market_quantities);
	
		double new_num;
		
		//loop over cards taken and subtract 1 whenever a certain card is found
		for (int i = 0; i < cards_taken.size(); i++) {
			
			//if a certain card types are found, their quantities are subtracted by 1
			
			if (cards_taken.get(i).equals("D")) {
				new_num = next_market_quantities.get(0) - 1; //subtract quantity by 1
				next_market_quantities.set(0, new_num); //add new quantity to list
			}
			
			//subtract gold cards
			if (cards_taken.get(i).equals("G")) {
				new_num = next_market_quantities.get(1) - 1; //subtract quantity by 1
				next_market_quantities.set(1, new_num); //add new quantity to list
			}
			
			//subtract silver cards
			if (cards_taken.get(i).equals("Sil")) {
				new_num = next_market_quantities.get(2) - 1; //subtract quantity by 1
				next_market_quantities.set(2, new_num); //add new quantity to list
			}

			//subtract cloth cards
			if (cards_taken.get(i).equals("Clo")) {
				new_num = next_market_quantities.get(3) - 1; //subtract quantity by 1
				next_market_quantities.set(3, new_num); //add new quantity to list
			}
			
			//subtract spice cards
			if (cards_taken.get(i).equals("Spi")) {
				new_num = next_market_quantities.get(4) - 1; //subtract quantity by 1
				next_market_quantities.set(4, new_num); //add new quantity to list
			}
			
			//subtract leather cards			
			if (cards_taken.get(i).equals("L")) {
				new_num = next_market_quantities.get(5) - 1; //subtract quantity by 1
				next_market_quantities.set(5, new_num); //add new quantity to list
			}
			
			//subtract camel cards			
			if (cards_taken.get(i).equals("Cam")) {
				new_num = next_market_quantities.get(6) - 1; //subtract quantity by 1
				next_market_quantities.set(6, new_num); //add new quantity to list
			}
		}

		//loop over cards received and add 1 whenever a certain card is found
		for (int i = 0; i < cards_recieved.size(); i++) {
			
			//if a certain card types are found, their quantities are added by 1
			
			//add diamond cards
			if (cards_recieved.get(i).equals("D")) {
				new_num = next_market_quantities.get(0) + 1; //add quantity by 1
				next_market_quantities.set(0, new_num); //add new quantity to list
			}
			
			//add gold cards
			if (cards_recieved.get(i).equals("G")) {
				new_num = next_market_quantities.get(1) + 1; //add quantity by 1
				next_market_quantities.set(1, new_num); //add new quantity to list
			}
			
			//add silver cards
			if (cards_recieved.get(i).equals("Sil")) {
				new_num = next_market_quantities.get(2) + 1; //add quantity by 1
				next_market_quantities.set(2, new_num); //add new quantity to list
			}

			//add cloth cards
			if (cards_recieved.get(i).equals("Clo")) {
				new_num = next_market_quantities.get(3) + 1; //add quantity by 1
				next_market_quantities.set(3, new_num); //add new quantity to list
			}
			
			//add spice cards
			if (cards_recieved.get(i).equals("Spi")) {
				new_num = next_market_quantities.get(4) + 1; //add quantity by 1
				next_market_quantities.set(4, new_num); //add new quantity to list
			}
			
			//add leather cards			
			if (cards_recieved.get(i).equals("L")) {
				new_num = next_market_quantities.get(5) + 1; //add quantity by 1
				next_market_quantities.set(5, new_num); //add new quantity to list
			}
			
			//add camel cards			
			if (cards_recieved.get(i).equals("Cam")) {
				new_num = next_market_quantities.get(6) + 1; //add quantity by 1
				next_market_quantities.set(6, new_num); //add new quantity to list
			}
		}
		
		return next_market_quantities;
	}

	//method to get the next hand quantities in the next state (not including new card probabilities)
	public ArrayList<Double> get_next_hand_quantities(ArrayList<Double> current_goods_quantities, ArrayList<Double> current_market_quantities, 
			ArrayList<String> next_action) {
		
		//ArrayLists for cards taken and received by the goods hand
		ArrayList<String> cards_taken, cards_recieved;
		cards_taken = new ArrayList<String>();
		cards_recieved = new ArrayList<String>();
		
		//the action take_camels does not affect the goods hand
		if (next_action.get(0).equals("take_camels")) {
			
			//obtain amount of camels in the market place
			double camel_total = current_market_quantities.get(6);

			for (int i = 0; i < camel_total; i++) {
				
				//add camel cards to cards_recieved list
				cards_recieved.add("Cam");
			}
		}
					
		//checks if the take_one_good actions has been chosen
		if (next_action.get(0).equals("take_one_good")) {
		
			//add goods card to cards_recieved list
			cards_recieved.add(next_action.get(1));
		}
			
		//checks if the trade_cards actions has been chosen
		if (next_action.get(0).equals("trade_cards")) {
			
			int index_num = 1;
			
			//find all cards that are taken from the market and add
			//to cards_taken list
			while (!next_action.get(index_num).equals("--")) {
				
				cards_taken.add(next_action.get(index_num));
				index_num++;
			}
				
			index_num++;
				
			//find all cards that are received from the market and add
			//to cards_recieved list
			while (!next_action.get(index_num).equals("--")) {
					
				cards_recieved.add(next_action.get(index_num));
				index_num++;
			}
		}
			
		//checks if the sell_cards actions has been chosen
		if (next_action.get(0).equals("sell_cards")) {
				
			int index_num = 1;
				
			//find all cards that are taken from the player's hand and add
			//to cards_taken list
			while (!next_action.get(index_num).equals("--")) {
					
				cards_taken.add(next_action.get(index_num));
				index_num++;
			}
		}
	
		ArrayList<Double> next_goods_quantities = new ArrayList<Double>();
		next_goods_quantities.addAll(current_goods_quantities);
		
		double new_num;
			
		//loop over cards taken and subtract 1 whenever a certain card is found
		for (int i = 0; i < cards_taken.size(); i++) {
				
			//if a certain card types are found, their quantities are subtracted by 1
			
			//subtract diamond cards
			if (cards_taken.get(i).equals("D")) {
				new_num = next_goods_quantities.get(0) - 1; //subtract quantity by 1
				next_goods_quantities.set(0, new_num); //add new quantity to list
			}
				
			//subtract gold cards
			if (cards_taken.get(i).equals("G")) {
				new_num = next_goods_quantities.get(1) - 1; //subtract quantity by 1
				next_goods_quantities.set(1, new_num); //add new quantity to list
			}
				
			//subtract silver cards
			if (cards_taken.get(i).equals("Sil")) {
				new_num = next_goods_quantities.get(2) - 1; //subtract quantity by 1
				next_goods_quantities.set(2, new_num); //add new quantity to list
			}

			//subtract cloth cards
			if (cards_taken.get(i).equals("Clo")) {
				new_num = next_goods_quantities.get(3) - 1; //subtract quantity by 1
				next_goods_quantities.set(3, new_num); //add new quantity to list
			}
				
			//subtract spice cards
			if (cards_taken.get(i).equals("Spi")) {
				new_num = next_goods_quantities.get(4) - 1; //subtract quantity by 1
				next_goods_quantities.set(4, new_num); //add new quantity to list
			}
				
			//subtract leather cards			
			if (cards_taken.get(i).equals("L")) {
				new_num = next_goods_quantities.get(5) - 1; //subtract quantity by 1
				next_goods_quantities.set(5, new_num); //add new quantity to list
			}
			
			//subtract camel cards			
			if (cards_taken.get(i).equals("Cam")) {
				new_num = next_goods_quantities.get(6) - 1; //subtract quantity by 1
				next_goods_quantities.set(6, new_num); //add new quantity to list
			}
		}

		//loop over cards recieved and add 1 whenever a certain card is found
		for (int i = 0; i < cards_recieved.size(); i++) {
				
			//if a certain card types are found, their quantities are added by 1
			
			//add diamond cards
			if (cards_recieved.get(i).equals("D")) {
				new_num = next_goods_quantities.get(0) + 1; //add quantity by 1
				next_goods_quantities.set(0, new_num); //add new quantity to list
			}
				
			//add gold cards
			if (cards_recieved.get(i).equals("G")) {
				new_num = next_goods_quantities.get(1) + 1; //add quantity by 1
				next_goods_quantities.set(1, new_num); //add new quantity to list
			}
				
			//add silver cards
			if (cards_recieved.get(i).equals("Sil")) {
				new_num = next_goods_quantities.get(2) + 1; //add quantity by 1
				next_goods_quantities.set(2, new_num); //add new quantity to list
			}

			//add cloth cards
			if (cards_recieved.get(i).equals("Clo")) {
				new_num = next_goods_quantities.get(3) + 1; //add quantity by 1
				next_goods_quantities.set(3, new_num); //add new quantity to list
			}
				
			//add spice cards
			if (cards_recieved.get(i).equals("Spi")) {
				new_num = next_goods_quantities.get(4) + 1; //add quantity by 1
				next_goods_quantities.set(4, new_num); //add new quantity to list
			}
			
			//add leather cards			
			if (cards_recieved.get(i).equals("L")) {
				new_num = next_goods_quantities.get(5) + 1; //add quantity by 1
				next_goods_quantities.set(5, new_num); //add new quantity to list
			}
			
			//add camel cards			
			if (cards_recieved.get(i).equals("Cam")) {
				new_num = next_goods_quantities.get(6) + 1; //add quantity by 1
				next_goods_quantities.set(6, new_num); //add new quantity to list
			}
		}
			
		return next_goods_quantities;
	}


	//method which finds how many cards have been taken (not traded) from the market, thus needed
	//for determining the amount of cards that need to be taken from the deck
	public int get_total_market_cards_taken(ArrayList<String> current_market, ArrayList<String> next_action) {
		
		//checks if the action is take_camels
		if (next_action.get(0).equals("take_camels")) {
			
			int total_camels = 0;
			
			//loops over market and determines how many camels are going to
			//be taken after the action has taken place
			for (int i = 0; i < current_market.size(); i++) {
				
				if (current_market.get(i).equals("Cam")) {
					total_camels++;
				}
			}
			
			return total_camels;
		}
		
		//checks if the action is take_one_good
		if (next_action.get(0).equals("take_one_good")) {
			
			//only one card needs to be taken from the deck
			return 1;
		}
		
		//the actions trade_cards and sell_cards do not require new cards
		//from the deck once the action has taken place
		else {
			return 0;
		}
	}
	
	//method to add the new card probabilities (from the deck) for next market card quantities in the next state
	public ArrayList<Double> get_next_market_quantities_with_prob(ArrayList<Double> current_market_quantities, ArrayList<String> next_action, 
			ArrayList<String> current_market, ArrayList<String> current_goods_hand, ArrayList<String> current_camels_hand, ArrayList<String> sold_cards) {
				
		//find how many new cards are to be added
		int new_card_total = this.get_total_market_cards_taken(current_market, next_action);
		
		ArrayList<Double> next_market_quantities = new ArrayList<Double>();
		double card_type_quantity;
		
		//loops over current market quantities and adds them to a new list
		//and converts them to double so probabilities can be incorporated
		for (int i = 0; i < current_market_quantities.size(); i++) {
			
			card_type_quantity = current_market_quantities.get(i);
			next_market_quantities.add(card_type_quantity);
		}
		
		
		//retrieve a list of all cards the player knows are not in the deck
		ArrayList<String> all_known_cards = this.get_all_known_cards(current_goods_hand, current_camels_hand,
				current_market, sold_cards);
		
		//retrieve a list of all cards the player knows are still in the deck
		ArrayList<Double> remaining_card_quantities = this.get_remaining_card_quantities(all_known_cards);		

		//retrieve a list of all probabilities of the next card being of a certain type
		ArrayList<Double> next_card_probabilities = new ArrayList<Double>();
		next_card_probabilities = this.get_remaining_card_probabilities(remaining_card_quantities);
		
		//nested loop to add the multiple probability totals, for how ever many
		//cards are going to be added from the deck
		for (int i = 0; i < new_card_total; i++) {
			
			double new_num;
			
			for (int j = 0; j < 7; j++) {
								
				//if a certain card types are found, their quantities are added by the probability
				//of that specific card appearing next in the deck
				
				//add diamond probability
				if (j == 0) {
					new_num = next_market_quantities.get(0) + next_card_probabilities.get(0); //add quantity by probability
					next_market_quantities.set(0, new_num); //add new quantity to list
				}
					
				//add gold probability
				if (j == 1) {
					new_num = next_market_quantities.get(1) + next_card_probabilities.get(1); //add quantity by probability
					next_market_quantities.set(1, new_num); //add new quantity to list
				}
					
				//add silver probability
				if (j == 2) {
					new_num = next_market_quantities.get(2) + next_card_probabilities.get(2); //add quantity by probability
					next_market_quantities.set(2, new_num); //add new quantity to list
				}

				//add cloth probability
				if (j == 3) {
					new_num = next_market_quantities.get(3) + next_card_probabilities.get(3); //add quantity by probability
					next_market_quantities.set(3, new_num); //add new quantity to list
				}
					
				//add spice probability
				if (j == 4) {
					new_num = next_market_quantities.get(4) + next_card_probabilities.get(4); //add quantity by probability
					next_market_quantities.set(4, new_num); //add new quantity to list
				}
				
				//add leather probability			
				if (j == 5) {
					new_num = next_market_quantities.get(5) + next_card_probabilities.get(5); //add quantity by probability
					next_market_quantities.set(5, new_num); //add new quantity to list
				}
				
				//add camel probability			
				if (j == 6) {
					new_num = next_market_quantities.get(6) + next_card_probabilities.get(6); //add quantity by probability
					next_market_quantities.set(6, new_num); //add new quantity to list
				}
			}
		}
		
		return next_market_quantities;
	}

	//method to establish the points the player would obtain given a sale of pre-specified cards
	public double get_price_of_sale(ArrayList<String> card_list, String card_type, double additional_probability, ArrayList<Integer> current_diamond_tokens, 
			ArrayList<Integer> current_gold_tokens, ArrayList<Integer> current_silver_tokens, ArrayList<Integer> current_cloth_tokens, 
			ArrayList<Integer> current_spice_tokens, ArrayList<Integer> current_leather_tokens) {
		
		double total_points_gained = 0;
			
		//initialise the tokens to be used in the sale
		ArrayList<Integer> current_tokens = new ArrayList<Integer>();
											
		//initialise variable to establish how many tokens are left of a specific type
		//and the amount of cards to be sold
		int token_total_left = 0, sale_cards_total = card_list.size();
						
		if (card_type.equals("D")) { //diamonds
			current_tokens.addAll(current_diamond_tokens);
			token_total_left = current_diamond_tokens.size();
		}	
			
		if (card_type.equals("G")) { //gold
			current_tokens.addAll(current_gold_tokens);
			token_total_left = current_gold_tokens.size();
		}
			
		if (card_type.equals("Sil")) { //silver
			token_total_left = current_silver_tokens.size();
			current_tokens.addAll(current_silver_tokens);
		}
			
		if (card_type.equals("Clo")) { //cloth
			token_total_left = current_cloth_tokens.size();
			current_tokens.addAll(current_cloth_tokens);
		}
			
		if (card_type.equals("Spi")) { //spice
			token_total_left = current_spice_tokens.size();
			current_tokens.addAll(current_spice_tokens);
		}
			
		if (card_type.equals("L")) { //leather
			token_total_left = current_leather_tokens.size();
			current_tokens.addAll(current_leather_tokens);
		}

		int index_limit;
		boolean token_limit_reached = false;

		//establish index limit to ensure that no bound errors occur
		if (sale_cards_total < token_total_left) {
			index_limit = sale_cards_total;
			
			if ((sale_cards_total + 1 < token_total_left) && additional_probability > 0) {
				index_limit += 1;
			}
		}
					
		else {
			token_limit_reached = true;
			index_limit = token_total_left;
		}
		
		//if their is no sale cards, but the probability is still present and needs to be considered
		if (sale_cards_total == 0 && token_total_left > 0) {
			index_limit = 1;
		}
		
		if (sale_cards_total > 0) {
						
			//loop over the correct token type, and find how many points are
			//obtained given a specific sale
			for (int i = 0; i < index_limit; i++) {
				total_points_gained += current_tokens.get(i);			
			}
		}
		
		//adds additional probability if there is tokens left to consider
		if (token_limit_reached != true) {
			total_points_gained += additional_probability * current_tokens.get(index_limit - 1);
		}

		return total_points_gained;
	}
	
	
	//method to add bonus token averages to points gained in sale
	public int add_bonus_token_average(int price_of_sale, ArrayList<String> card_list) {
		
		int updated_points_gained = price_of_sale;					
		int sale_card_total = card_list.size();
			
		//boolean statements to find if a bonus average should be added
			
		if (sale_card_total >= 3 && sale_card_total < 4) {
			updated_points_gained += 2; //x3 bonus token average
		}
			
		if (sale_card_total >= 4 && sale_card_total < 5) {
			updated_points_gained += 5; //x4 bonus token average
		}
			
		if (sale_card_total >= 5) {
			updated_points_gained += 9; //x5 bonus token average
		}
		
		return updated_points_gained;
	}
	
	
	//method to evaluate the current/next state value (with heuristics) of the player's current
	//hand or the current market place
	public double get_total_card_value(ArrayList<Double> card_quantities, ArrayList<Integer> current_diamond_tokens, ArrayList<Integer> current_gold_tokens, 
			ArrayList<Integer> current_silver_tokens, ArrayList<Integer> current_cloth_tokens, ArrayList<Integer> current_spice_tokens, 
			ArrayList<Integer> current_leather_tokens, double k1, double k2, double k3) {
		
		double total_card_value = 0;
		
		//initialise arrays for list of cards
		ArrayList<String> diamond_cards = new ArrayList<String>();
		ArrayList<String> gold_cards = new ArrayList<String>();
		ArrayList<String> silver_cards = new ArrayList<String>();
		ArrayList<String> cloth_cards = new ArrayList<String>();
		ArrayList<String> spice_cards = new ArrayList<String>();
		ArrayList<String> leather_cards = new ArrayList<String>();

		//find card type quantities (cast to double to account for market probabilities)
		double diamond_total = card_quantities.get(0);
		double gold_total = card_quantities.get(1);
		double silver_total = card_quantities.get(2);
		double cloth_total = card_quantities.get(3);
		double spice_total = card_quantities.get(4);
		double leather_total = card_quantities.get(5);
		double camel_total = card_quantities.get(6);
		
		//find card remainder if probabilities factor in the card quantities
		double diamond_total_remainder = diamond_total - (int) diamond_total;
		double gold_total_remainder = gold_total - (int) gold_total;
		double silver_total_remainder = silver_total - (int) silver_total;
		double cloth_total_remainder = cloth_total - (int) cloth_total;
		double spice_total_remainder = spice_total - (int) spice_total;
		double leather_total_remainder = leather_total - (int) leather_total;
		
		//append cards to arrays, with the appropriate quantity
		
		//add diamond cards
		for (int i = 0; i < (int) diamond_total; i++) {
			diamond_cards.add("D");
		}

		//add gold cards
		for (int i = 0; i < (int) gold_total; i++) {
			gold_cards.add("G");
		}

		//add silver cards
		for (int i = 0; i < (int) silver_total; i++) {
			silver_cards.add("Sil");
		}
		
		//add cloth cards
		for (int i = 0; i < (int) cloth_total; i++) {
			cloth_cards.add("Clo");
		}
		
		//add spice cards
		for (int i = 0; i < (int) spice_total; i++) {
			spice_cards.add("Spi");
		}

		//add leather cards
		for (int i = 0; i < (int) leather_total; i++) {
			leather_cards.add("L");
		}
		
		//find the current value of the cards, if a sale was to take place (with probabilities)
		
		double diamond_cards_value = this.get_price_of_sale(diamond_cards, "D", diamond_total_remainder, current_diamond_tokens, 
				current_gold_tokens, current_silver_tokens, current_cloth_tokens, current_spice_tokens, current_leather_tokens);
						
		double gold_cards_value = this.get_price_of_sale(gold_cards, "G", gold_total_remainder, current_diamond_tokens, 
				current_gold_tokens, current_silver_tokens, current_cloth_tokens, current_spice_tokens, current_leather_tokens);
		
		double silver_cards_value = this.get_price_of_sale(silver_cards, "Sil", silver_total_remainder, current_diamond_tokens, 
				current_gold_tokens, current_silver_tokens, current_cloth_tokens, current_spice_tokens, current_leather_tokens);
		
		double cloth_cards_value = this.get_price_of_sale(cloth_cards, "Clo", cloth_total_remainder, current_diamond_tokens, 
				current_gold_tokens, current_silver_tokens, current_cloth_tokens, current_spice_tokens, current_leather_tokens);
		
		double spice_cards_value = this.get_price_of_sale(spice_cards, "Spi", spice_total_remainder, current_diamond_tokens, 
				current_gold_tokens, current_silver_tokens, current_cloth_tokens, current_spice_tokens, current_leather_tokens);

		double leather_cards_value = this.get_price_of_sale(leather_cards, "L", leather_total_remainder, current_diamond_tokens, 
				current_gold_tokens, current_silver_tokens, current_cloth_tokens, current_spice_tokens, current_leather_tokens);
		
		//add bonus averages to the card values previously found
		diamond_cards_value = this.add_bonus_token_average((int) diamond_cards_value, diamond_cards);
		gold_cards_value = this.add_bonus_token_average((int) gold_cards_value, gold_cards);
		silver_cards_value = this.add_bonus_token_average((int) silver_cards_value, silver_cards);
		cloth_cards_value = this.add_bonus_token_average((int) cloth_cards_value, cloth_cards);
		spice_cards_value = this.add_bonus_token_average((int) spice_cards_value, spice_cards);
		leather_cards_value = this.add_bonus_token_average((int) leather_cards_value, leather_cards);
		
		//factor in heuristic: (k1 * (if sellable)) + (k2 * (if one D/G/Sil)) + (k3 * camel_num)
		
		//multiplies D/G/Sil by k1 if more than 1, multiplies by k2 otherwise
		if (diamond_total < 2) {
			total_card_value += diamond_cards_value * k2;
		}
		
		else {
			total_card_value += diamond_cards_value * k1;
		}
		
		if (gold_total < 2) {
			total_card_value += gold_cards_value * k2;
		}
		
		else {
			total_card_value += gold_cards_value * k1;
		}

		if (silver_total < 2) {
			total_card_value += silver_cards_value * k2;
		}
		
		else {
			total_card_value += silver_cards_value * k1;
		}
		
		//Clo/Spi/L leather will always be sellable, so is always multiplied by k1
		total_card_value += (cloth_cards_value + spice_cards_value + leather_cards_value) * k1;

		//camel total is multiplied by k3
		total_card_value += camel_total * k3;
		
		return total_card_value;
	}
	
	
	//method to evaluate the next state value (with heuristics) of the player's hand after 
	//selling cards, and subtracting the next market value
	public double get_heuristic_state_value(ArrayList<Double> hand_quantities, ArrayList<Double> market_quantities, ArrayList<String> action, 
			ArrayList<Integer> current_diamond_tokens, ArrayList<Integer> current_gold_tokens, ArrayList<Integer> current_silver_tokens, 
			ArrayList<Integer> current_cloth_tokens, ArrayList<Integer> current_spice_tokens, ArrayList<Integer> current_leather_tokens, 
			double hand_k1, double hand_k2, double hand_k3, double market_k1, double market_k2, double market_k3) {
		
		int points_gained_in_sale = 0;
		
		//add points if a sale has taken place with current action
		if (action.get(0).equals("sell_cards")) {
			
			String current_goods_card = "";
			int index = 1;
			
			//loop over goods cards being sold and sum their total value
			while (!current_goods_card.equals("--")) {
				
				current_goods_card = action.get(index);
				
				//add token values if there are tokens left
				
				//add diamond token values
			    if (current_diamond_tokens.size() != 0 && current_goods_card.equals("D")) {
			    	
					int chosen_token = current_diamond_tokens.get(0);
					current_diamond_tokens.remove(0);
					points_gained_in_sale += chosen_token;
			    }
			    
				//add gold token values
			    if (current_gold_tokens.size() != 0 && current_goods_card.equals("G")) {
			    	
					int chosen_token = current_gold_tokens.get(0);
					current_gold_tokens.remove(0);
					points_gained_in_sale += chosen_token;
			    }

				//add silver token values
			    if (current_silver_tokens.size() != 0 && current_goods_card.equals("Sil")) {
			    	
					int chosen_token = current_silver_tokens.get(0);
					current_silver_tokens.remove(0);
					points_gained_in_sale += chosen_token;
			    }

				//add cloth token values
			    if (current_cloth_tokens.size() != 0 && current_goods_card.equals("Clo")) {
			    	
					int chosen_token = current_cloth_tokens.get(0);
					current_cloth_tokens.remove(0);
					points_gained_in_sale += chosen_token;
			    }
			    
				//add spice token values
			    if (current_spice_tokens.size() != 0 && current_goods_card.equals("Spi")) {
			    	
					int chosen_token = current_spice_tokens.get(0);
					current_spice_tokens.remove(0);
					points_gained_in_sale += chosen_token;
			    }
			    
				//add leather token values
			    if (current_leather_tokens.size() != 0 && current_goods_card.equals("L")) {
			    	
					int chosen_token = current_leather_tokens.get(0);
					current_leather_tokens.remove(0);
					points_gained_in_sale += chosen_token;
			    }

				index++;
			}
			
			//add x3 multiplier average if there are three cards present
			if (action.size() == 5) {
				points_gained_in_sale += 2;
			}
			
			//add x4 multiplier average if there are three cards present
			if (action.size() == 6) {
				points_gained_in_sale += 5;
			}
			
			//add x5 multiplier average if there are three cards present		
			if (action.size() >= 7) {
				points_gained_in_sale += 9;
			}
		}
		
		
		//find the value of the current player's hand
		double total_hand_value = this.get_total_card_value(hand_quantities,  current_diamond_tokens, 
				 current_gold_tokens, current_silver_tokens, current_cloth_tokens, current_spice_tokens, 
				current_leather_tokens, hand_k1, hand_k2, hand_k3);
		
		//find the value of the current player's hand
		double total_market_value = this.get_total_card_value(market_quantities,  current_diamond_tokens, 
				 current_gold_tokens, current_silver_tokens, current_cloth_tokens, current_spice_tokens, 
				current_leather_tokens, market_k1, market_k2, market_k3);		
		
		//find the overall state value by subtracting the market value from the hand value, and adding the points
		//gained if a sale was to take place
		double total_state_value = total_hand_value + points_gained_in_sale - total_market_value;

		return total_state_value;
	}
		
	//methods to estimate the opposite player's current hand (goods and camels)
	
	//method to get the current camel count of the opposing player at the start of each round
	public int get_opposition_initial_camel_total(int opposite_player_goods_total, boolean opposite_player_has_camels) {
	
		//if opposite player has no camels, return 0
		if (!opposite_player_has_camels) {
			return 0;
		}
		
		//if opposite player does have camels, find the number by subtracting the goods cards total by 5 (starting card amount)
		else {
			int total_camels = 5 - opposite_player_goods_total;
			return total_camels;
		}
	}
	
	//method to update the camel total by observing the previous move made by the opposing player
	public int update_opposition_camel_total(int current_camel_total, ArrayList<String> previous_move) {
				
		int total_remaining_camels = 0;

		String action_type = previous_move.get(0);
		
		//if the action is either 'take_one_good' or 'sell_goods' return current amount as
		//this does not effect the camel quantity
		if (action_type.equals("take_one_good") || action_type.equals("sell_cards")) {
			return current_camel_total;
		}
		
		if (action_type.equals("take_camels")) {
			int camel_total_taken = previous_move.size() - 1;
			
			total_remaining_camels = current_camel_total + camel_total_taken;
			
			return total_remaining_camels;
		}
		
		//can only give camels in a trade
		if (action_type.equals("trade_cards")) {
			
			int index_num = 1, camels_given = 0;
			
			//find all camels that the opposite player has given
			while (!previous_move.get(index_num).equals("--")) {
				
				if (previous_move.get(index_num).equals("Cam")) {
				
					camels_given += 1;
				}
				
				index_num++;
			}
			
			//calculate total camels remaining by subtracting the camels given by 
			//the camels recieved in the trade
			total_remaining_camels = current_camel_total - camels_given;
			
			return total_remaining_camels;
		}
		
		//will return error if an action type is invalid 
		else {
			throw new IllegalArgumentException("ERROR: Action can only equal: 'take_camels', 'take_one_good',"
					+ "'trade_cards' or 'sell_cards'");
		}

	}
	
	//method to get all known cards the opposite player has by inspecting past moves
	public ArrayList<Double> get_known_opposition_card_quantities(ArrayList<ArrayList<String>> past_moves, boolean is_player_one_turn) {
		
		//container to find all past moves of the other player
		ArrayList<ArrayList<String>> opposite_player_past_moves = new ArrayList<ArrayList<String>>();
		
		//looping through all past moves
		for (int i = 0; i < past_moves.size(); i++) {
			
			//if it is not player one's go, and the past_moves element is even, it means player_two
			//is the opposite player and all their moves (even elements) are added
			if (!is_player_one_turn && (i % 2 == 0)) {
				
				opposite_player_past_moves.add(past_moves.get(i));
			}
			
			//if it is player one's go, and the past_moves element is odd, it means player_one
			//is the opposite player and all their moves (odd elements) are added
			if (is_player_one_turn && (i % 2 != 0)) {
				
				opposite_player_past_moves.add(past_moves.get(i));
			}
		}
				
		ArrayList<String> past_move_selected;
		String action_type;
		
		double diamond_total = 0, gold_total = 0, silver_total = 0, cloth_total = 0, spice_total = 0, 
				leather_total = 0, camel_total = 0;
		
		//looping through all opposition's moves
		for (int i = 0; i < opposite_player_past_moves.size(); i++) {
			
			past_move_selected = opposite_player_past_moves.get(i);
			action_type = past_move_selected.get(0);
			
			//if action 'take_camels' is selected, it simply adds all the
			//camels that were previously taken
			if (action_type.equals("take_camels")) {
				
				int camel_cards_taken = past_move_selected.size() - 1;
				camel_total += camel_cards_taken;
			}
			
			//if action 'take_one_good' is selected, it simply adds one of
			//the good type that was previously taken
			if (action_type.equals("take_one_good")) {
				
				String selected_card = past_move_selected.get(1);
				
				if (selected_card.equals("D")) {
			
					//adds if diamond was selected
					
					//if the current value is 0 or lower, it sets it to 1
					if (diamond_total <= 0) {
						diamond_total = 1;
					}
					
					//else it adds on the current value
					else {
						diamond_total += 1;
					}
				}
				
				if (selected_card.equals("G")) {
					
					//adds if gold was selected
					
					//if the current value is 0 or lower, it sets it to 1
					if (gold_total <= 0) {
						gold_total = 1;
					}
					
					else {
						gold_total += 1;
					}
				}

				if (selected_card.equals("Sil")) {
					
					//adds if silver was selected
					
					//if the current value is 0 or lower, it sets it to 1
					if (silver_total <= 0) {
						silver_total = 1;
					}
					
					else {
						silver_total += 1;
					}
				}

				if (selected_card.equals("Clo")) {
					
					//adds if cloth was selected
					
					//if the current value is 0 or lower, it sets it to 1
					if (cloth_total <= 0) {
						cloth_total = 1;
					}
					
					else {
						cloth_total += 1;
					}
				}

				//adds if spice was selected
				
				//if the current value is 0 or lower, it sets it to 1
				if (selected_card.equals("Spi")) {
					
					if (spice_total <= 0) {
						spice_total = 1;
					}
					
					else {
						spice_total += 1;
					}
				}

				//adds if leather was selected
				
				//if the current value is 0 or lower, it sets it to 1
				if (selected_card.equals("L")) {

					if (leather_total <= 0) {
						leather_total = 1;
					}
					
					else {
						leather_total += 1;
					}
				}
			}
			
			//if action 'trade_cards' is selected, it simply adds the cards
			//previously received and subtracts the cards taken
			if (action_type.equals("trade_cards")) {
								
				int index_num = 1;
				
				//find all cards that are taken from the opposite player, and remove these
				//total amounts from the card totals
				while (!past_move_selected.get(index_num).equals("--")) {
					
					String selected_card = past_move_selected.get(index_num);
					
					//subtracts the cards value by type if selected
					
					if (selected_card.equals("D") && diamond_total > 0) {
						diamond_total -= 1;
					}
					
					if (selected_card.equals("G") && gold_total > 0) {
						gold_total -= 1;
					}

					if (selected_card.equals("Sil") && silver_total > 0) {
						silver_total -= 1;
					}

					if (selected_card.equals("Clo") && cloth_total > 0) {
						cloth_total -= 1;
					}

					if (selected_card.equals("Spi") && spice_total > 0) {
						spice_total -= 1;
					}

					if (selected_card.equals("L") && leather_total > 0) {
						leather_total -= 1;
					}
					
					if (selected_card.equals("Cam") && camel_total > 0) {
						camel_total -= 1;
					}

					index_num++;
				}
				
				index_num++;
								
				//find all cards that are given to the opposite player, and add these
				//total amounts from the card totals
				while (index_num != past_move_selected.size()) {
					
					String selected_card = past_move_selected.get(index_num);
					
					//adds the cards value by type if selected
					
					//if the current value is 0 or lower, it sets it to 1					
					if (selected_card.equals("D")) {
						
						if (diamond_total <= 0) {
							diamond_total = 1;
						}
						
						else {
							diamond_total += 1;
						}
					}
					
					//if the current value is 0 or lower, it sets it to 1
					if (selected_card.equals("G")) {
						
						if (gold_total <= 0) {
							gold_total = 1;
						}
						
						else {
							gold_total += 1;
						}
					}
					
					//if the current value is 0 or lower, it sets it to 1
					if (selected_card.equals("Sil")) {
						
						if (silver_total <= 0) {
							silver_total = 1;
						}
						
						else {
							silver_total += 1;
						}
					}
					
					//if the current value is 0 or lower, it sets it to 1
					if (selected_card.equals("Clo")) {
						
						if (cloth_total <= 0) {
							cloth_total = 1;
						}
						
						else {
							cloth_total += 1;
						}
					}
					
					//if the current value is 0 or lower, it sets it to 1
					if (selected_card.equals("Spi")) {
						
						if (spice_total <= 0) {
							spice_total = 1;
						}
						
						else {
							spice_total += 1;
						}
					}
					
					//if the current value is 0 or lower, it sets it to 1
					if (selected_card.equals("L")) {

						if (leather_total <= 0) {
							leather_total = 1;
						}
						
						else {
							leather_total += 1;
						}
					}
					
					index_num++;
				}
			}
			
			if (action_type.equals("sell_cards")) {
				
				int index_num = 1;

				//find all cards that are taken from the opposite player, and remove these
				//total amounts from the card totals
				while (!past_move_selected.get(index_num).equals("--")) {
					
					String selected_card = past_move_selected.get(index_num);
					
					if (selected_card.equals("D") && diamond_total > 0) {
						diamond_total -= 1;
					}
					
					if (selected_card.equals("G") && gold_total > 0) {
						gold_total -= 1;
					}

					if (selected_card.equals("Sil") && silver_total > 0) {
						silver_total -= 1;
					}

					if (selected_card.equals("Clo") && cloth_total > 0) {
						cloth_total -= 1;
					}

					if (selected_card.equals("Spi") && spice_total > 0) {
						spice_total -= 1;
					}

					if (selected_card.equals("L") && leather_total > 0) {
						leather_total -= 1;
					}
					
					index_num++;
				}
			}		
		}
		
		//creates an arraylist to represent card type quantities
		ArrayList<Double> get_cards_known_totals = new ArrayList<Double>();
		
		//adds all card type totals to arraylist
		get_cards_known_totals.add(diamond_total);
		get_cards_known_totals.add(gold_total);
		get_cards_known_totals.add(silver_total);
		get_cards_known_totals.add(cloth_total);
		get_cards_known_totals.add(spice_total);
		get_cards_known_totals.add(leather_total);
		get_cards_known_totals.add(camel_total);

		return get_cards_known_totals;
	}

	//method to turn a list of card quantities into a list of their string card names
	public ArrayList<String> convert_card_quantities_to_string_names(ArrayList<Double> card_quantities) {
		
		//initialised a list to hold card names instead of quantities
		ArrayList<String> card_name_list = new ArrayList<String>();
		
		//looping over each card quantity
		for (int i = 0; i < card_quantities.size(); i++) {
			
			//looping over the total amount of cards for each type
			for (int j = 0; j < card_quantities.get(i); j++) {
				
				//adding the appropriate string depending on where it originates
				//from the list
				
				if (i == 0) {
					card_name_list.add("D"); //diamonds
				}
				
				if (i == 1) {
					card_name_list.add("G"); //gold
				}

				if (i == 2) {
					card_name_list.add("Sil"); //silver
				}

				if (i == 3) {
					card_name_list.add("Clo"); //cloth
				}

				if (i == 4) {
					card_name_list.add("Spi"); //spice
				}
				
				if (i == 5) {
					card_name_list.add("L"); //leather
				}
				
				if (i == 6) {
					card_name_list.add("Cam");
				}
			}
		}
		
		return card_name_list;
	}
	
	//method to return the number of unknown goods cards of the opposing player
	public int get_unknown_goods_cards_total(ArrayList<Double> cards_known_totals, int known_goods_cards_total) {
		
		//looping over the cards known totals to find all type quantities and summing them
		int goods_cards_total_found = 0;
				
		for (int i = 0; i < 6; i++) {
			goods_cards_total_found += cards_known_totals.get(i);
		}
		
		//find the number of cards still not known by subtracting the goods cards found from
		//observation with the goods cards total the player already knows
		int unknown_goods_cards_total = known_goods_cards_total - goods_cards_total_found;
		
		return unknown_goods_cards_total;
	}
	
	//method to approximate the opposite player's hand at each turn
	public ArrayList<Double> evaluate_opposition_card_quantites(ArrayList<Double> cards_known_totals, 
			int known_goods_cards_total, int camel_cards_total_estimate, ArrayList<String> current_goods_hand, 
			ArrayList<String> current_camels_hand, ArrayList<String> current_market, 
			ArrayList<String> sold_cards, int total_cards_remaining_in_deck) {
								
		//get all known cards from what is currently visible to the player
		ArrayList<String> all_known_visible_cards = this.get_all_known_cards(current_goods_hand, current_camels_hand, 
				current_market, sold_cards);
		
		ArrayList<Double> cards_known_totals_temp = new ArrayList<Double>();
		cards_known_totals_temp.addAll(cards_known_totals);
		
		//get all known cards currently in the opposition's hand
		ArrayList<String> all_known_opposition_cards = this.convert_card_quantities_to_string_names(cards_known_totals_temp);
						
		//combine these two lists to establish all known cards outside of the main deck
		ArrayList<String> all_known_cards = new ArrayList<String>();
		all_known_cards.addAll(all_known_visible_cards);
		all_known_cards.addAll(all_known_opposition_cards);
		
		//obtain remaining card quantities from deck
		ArrayList<Double> remaining_card_quantities = this.get_remaining_card_quantities(all_known_cards);
				
		//obtain the remaining card probabilities
		ArrayList<Double> remaining_card_probabilities = this.get_remaining_card_probabilities(remaining_card_quantities);
		
		//find the number of goods cards which are currently unknown
		int unknown_card_total = this.get_unknown_goods_cards_total(cards_known_totals_temp, known_goods_cards_total);

		//loop over cards, to add probabilities for unknown cards
		for (int i = 0; i < unknown_card_total; i++) {
			
			double new_num;

			for (int j = 0; j < 6; j++) {
									
				//if a certain card type is found, its quantity is added by the probability
				//of that specific card appearing next in the deck
				if (j == 0) {
					new_num = cards_known_totals_temp.get(0) + remaining_card_probabilities.get(0); //add quantity by probability
					cards_known_totals_temp.set(0, new_num); //add new quantity to list
				}
						
				//add gold cards
				if (j == 1) {
					new_num = cards_known_totals_temp.get(1) + remaining_card_probabilities.get(1); //add quantity by probability
					cards_known_totals_temp.set(1, new_num); //add new quantity to list
				}
						
				//add silver cards
				if (j == 2) {
					new_num = cards_known_totals_temp.get(2) + remaining_card_probabilities.get(2); //add quantity by probability
					cards_known_totals_temp.set(2, new_num); //add new quantity to list
				}

				//add cloth cards
				if (j == 3) {
					new_num = cards_known_totals_temp.get(3) + remaining_card_probabilities.get(3); //add quantity by probability
					cards_known_totals_temp.set(3, new_num); //add new quantity to list
				}
						
				//add spice cards
				if (j == 4) {
					new_num = cards_known_totals_temp.get(4) + remaining_card_probabilities.get(4); //add quantity by probability
					cards_known_totals_temp.set(4, new_num); //add new quantity to list
				}
					
				//add leather cards			
				if (j == 5) {
					new_num = cards_known_totals_temp.get(5) + remaining_card_probabilities.get(5); //add quantity by probability
					cards_known_totals_temp.set(5, new_num); //add new quantity to list
				}
			}
		}
		
		//set camel quantity to pre-calculated value
		cards_known_totals_temp.set(6, (double) camel_cards_total_estimate); //add new quantity to list
		
		return cards_known_totals_temp;
	}
	
	//methods for minimax algorithm
	
	//method to retrieve all goods from a list containing goods and camels cards
	public ArrayList<String> retrive_goods_from_goods_and_camels(ArrayList<String> current_goods_and_camels_hand) {
		
		ArrayList<String> current_goods_hand = new ArrayList<String>();
		
		//loop over all goods and camel cards
		for (int i = 0; i < current_goods_and_camels_hand.size(); i++) {
			
			String current_card = current_goods_and_camels_hand.get(i);
			
			//if a card isn't a camel, add it to new card list
			if (!current_card.equals("Cam")) {
				current_goods_hand.add(current_card);
			}
		}
		
		return current_goods_hand;
	}
	
	//method to retrieve all camels from a list containing goods and camels cards
	public ArrayList<String> retrive_camels_from_goods_and_camels(ArrayList<String> current_goods_and_camels_hand) {
		
		ArrayList<String> current_camels_hand = new ArrayList<String>();
		
		//loop over all goods and camel cards
		for (int i = 0; i < current_goods_and_camels_hand.size(); i++) {
			
			String current_card = current_goods_and_camels_hand.get(i);
			
			//if a card is a camel, add it to new card list
			if (current_card.equals("Cam")) {
				current_camels_hand.add(current_card);
			}
		}
		
		return current_camels_hand;
	}
	
	//method to update current player score, given a sale takes place
	public int update_current_player_score(int current_player_score, ArrayList<String> action, ArrayList<Integer> current_diamond_tokens,
			ArrayList<Integer> current_gold_tokens, ArrayList<Integer> current_silver_tokens, ArrayList<Integer> current_cloth_tokens, 
			ArrayList<Integer> current_spice_tokens, ArrayList<Integer> current_leather_tokens) {
		
		int updated_player_score = current_player_score;
		
		//the players score will only increase if they sell cards
		if (action.get(0).equals("sell_cards")) {
			
			//obtaining the card types being sold, and the amount
			String card_type_sold = action.get(1);
			int total_cards_sold = action.size() - 2;
			
			//checking if the cards are diamonds
			if (card_type_sold.equals("D")) {
				
				//updated the total_cards_sold value to avoid indexing errors
				if (total_cards_sold > current_diamond_tokens.size()) {
					total_cards_sold = current_diamond_tokens.size();
				}
				
				//increases score accordingly with token values
				for (int i = 0; i < total_cards_sold; i++) {
					updated_player_score += current_diamond_tokens.get(i);
				}
			}
			
			//checking if the cards are gold
			if (card_type_sold.equals("G")) {
				
				if (total_cards_sold > current_gold_tokens.size()) {
					total_cards_sold = current_gold_tokens.size();
				}
				
				for (int i = 0; i < total_cards_sold; i++) {
					updated_player_score += current_gold_tokens.get(i);
				}
			}

			//checking if the cards are silver
			if (card_type_sold.equals("Sil")) {
				
				if (total_cards_sold > current_silver_tokens.size()) {
					total_cards_sold = current_silver_tokens.size();
				}
				
				for (int i = 0; i < total_cards_sold; i++) {
					updated_player_score += current_silver_tokens.get(i);
				}
			}

			//checking if the cards are cloth
			if (card_type_sold.equals("Clo")) {
				
				if (total_cards_sold > current_cloth_tokens.size()) {
					total_cards_sold = current_cloth_tokens.size();
				}
				
				for (int i = 0; i < total_cards_sold; i++) {
					updated_player_score += current_cloth_tokens.get(i);
				}
			}
			
			//checking if the cards are spices
			if (card_type_sold.equals("Spi")) {
				
				if (total_cards_sold > current_spice_tokens.size()) {
					total_cards_sold = current_spice_tokens.size();
				}
				
				for (int i = 0; i < total_cards_sold; i++) {
					updated_player_score += current_spice_tokens.get(i);
				}
			}

			//checking if the cards are leather
			if (card_type_sold.equals("L")) {
				
				if (total_cards_sold > current_leather_tokens.size()) {
					total_cards_sold = current_leather_tokens.size();
				}
				
				for (int i = 0; i < total_cards_sold; i++) {
					updated_player_score += current_leather_tokens.get(i);
				}
			}
		}
		
		return updated_player_score;
	}
	
	//method to update the tokens values and availability of a given type
	public ArrayList<Integer> update_current_goods_tokens(ArrayList<Integer> current_tokens, ArrayList<String> action) {
		
		ArrayList<Integer> new_tokens = new ArrayList<Integer>();
		
		//make clone of current tokens
		for (int i = 0; i < current_tokens.size(); i++) {
			new_tokens.add(current_tokens.get(i));
		}
		
		//tokens only are removed when a selling action takes place
		if (action.get(0).equals("sell_cards")) {
			
			//the action also has the action_type and end of action string ('--'), therefore is
			//subtracted to find the total_cards_sold
			int total_cards_sold = action.size() - 2;
			
			//loops over all cards sold and removes tokens, unless there are no more to take
			for (int i = 0; i < total_cards_sold; i++) {
			
			    if (new_tokens.size() == 0) {
			    	break;
			    }
			    
			    else {
			    	new_tokens.remove(0);
			    }
			}
			
			return new_tokens;
		}
		
		//every other action does not take tokens, therefore the token total remains the same
		else {
			return new_tokens;
		}
	}
	
	//method to update the cards sold, after a selling action has occurred
	public ArrayList<String> update_cards_sold(ArrayList<String> current_sold_cards, ArrayList<String> action) {
		
		ArrayList<String> new_sold_cards = new ArrayList<String>();
		
		//make clone of current tokens
		for (int i = 0; i < current_sold_cards.size(); i++) {
			new_sold_cards.add(current_sold_cards.get(i));
		}

		//sold cards are only added when a selling action takes place
		if (action.get(0).equals("sell_cards")) {
			
			//the action also has the action_type and end of action string ('--'), therefore is
			//subtracted to find the total_cards_sold
			int total_cards_sold = action.size() - 2;

			//card type sold will always be the second value in the ArrayList
			String card_type_sold = action.get(1);
			
			//loops over all cards sold and adds each one to the sold_cards list
			for (int i = 0; i < total_cards_sold; i++) {
				
				new_sold_cards.add(card_type_sold);
			}
		}
		
		return new_sold_cards;
	}
	
	//method to update the cards remaining in the deck, after certain actions have taken place
	public int update_cards_remaining(int current_cards_remaining, ArrayList<String> action, ArrayList<String> current_market) {
		
		int new_cards_remaining;
		
		//cards get taken out of the deck every time one goods card is taken, or every time
		//all camel cards are taken
		if (action.get(0).equals("take_one_good")) {
			new_cards_remaining = current_cards_remaining - 1;
			return new_cards_remaining;
		}
		
		if (action.get(0).equals("take_camels")) {
			
			int total_camels_taken = 0;
			
			//loop over market cards to see how many camels would be taken in a
			//'take_camels' move
			for (int i = 0; i < current_market.size(); i++) {
				if (current_market.get(i).equals("Cam")) {
					total_camels_taken++;
				}
			}
			
			new_cards_remaining = current_cards_remaining - total_camels_taken;
			return new_cards_remaining;
		}
		
		//the other two actions do not effect the remaining card total
		else {
			return current_cards_remaining;
		}
	}
		
	//method to truncate market probabilities, to find the actual known quantities for the market place
	public ArrayList<Double> truncate_market_probabilities(ArrayList<Double> market_with_probabilities) {
		
		ArrayList<Double> market_without_probabilities = new ArrayList<Double>();
		
		double current_card_type_quantity;
		int new_current_card_type_quantity;
		
		//loop over current market quantities (with probabilities) and truncate values
		for (int i = 0; i < market_with_probabilities.size(); i++) {
			
			//obtain current card quantity and cast to an int (to truncate) and add back to
			//arraylist as a double to find new value
			current_card_type_quantity = market_with_probabilities.get(i);
			new_current_card_type_quantity = (int) current_card_type_quantity;
			
			market_without_probabilities.add((double) new_current_card_type_quantity);
		}
		
		return market_without_probabilities;
	}
	
	//method to find best next action using minimax algorithm (expectaminimax)
	public ArrayList<String> MINIMAX_NEXT_ACTION(int current_depth, int depth_limit, ArrayList<ArrayList<String>> all_possible_actions, 
			ArrayList<Double> current_hand_quantities, ArrayList<Double> opposition_hand_quantities, int unknown_card_total, 
			ArrayList<Double> current_market_quantities, int current_player_score, int current_opposing_score, ArrayList<Integer> diamond_token_total, 
			ArrayList<Integer> gold_token_total, ArrayList<Integer> silver_token_total, ArrayList<Integer> cloth_token_total, 
			ArrayList<Integer> spice_token_total, ArrayList<Integer> leather_token_total, ArrayList<String> sold_cards, int current_cards_remaining, 
			double hand_k1, double hand_k2, double hand_k3, double market_k1, double market_k2, double market_k3, ArrayList<Double> initial_remaining_probabilities) {
		
		ArrayList<String> next_possible_action = new ArrayList<String>();
		ArrayList<String> best_possible_action = new ArrayList<String>();
		
		double best_state_value = 0, current_state_value = 0;
		
		//if the depth limit is 1, 3, 5 ... ,the best state value needs to be maximised
		if (depth_limit % 2 == 0) {
			best_state_value = -9999999;
		}
		
		//if the depth limit is 2, 4, 6 ... ,the best state value needs to be minimised
		else {
			best_state_value = 9999999;
		}

		//loop over all possible actions to find best possible next action within minimax
		for (int i = 0; i < all_possible_actions.size(); i++) {
			
			next_possible_action = all_possible_actions.get(i);
			
			current_state_value = INITIAL_MIN_NODE_VALUE(current_depth, depth_limit, next_possible_action, current_hand_quantities, 
					opposition_hand_quantities, unknown_card_total, current_market_quantities, current_player_score, 
					current_opposing_score, diamond_token_total, gold_token_total, silver_token_total, cloth_token_total, 
					spice_token_total, leather_token_total, sold_cards, current_cards_remaining, hand_k1, hand_k2, hand_k3, 
					market_k1, market_k2, market_k3, initial_remaining_probabilities);

			//finding the highest state value
			if (depth_limit % 2 == 0) {
				if (current_state_value > best_state_value) {
					best_state_value = current_state_value;
					best_possible_action = next_possible_action;
				}
			}
			
			//finding the lowest state value
			else {
				if (current_state_value < best_state_value) {
					best_state_value = current_state_value;
					best_possible_action = next_possible_action;
				}
			}
		}
		
		return best_possible_action;
	}
	
	//method to find the initial MIN node value, or find current player's state if depth has been reached
	public double INITIAL_MIN_NODE_VALUE(int current_depth, int depth_limit, ArrayList<String> next_possible_action, ArrayList<Double> current_hand_quantities, 
			ArrayList<Double> opposition_hand_quantities, int unknown_card_total, ArrayList<Double> current_market_quantities, int current_player_score, 
			int current_opposing_score, ArrayList<Integer> diamond_token_total, ArrayList<Integer> gold_token_total, ArrayList<Integer> silver_token_total, 
			ArrayList<Integer> cloth_token_total, ArrayList<Integer> spice_token_total, ArrayList<Integer> leather_token_total, ArrayList<String> sold_cards, 
			int current_cards_remaining, double hand_k1, double hand_k2, double hand_k3, double market_k1, double market_k2, double market_k3,
			ArrayList<Double> initial_remaining_probabilities) {
		
		//only search further if depth limit has not been reached
		if (current_depth != depth_limit) {
						
			//set initial node value unrealistically low to be updated
			double node_value = 9999999;
						
			//clone all input parameters to new lists and variables to avoid unavoidable bugs
			
			ArrayList<String> current_hand = new ArrayList<String>();
			ArrayList<String> current_goods_hand = new ArrayList<String>();
			ArrayList<String> current_camels_hand = new ArrayList<String>();
			ArrayList<String> current_market = new ArrayList<String>();
				
			ArrayList<Double> next_hand_quantities = new ArrayList<Double>();
			ArrayList<Double> next_market_quantities = new ArrayList<Double>();
			ArrayList<Double> next_market_quantities_with_prob = new ArrayList<Double>();

			ArrayList<Double> current_hand_quantities_temp = new ArrayList<Double>();
			current_hand_quantities_temp.addAll(current_hand_quantities);
								
			ArrayList<Double> current_market_quantities_temp = this.truncate_market_probabilities(current_market_quantities);
				
			ArrayList<Double> opposition_hand_estimate = new ArrayList<Double>();
			opposition_hand_estimate.addAll(opposition_hand_quantities);
				
			ArrayList<Integer> next_diamond_token_total = new ArrayList<Integer>();
			next_diamond_token_total.addAll(diamond_token_total);
				
			ArrayList<Integer> next_gold_token_total = new ArrayList<Integer>();
			next_gold_token_total.addAll(gold_token_total);
				
			ArrayList<Integer> next_silver_token_total = new ArrayList<Integer>();
			next_silver_token_total.addAll(silver_token_total);

			ArrayList<Integer> next_cloth_token_total = new ArrayList<Integer>();
			next_cloth_token_total.addAll(cloth_token_total);

			ArrayList<Integer> next_spice_token_total = new ArrayList<Integer>();
			next_spice_token_total.addAll(spice_token_total);

			ArrayList<Integer> next_leather_token_total = new ArrayList<Integer>();
			next_leather_token_total.addAll(leather_token_total);
				
			ArrayList<String> next_sold_cards = new ArrayList<String>();
			next_sold_cards.addAll(sold_cards);
				
			int next_current_player_score = current_player_score, next_cards_remaining = current_cards_remaining;
								
			//find all the cards within the current players goods and camels hand, and also the market place
			current_hand = this.convert_card_quantities_to_string_names(current_hand_quantities);
			current_goods_hand = this.retrive_goods_from_goods_and_camels(current_hand);
			current_camels_hand = this.retrive_camels_from_goods_and_camels(current_hand);
			current_market = this.convert_card_quantities_to_string_names(current_market_quantities);
				
			//find all new cards quantities of the next hand
			next_hand_quantities = this.get_next_hand_quantities(current_hand_quantities_temp, current_market_quantities_temp, next_possible_action);
				
			//find all new card quantities of the next market
			next_market_quantities = this.get_next_market_quantities(current_market_quantities_temp, next_possible_action);
				
			next_market_quantities_with_prob = this.get_next_market_quantities_with_prob(next_market_quantities, next_possible_action, current_market, 
					current_goods_hand, current_camels_hand, sold_cards);
				
			//find new current player's score, if sale takes place
			next_current_player_score = this.update_current_player_score(current_player_score, next_possible_action, diamond_token_total, gold_token_total, 
					silver_token_total, cloth_token_total, spice_token_total, leather_token_total);
				
			//find all new token totals
			if (next_possible_action.get(0).equals("sell_cards")) {
					
				if (next_possible_action.get(1).equals("D")) {
					next_diamond_token_total = this.update_current_goods_tokens(diamond_token_total, next_possible_action);
				}
					
				if (next_possible_action.get(1).equals("G")) {
					next_gold_token_total = this.update_current_goods_tokens(gold_token_total, next_possible_action);
				}
					
				if (next_possible_action.get(1).equals("Sil")) {
					next_silver_token_total = this.update_current_goods_tokens(silver_token_total, next_possible_action);
				}

				if (next_possible_action.get(1).equals("Clo")) {
					next_cloth_token_total = this.update_current_goods_tokens(cloth_token_total, next_possible_action);
				}
					
				if (next_possible_action.get(1).equals("Spi")) {
					next_spice_token_total = this.update_current_goods_tokens(spice_token_total, next_possible_action);
				}
					
				if (next_possible_action.get(1).equals("L")) {
					next_leather_token_total = this.update_current_goods_tokens(leather_token_total, next_possible_action);
				}
			}
				
			//find all cards newly sold
			next_sold_cards = this.update_cards_sold(sold_cards, next_possible_action);
				
			//find the new card total remaining in the deck
			next_cards_remaining = this.update_cards_remaining(current_cards_remaining, next_possible_action, current_market);
				
			//find convert next market cards and opposition's hand to string representations
			ArrayList<Double> next_market_cards_truncated = this.truncate_market_probabilities(next_market_quantities_with_prob);
			ArrayList<String> next_market_cards = this.convert_card_quantities_to_string_names(next_market_cards_truncated);
			ArrayList<String> opposition_goods_and_camels_cards = this.convert_card_quantities_to_string_names(opposition_hand_estimate);
			ArrayList<String> opposition_goods_cards = this.retrive_goods_from_goods_and_camels(opposition_goods_and_camels_cards);
			ArrayList<String> opposition_camels_cards = this.retrive_camels_from_goods_and_camels(opposition_goods_and_camels_cards);

			//find all possible actions the opposite player can take in the next turn
			ArrayList<ArrayList<String>> all_possible_actions_next_player = this.get_all_possible_moves(next_market_cards, 
					opposition_goods_cards, opposition_camels_cards);
			
			//minimise the current node value
			node_value = java.lang.Math.min(node_value, this.MAX_NODE_VALUE(current_depth + 1, depth_limit, all_possible_actions_next_player, next_hand_quantities, 
					opposition_hand_estimate, unknown_card_total, next_market_quantities_with_prob, current_opposing_score, next_current_player_score, 
					next_diamond_token_total, next_gold_token_total, next_silver_token_total, next_cloth_token_total, next_spice_token_total, next_leather_token_total, 
					next_sold_cards, next_cards_remaining, hand_k1, hand_k2, hand_k3, market_k1, market_k2, market_k3, initial_remaining_probabilities));
					
			return node_value;
		}
		
		//if depth limit has been reached, find maximum heuristic value
		else {
			
			//find next hand and market quantities
			ArrayList<Double> hand_quantities = this.get_next_hand_quantities(current_hand_quantities, current_market_quantities, next_possible_action);
			ArrayList<Double> market_quantities = this.get_next_market_quantities(current_market_quantities, next_possible_action);
		
			//convert market and hand quantities to string representations
			ArrayList<String> current_market = this.convert_card_quantities_to_string_names(current_market_quantities);
			ArrayList<String> current_goods_and_camels = this.convert_card_quantities_to_string_names(current_hand_quantities);
			ArrayList<String> current_goods_hand = this.retrive_goods_from_goods_and_camels(current_goods_and_camels);
			ArrayList<String> current_camels_hand = this.retrive_camels_from_goods_and_camels(current_goods_and_camels);

			//add market probability
			ArrayList<Double> market_quantities_with_prob = this.get_next_market_quantities_with_prob(market_quantities, next_possible_action, 
					current_market, current_goods_hand, current_camels_hand, sold_cards);

			//find next total state value
			double next_total_state_value = this.get_heuristic_state_value(hand_quantities, 
					market_quantities_with_prob, next_possible_action, diamond_token_total, gold_token_total, silver_token_total, cloth_token_total, 
					spice_token_total, leather_token_total, hand_k1, hand_k2, hand_k3, market_k1, market_k2, market_k3);

			//add state value to current score
			next_total_state_value += current_player_score;

			return next_total_state_value;
		}
	}

	//method to find the MAX node value, or find opposing player's highest state value if depth has been reached
	public double MAX_NODE_VALUE(int current_depth, int depth_limit, ArrayList<ArrayList<String>> all_possible_actions, ArrayList<Double> current_hand_quantities, 
			ArrayList<Double> opposition_hand_quantities, int unknown_card_total, ArrayList<Double> current_market_quantities, int current_player_score, 
			int current_opposing_score, ArrayList<Integer> diamond_token_total, ArrayList<Integer> gold_token_total, ArrayList<Integer> silver_token_total, 
			ArrayList<Integer> cloth_token_total, ArrayList<Integer> spice_token_total, ArrayList<Integer> leather_token_total, ArrayList<String> sold_cards, 
			int current_cards_remaining, double hand_k1, double hand_k2, double hand_k3, double market_k1, double market_k2, double market_k3,
			ArrayList<Double> initial_remaining_probabilities ) {
		
		//only search further if depth limit has not been reached
		if (current_depth != depth_limit) {
			
			//set initial node value unrealistically low to be updated
			double node_value = -9999999;
		
			//loop over all possible moves for this specific node
			for (int i = 0; i < all_possible_actions.size(); i++) {
				
				//clone all input parameters to new lists and variables to avoid unavoidable bugs
				
				ArrayList<String> current_hand = new ArrayList<String>();
				ArrayList<String> current_goods_hand = new ArrayList<String>();
				ArrayList<String> current_camels_hand = new ArrayList<String>();
				ArrayList<String> current_market = new ArrayList<String>();
				
				ArrayList<Double> opposition_next_hand_quantities = new ArrayList<Double>();
				ArrayList<Double> next_market_quantities = new ArrayList<Double>();
				ArrayList<Double> next_market_quantities_with_prob = new ArrayList<Double>();

				ArrayList<Double> current_hand_quantities_temp = new ArrayList<Double>();
				current_hand_quantities_temp.addAll(current_hand_quantities);
								
				ArrayList<Double> current_market_quantities_temp = this.truncate_market_probabilities(current_market_quantities);
												
				ArrayList<Double> opposition_hand_estimate = new ArrayList<Double>();
				opposition_hand_estimate.addAll(opposition_hand_quantities);
				
				ArrayList<Integer> next_diamond_token_total = new ArrayList<Integer>();
				next_diamond_token_total.addAll(diamond_token_total);
				
				ArrayList<Integer> next_gold_token_total = new ArrayList<Integer>();
				next_gold_token_total.addAll(gold_token_total);
				
				ArrayList<Integer> next_silver_token_total = new ArrayList<Integer>();
				next_silver_token_total.addAll(silver_token_total);

				ArrayList<Integer> next_cloth_token_total = new ArrayList<Integer>();
				next_cloth_token_total.addAll(cloth_token_total);

				ArrayList<Integer> next_spice_token_total = new ArrayList<Integer>();
				next_spice_token_total.addAll(spice_token_total);

				ArrayList<Integer> next_leather_token_total = new ArrayList<Integer>();
				next_leather_token_total.addAll(leather_token_total);
				
				ArrayList<String> next_sold_cards = new ArrayList<String>();
				next_sold_cards.addAll(sold_cards);
				
				int next_opposing_score = current_opposing_score, next_cards_remaining = current_cards_remaining;
				
				//find next action
				ArrayList<String> next_action = all_possible_actions.get(i);
				
				//find all the cards within the current players goods and camels hand, and also the market place
				current_hand = this.convert_card_quantities_to_string_names(current_hand_quantities);
				current_goods_hand = this.retrive_goods_from_goods_and_camels(current_hand);
				current_camels_hand = this.retrive_camels_from_goods_and_camels(current_hand);
				current_market = this.convert_card_quantities_to_string_names(current_market_quantities_temp);
				
				//find all new cards quantities of the next hand
				opposition_next_hand_quantities = this.get_next_hand_quantities(opposition_hand_estimate, current_market_quantities, next_action);
				
				//find all new card quantities of the next market
				next_market_quantities = this.get_next_market_quantities(current_market_quantities, next_action);
				
				next_market_quantities_with_prob = this.get_next_market_quantities_with_prob(next_market_quantities, next_action, current_market, 
						current_goods_hand, current_camels_hand, sold_cards);
				
				//find new current player's score, if sale takes place
				next_opposing_score = this.update_current_player_score(current_opposing_score, next_action, diamond_token_total, gold_token_total, 
						silver_token_total, cloth_token_total, spice_token_total, leather_token_total);
				
				//find all new token totals
				if (next_action.get(0).equals("sell_cards")) {
					
					if (next_action.get(1).equals("D")) {
						next_diamond_token_total = this.update_current_goods_tokens(diamond_token_total, next_action);
					}
					
					if (next_action.get(1).equals("G")) {
						next_gold_token_total = this.update_current_goods_tokens(gold_token_total, next_action);
					}
					
					if (next_action.get(1).equals("Sil")) {
						next_silver_token_total = this.update_current_goods_tokens(silver_token_total, next_action);
					}

					if (next_action.get(1).equals("Clo")) {
						next_cloth_token_total = this.update_current_goods_tokens(cloth_token_total, next_action);
					}
					
					if (next_action.get(1).equals("Spi")) {
						next_spice_token_total = this.update_current_goods_tokens(spice_token_total, next_action);
					}
					
					if (next_action.get(1).equals("L")) {
						next_leather_token_total = this.update_current_goods_tokens(leather_token_total, next_action);
					}
				}
				
				//find all cards newly sold
				next_sold_cards = this.update_cards_sold(sold_cards, next_action);
				
				//find the new card total remaining in the deck
				next_cards_remaining = this.update_cards_remaining(current_cards_remaining, next_action, current_market);
								
				//find convert next market cards and current hand to string representations
				ArrayList<Double> next_market_cards_truncated = this.truncate_market_probabilities(next_market_quantities_with_prob);
				ArrayList<String> next_market_cards = this.convert_card_quantities_to_string_names(next_market_cards_truncated);
				ArrayList<String> current_goods_and_camels_cards = this.convert_card_quantities_to_string_names(current_hand_quantities_temp);
				ArrayList<String> current_goods_cards = this.retrive_goods_from_goods_and_camels(current_goods_and_camels_cards);
				ArrayList<String> current_camels_cards = this.retrive_camels_from_goods_and_camels(current_goods_and_camels_cards);

				//find all possible actions the current player can take in the next turn
				ArrayList<ArrayList<String>> all_possible_actions_next_player = this.get_all_possible_moves(next_market_cards, 
						current_goods_cards, current_camels_cards);
								
				//maximise the current node value
				node_value = java.lang.Math.max(node_value, this.MIN_NODE_VALUE(current_depth + 1, depth_limit, all_possible_actions_next_player, 
						current_hand_quantities, opposition_next_hand_quantities, unknown_card_total, next_market_quantities_with_prob, current_player_score, 
						next_opposing_score, next_diamond_token_total, next_gold_token_total, next_silver_token_total, next_cloth_token_total, 
						next_spice_token_total, next_leather_token_total, next_sold_cards, next_cards_remaining, hand_k1, hand_k2, hand_k3, market_k1, 
						market_k2, market_k3, initial_remaining_probabilities));
			}
			
			return node_value;
		}
		
		//if depth limit has been reached, find maximum heuristic value
		else {
			
			double max_state_value = -999999;
			
			//loop over all possible moves for this specific node
			for (int i = 0; i < all_possible_actions.size(); i++) {
				
				//find next possible action
				ArrayList<String> next_possible_action = all_possible_actions.get(i);

				//find next hand and market quantities
				ArrayList<Double> hand_quantities = this.get_next_hand_quantities(opposition_hand_quantities, current_market_quantities, next_possible_action);
				ArrayList<Double> new_market_quantities = this.truncate_market_probabilities(current_market_quantities);
				ArrayList<Double> market_quantities = this.get_next_market_quantities(current_market_quantities, next_possible_action);

				//convert market and hand quantities to string representations
				ArrayList<String> current_hand = this.convert_card_quantities_to_string_names(opposition_hand_quantities);
				ArrayList<String> current_goods_hand = this.retrive_goods_from_goods_and_camels(current_hand);
				ArrayList<String> current_camels_hand = this.retrive_camels_from_goods_and_camels(current_hand);
				ArrayList<String> current_market = this.convert_card_quantities_to_string_names(new_market_quantities);

				//add market probability
				ArrayList<Double> market_quantities_with_prob = this.get_next_market_quantities_with_prob(market_quantities, next_possible_action, 
						current_market, current_goods_hand, current_camels_hand, sold_cards);

				//find next total state value
				double next_total_state_value = this.get_heuristic_state_value(hand_quantities, 
						market_quantities_with_prob, next_possible_action, diamond_token_total, gold_token_total, silver_token_total, cloth_token_total, 
						spice_token_total, leather_token_total, hand_k1, hand_k2, hand_k3, market_k1, market_k2, market_k3);

				//find highest state value
				if (next_total_state_value > max_state_value) {
					max_state_value = next_total_state_value;
				}
			}
			
			//add state value to current score
			max_state_value += current_opposing_score;			

			return max_state_value;
		}	
	}
	
	//method to find the MIN node value, or find current player's highest state value if depth has been reached
	public double MIN_NODE_VALUE(int current_depth, int depth_limit, ArrayList<ArrayList<String>> all_possible_actions, ArrayList<Double> current_hand_quantities, 
			ArrayList<Double> opposition_hand_quantities, int unknown_card_total, ArrayList<Double> current_market_quantities, int current_player_score, 
			int current_opposing_score, ArrayList<Integer> diamond_token_total, ArrayList<Integer> gold_token_total, ArrayList<Integer> silver_token_total, 
			ArrayList<Integer> cloth_token_total, ArrayList<Integer> spice_token_total, ArrayList<Integer> leather_token_total, ArrayList<String> sold_cards, 
			int current_cards_remaining, double hand_k1, double hand_k2, double hand_k3, double market_k1, double market_k2, double market_k3,
			ArrayList<Double> initial_remaining_probabilities) {
		
		//only search further if depth limit has not been reached
		if (current_depth != depth_limit) {
			
			//set initial node value unrealistically high to be updated
			double node_value = 9999999;
		
			//loop over all possible moves for this specific node
			for (int i = 0; i < all_possible_actions.size(); i++) {
				
				//clone all input parameters to new lists and variables to avoid unavoidable bugs
				
				ArrayList<String> current_hand = new ArrayList<String>();
				ArrayList<String> current_goods_hand = new ArrayList<String>();
				ArrayList<String> current_camels_hand = new ArrayList<String>();
				ArrayList<String> current_market = new ArrayList<String>();
				
				ArrayList<Double> next_hand_quantities = new ArrayList<Double>();
				ArrayList<Double> next_market_quantities = new ArrayList<Double>();
				ArrayList<Double> next_market_quantities_with_prob = new ArrayList<Double>();

				ArrayList<Double> current_hand_quantities_temp = new ArrayList<Double>();
				current_hand_quantities_temp.addAll(current_hand_quantities);
								
				ArrayList<Double> current_market_quantities_temp = this.truncate_market_probabilities(current_market_quantities);
				
				ArrayList<Double> opposition_hand_estimate = new ArrayList<Double>();
				opposition_hand_estimate.addAll(opposition_hand_quantities);
				
				ArrayList<Integer> next_diamond_token_total = new ArrayList<Integer>();
				next_diamond_token_total.addAll(diamond_token_total);
				
				ArrayList<Integer> next_gold_token_total = new ArrayList<Integer>();
				next_gold_token_total.addAll(gold_token_total);
				
				ArrayList<Integer> next_silver_token_total = new ArrayList<Integer>();
				next_silver_token_total.addAll(silver_token_total);

				ArrayList<Integer> next_cloth_token_total = new ArrayList<Integer>();
				next_cloth_token_total.addAll(cloth_token_total);

				ArrayList<Integer> next_spice_token_total = new ArrayList<Integer>();
				next_spice_token_total.addAll(spice_token_total);

				ArrayList<Integer> next_leather_token_total = new ArrayList<Integer>();
				next_leather_token_total.addAll(leather_token_total);
				
				ArrayList<String> next_sold_cards = new ArrayList<String>();
				next_sold_cards.addAll(sold_cards);
				
				int next_current_player_score = current_player_score, next_cards_remaining = current_cards_remaining;
				
				//find next action
				ArrayList<String> next_action = all_possible_actions.get(i);
				
				//find all the cards within the current players goods and camels hand, and also the market place
				current_hand = this.convert_card_quantities_to_string_names(current_hand_quantities);
				current_goods_hand = this.retrive_goods_from_goods_and_camels(current_hand);
				current_camels_hand = this.retrive_camels_from_goods_and_camels(current_hand);
				current_market = this.convert_card_quantities_to_string_names(current_market_quantities_temp);
				
				//find all new cards quantities of the next hand
				next_hand_quantities = this.get_next_hand_quantities(current_hand_quantities_temp, current_market_quantities_temp, next_action);
				
				//find all new card quantities of the next market
				next_market_quantities = this.get_next_market_quantities(current_market_quantities, next_action);
				
				next_market_quantities_with_prob = this.get_next_market_quantities_with_prob(next_market_quantities, next_action, current_market, 
						current_goods_hand, current_camels_hand, sold_cards);
				
				//find new current player's score, if sale takes place
				next_current_player_score = this.update_current_player_score(current_player_score, next_action, diamond_token_total, gold_token_total, 
						silver_token_total, cloth_token_total, spice_token_total, leather_token_total);
								
				//find all new token totals
				if (next_action.get(0).equals("sell_cards")) {
					
					if (next_action.get(1).equals("D")) {
						next_diamond_token_total = this.update_current_goods_tokens(diamond_token_total, next_action);
					}
					
					if (next_action.get(1).equals("G")) {
						next_gold_token_total = this.update_current_goods_tokens(gold_token_total, next_action);
					}
					
					if (next_action.get(1).equals("Sil")) {
						next_silver_token_total = this.update_current_goods_tokens(silver_token_total, next_action);
					}

					if (next_action.get(1).equals("Clo")) {
						next_cloth_token_total = this.update_current_goods_tokens(cloth_token_total, next_action);
					}
					
					if (next_action.get(1).equals("Spi")) {
						next_spice_token_total = this.update_current_goods_tokens(spice_token_total, next_action);
					}
					
					if (next_action.get(1).equals("L")) {
						next_leather_token_total = this.update_current_goods_tokens(leather_token_total, next_action);
					}
				}
				
				//find all cards newly sold
				next_sold_cards = this.update_cards_sold(sold_cards, next_action);
				
				//find the new card total remaining in the deck
				next_cards_remaining = this.update_cards_remaining(current_cards_remaining, next_action, current_market);
				
				//find convert next market cards and current hand to string representations
				ArrayList<Double> next_market_cards_truncated = this.truncate_market_probabilities(next_market_quantities_with_prob);
				ArrayList<String> next_market_cards = this.convert_card_quantities_to_string_names(next_market_cards_truncated);
				ArrayList<String> opposition_goods_and_camels_cards = this.convert_card_quantities_to_string_names(opposition_hand_estimate);
				ArrayList<String> opposition_goods_cards = this.retrive_goods_from_goods_and_camels(opposition_goods_and_camels_cards);
				ArrayList<String> opposition_camels_cards = this.retrive_camels_from_goods_and_camels(opposition_goods_and_camels_cards);

				//find all possible actions the current player can take in the next turn
				ArrayList<ArrayList<String>> all_possible_actions_next_player = this.get_all_possible_moves(next_market_cards, opposition_goods_cards, 
						opposition_camels_cards);				
				
				//minimise node value
				node_value = java.lang.Math.min(node_value, this.MAX_NODE_VALUE(current_depth + 1, depth_limit, all_possible_actions_next_player, next_hand_quantities, 
						opposition_hand_estimate, unknown_card_total, next_market_quantities_with_prob, current_opposing_score, next_current_player_score, 
						next_diamond_token_total, next_gold_token_total, next_silver_token_total, next_cloth_token_total, next_spice_token_total, next_leather_token_total, 
						next_sold_cards, next_cards_remaining, hand_k1, hand_k2, hand_k3, market_k1, market_k2, market_k3, initial_remaining_probabilities));
			}
			
			return node_value;
		}
		
		//if depth limit has been reached, find maximum heuristic value		
		else {
				
			double max_state_value = -999999;
			
			//loop over all possible moves for this specific node
			for (int i = 0; i < all_possible_actions.size(); i++) {
				
				//find next possible action
				ArrayList<String> next_possible_action = all_possible_actions.get(i);

				//find next hand and market quantities
				ArrayList<Double> hand_quantities = this.get_next_hand_quantities(current_hand_quantities, current_market_quantities, next_possible_action);			
				ArrayList<Double> new_market_quantities = this.truncate_market_probabilities(current_market_quantities);
				ArrayList<Double> market_quantities = this.get_next_market_quantities(current_market_quantities, next_possible_action);

				//convert market and hand quantities to string representations
				ArrayList<String> current_hand = this.convert_card_quantities_to_string_names(current_hand_quantities);
				ArrayList<String> current_goods_hand = this.retrive_goods_from_goods_and_camels(current_hand);
				ArrayList<String> current_camels_hand = this.retrive_camels_from_goods_and_camels(current_hand);
				ArrayList<String> current_market = this.convert_card_quantities_to_string_names(new_market_quantities);

				//add market probability
				ArrayList<Double> market_quantities_with_prob = this.get_next_market_quantities_with_prob(market_quantities, next_possible_action, 
						current_market, current_goods_hand, current_camels_hand, sold_cards);

				//find next total state value
				double next_total_state_value = this.get_heuristic_state_value(hand_quantities, 
						market_quantities_with_prob, next_possible_action, diamond_token_total, gold_token_total, silver_token_total, cloth_token_total, 
						spice_token_total, leather_token_total, hand_k1, hand_k2, hand_k3, market_k1, market_k2, market_k3);

				//find highest state value
				if (next_total_state_value > max_state_value) {
					max_state_value = next_total_state_value;
				}
			}
			
			//add state value to current score
			max_state_value += current_player_score;
			
			return max_state_value;
		}
	}
	
	//methods that take basic approaches in selecting the next action
	
	//method to return a random possible action to take, given a specific state
	public ArrayList<String> next_action_random(ArrayList<ArrayList<String>> all_possible_actions) {
		
		ArrayList<String> next_action = new ArrayList<String>();
		
		//obtaining random index
		Random random_num = new Random();
		int random_index = random_num.nextInt(all_possible_actions.size());
		
		//obtaining random next action
		next_action = all_possible_actions.get(random_index);
		
		return next_action;
	}
	
	//method to return the best selling action, if no selling actions can occur a random action is chosen instead
	public ArrayList<String> next_action_greedy_random(ArrayList<ArrayList<String>> all_possible_actions, ArrayList<Integer> current_diamond_tokens, 
			ArrayList<Integer> current_gold_tokens, ArrayList<Integer> current_silver_tokens, ArrayList<Integer> current_cloth_tokens, 
			ArrayList<Integer> current_spice_tokens, ArrayList<Integer> current_leather_tokens) {
		
		ArrayList<String> next_action = new ArrayList<String>();
		
		int highest_sale = 0, highest_index_sale = 0;

		//looping over all possible actions
		for (int i = 0; i < all_possible_actions.size(); i++) {
			
			int points_gained_in_sale = 0;
			
			//checking if an action is selling cards
			next_action = all_possible_actions.get(i);
			
			if (next_action.get(0).equals("sell_cards")) {
				
				String current_goods_card = "";
				int index = 1;
				
				//loop over action to find price of a sale
				while (!current_goods_card.equals("--")) {
					
					current_goods_card = next_action.get(index);
					
					//find value of sale if diamond cards were being sold
				    if (current_diamond_tokens.size() != 0 && current_goods_card.equals("D")) {
						int chosen_token = current_diamond_tokens.get(0);
						current_diamond_tokens.remove(0);
						points_gained_in_sale += chosen_token;
				    }
				    
					//find value of sale if gold cards were being sold
				    if (current_gold_tokens.size() != 0 && current_goods_card.equals("G")) {
						int chosen_token = current_gold_tokens.get(0);
						current_gold_tokens.remove(0);
						points_gained_in_sale += chosen_token;
				    }
				    
					//find value of sale if silver cards were being sold
				    if (current_silver_tokens.size() != 0 && current_goods_card.equals("Sil")) {
						int chosen_token = current_silver_tokens.get(0);
						current_silver_tokens.remove(0);
						points_gained_in_sale += chosen_token;
				    }

					//find value of sale if cloth cards were being sold
				    if (current_cloth_tokens.size() != 0 && current_goods_card.equals("Clo")) {
						int chosen_token = current_cloth_tokens.get(0);
						current_cloth_tokens.remove(0);
						points_gained_in_sale += chosen_token;
				    }
				    
					//find value of sale if spice cards were being sold
				    if (current_spice_tokens.size() != 0 && current_goods_card.equals("Spi")) {
						int chosen_token = current_spice_tokens.get(0);
						current_spice_tokens.remove(0);
						points_gained_in_sale += chosen_token;
				    }
				    
					//find value of sale if leather cards were being sold
				    if (current_leather_tokens.size() != 0 && current_goods_card.equals("L")) {
						int chosen_token = current_leather_tokens.get(0);
						current_leather_tokens.remove(0);
						points_gained_in_sale += chosen_token;
				    }

					index++;
				}
				
				//add x3 multiplier average if there are three cards present
				if (next_action.size() == 5) {
					points_gained_in_sale += 2;

				}
				
				//add x4 multiplier average if there are three cards present
				if (next_action.size() == 6) {
					points_gained_in_sale += 5;

				}
				
				//add x5 multiplier average if there are three cards present
				if (next_action.size() >= 7) {
					points_gained_in_sale += 9;
				}

				//finds highest sale in all possible actions
				if (points_gained_in_sale > highest_sale) {
					highest_sale = points_gained_in_sale;
					highest_index_sale = i;
				}
			}		
		}

		//if there are no sale actions, a random action is selected
		if (highest_sale == 0) {
			next_action = this.next_action_random(all_possible_actions);
		}
	
		//else, next action is the selling action that gains the player
		//the most points
		else {
			next_action = all_possible_actions.get(highest_index_sale);
		}
		
		return next_action;
	}

	//method to return the best selling action, if no selling actions can occur then the action that gives the most valued next hand is
	//chosen, also if permitted (and if possible) the agent will pick up camel cards every 5 turns
	public ArrayList<String> next_action_greedy(ArrayList<Double> current_hand_quantities, ArrayList<Double> current_market_quantities, 
			ArrayList<ArrayList<String>> all_possible_actions, ArrayList<Integer> current_diamond_tokens, 
			ArrayList<Integer> current_gold_tokens, ArrayList<Integer> current_silver_tokens, ArrayList<Integer> current_cloth_tokens, 
			ArrayList<Integer> current_spice_tokens, ArrayList<Integer> current_leather_tokens, int turn_num) {
		
		ArrayList<String> next_action = new ArrayList<String>();
		boolean contains_sale_action = false;
		
		//loops over all possible actions to find if any actions are sales
		for (int i = 0; i < all_possible_actions.size(); i++) {
			
			next_action = all_possible_actions.get(i);
			
			//if an action is found which is a sale, a flag is raised
			if (next_action.get(0).equals("sell_cards")) {
				
				contains_sale_action = true;
				break;
			}
		}
		
		//if all possible actions contains a sale action, the one with most profit is chosen
		if (contains_sale_action) {
			next_action = this.next_action_greedy_random(all_possible_actions, current_diamond_tokens, current_gold_tokens, 
					current_silver_tokens, current_cloth_tokens, current_spice_tokens, current_leather_tokens);
		}
		
		//if no sale actions are found, then a search for a valuable action in either trading or taking one good
		else {
			
			ArrayList<Double> next_hand_quantities = new ArrayList<Double>();
			
			double best_selling_value = 0, next_goods_value = 0;
			int best_selling_value_index = 0;
			
			//loops over all possible actions to find best possible trade or taking one good
			for (int i = 0; i < all_possible_actions.size(); i++) {
				
				//finds next possible action
				next_action = all_possible_actions.get(i);
				
				//gets next hand quantities given the current hand, market and next action chosen 
				next_hand_quantities = this.get_next_hand_quantities(current_hand_quantities, current_market_quantities, next_action);
			
				//finds absolute value of next hand based on the goods found and the tokens left
				next_goods_value = this.get_total_card_value(next_hand_quantities, current_diamond_tokens, current_gold_tokens, 
					current_silver_tokens, current_cloth_tokens, current_spice_tokens, 
					current_leather_tokens, 1, 1, 0);
				
				//updates best selling value if one is found
				if (best_selling_value < next_goods_value) {
					
					best_selling_value = next_goods_value;
					best_selling_value_index = i;
				}
			}
			
			//next possible action is chosen based on the one that makes the next hand have most valued goods
			next_action = all_possible_actions.get(best_selling_value_index);
		}
		
		//if permitted and if the turn number is a multiple of 5 then the search for the take camels action is held
		if (turn_num % 5 == 0) {

			//if the 'take_camels' action is possible, it is chosen for the next action, regardless of any other
			//action previously selected
			if (all_possible_actions.get(0).get(0).equals("take_camels")) {
				
				next_action = all_possible_actions.get(0);
			}
		}
		
		return next_action; 
	}
	
	//method to return action using minimax algorithm using a pre-selected depth level (if all opposite player's cards are known). If three or more cards of 
	//the same type are in the current hand, 7 goods cards in total or not all other players cards are known then it will only look down one level
	public ArrayList<String> next_action_minimax(int depth_limit, ArrayList<ArrayList<String>> all_possible_actions, ArrayList<Double> current_hand_quantities,
			ArrayList<Double> opposition_hand_quantities, int unknown_card_total, ArrayList<Double> current_market_quantities, int current_player_score, 
			int opposing_player_score, ArrayList<Integer> current_diamond_tokens, ArrayList<Integer> current_gold_tokens, ArrayList<Integer> current_silver_tokens, 
			ArrayList<Integer> current_cloth_tokens, ArrayList<Integer> current_spice_tokens, ArrayList<Integer> current_leather_tokens, ArrayList<String> sold_cards,
			int total_cards_remaining, ArrayList<Double> remaining_card_probabilities) {
		
		ArrayList<String> next_action = new ArrayList<String>();
		boolean card_type_three_or_more = false;
		int total_cards_in_hand = 0;

		//only explore deeper levels of game tree if all other player's cards are known
		//(usually around turn 3 to 8)
		if (unknown_card_total == 0) {
			
			//loop over all current goods quantities to find if there are 3 or more cards of the same type
			//and to find the total goods cards in the current hand
			for (int i = 0; i < 6; i++) {
				
				if (current_hand_quantities.get(i) >= 3) {
					card_type_three_or_more = true;
				}
				
				total_cards_in_hand += current_hand_quantities.get(i); 
			}
			
			//if there are 3 or more cards of the same type or 7 goods cards in total, it only looks down one level (takes greedy action)
			if (card_type_three_or_more || total_cards_in_hand == 7) {
				
				next_action = this.MINIMAX_NEXT_ACTION(0, 0, all_possible_actions, current_hand_quantities, opposition_hand_quantities, unknown_card_total, 
						current_market_quantities, current_player_score, opposing_player_score, current_diamond_tokens, current_gold_tokens, current_silver_tokens, 
						current_cloth_tokens,  current_spice_tokens,  current_leather_tokens, sold_cards, total_cards_remaining, selected_hand_k1, selected_hand_k2, 
						selected_hand_k3, selected_market_k1, selected_market_k2, selected_market_k3, remaining_card_probabilities);
			}
			
			//if there are not, then it will search down the game tree to a pre-selected depth
			else {	
				
				next_action = this.MINIMAX_NEXT_ACTION(0, depth_limit, all_possible_actions, current_hand_quantities, opposition_hand_quantities, unknown_card_total, 
						current_market_quantities, current_player_score, opposing_player_score, current_diamond_tokens, current_gold_tokens, current_silver_tokens, 
						current_cloth_tokens,  current_spice_tokens,  current_leather_tokens, sold_cards, total_cards_remaining, selected_hand_k1, selected_hand_k2, 
						selected_hand_k3, selected_market_k1, selected_market_k2, selected_market_k3, remaining_card_probabilities);
			}
		}
		
		//if not all other player's cards are known, it will not explore the game tree further than one level
		else {
			
			next_action = this.MINIMAX_NEXT_ACTION(0, 0, all_possible_actions, current_hand_quantities, opposition_hand_quantities, unknown_card_total, 
					current_market_quantities, current_player_score, opposing_player_score, current_diamond_tokens, current_gold_tokens, current_silver_tokens, 
					current_cloth_tokens,  current_spice_tokens,  current_leather_tokens, sold_cards, total_cards_remaining, selected_hand_k1, selected_hand_k2, 
					selected_hand_k3, selected_market_k1, selected_market_k2, selected_market_k3, remaining_card_probabilities);
		}
		
		return next_action;
	}
	
	
	//methods to use specific strategies when selecting the next move in the game
	
	//strategies that use basic approaches
	
	//strategy 1 uses a completely random approach when selecting the next move
	public ArrayList<String> strategy_1_next_move(ArrayList<ArrayList<String>> all_possible_actions) {
		
		ArrayList<String> next_move = this.next_action_random(all_possible_actions);

		return next_move;
	}
	
	//strategy 2 sells the highest selling action, if selling isn't possible it selects the next action randomly
	public ArrayList<String> strategy_2_next_move(ArrayList<ArrayList<String>> all_possible_actions, ArrayList<Integer> current_diamond_tokens, 
			ArrayList<Integer> current_gold_tokens, ArrayList<Integer> current_silver_tokens, ArrayList<Integer> current_cloth_tokens, 
			ArrayList<Integer> current_spice_tokens, ArrayList<Integer> current_leather_tokens) {
		
		ArrayList<String> next_move = this.next_action_greedy_random(all_possible_actions, current_diamond_tokens, current_gold_tokens, 
				current_silver_tokens, current_cloth_tokens, current_spice_tokens, current_leather_tokens);

		return next_move;
	}
 
	//strategy 3 sells the highest selling action, if selling isn't possible it selects the next action that will maximise the overall goods value
	//in the hand, also if prompted and if possible camels are taken every 5 turns
	public ArrayList<String> strategy_3_next_move(ArrayList<Double> current_hand_quantities, ArrayList<Double> current_market_quantities, 
			ArrayList<ArrayList<String>> all_possible_actions, ArrayList<Integer> current_diamond_tokens, ArrayList<Integer> current_gold_tokens, 
			ArrayList<Integer> current_silver_tokens, ArrayList<Integer> current_cloth_tokens, ArrayList<Integer> current_spice_tokens, 
			ArrayList<Integer> current_leather_tokens, int turn_num) {
		
		ArrayList<String> next_move = this.next_action_greedy(current_hand_quantities, current_market_quantities, all_possible_actions, 
				current_diamond_tokens, current_gold_tokens, current_silver_tokens, current_cloth_tokens, current_spice_tokens, 
				current_leather_tokens, turn_num);

		return next_move;
	}
	
	//strategies that use heuristic based and minimax approaches
	
	//strategy 4 looks as far as one level down the minimax tree, using pre-selected heuristic parameters
	public ArrayList<String> strategy_4_next_move(ArrayList<ArrayList<String>> all_possible_actions, ArrayList<Double> current_hand_quantities,
			ArrayList<Double> opposition_hand_quantities, int unknown_card_total, ArrayList<Double> current_market_quantities, int current_player_score, 
			int opposing_player_score, ArrayList<Integer> current_diamond_tokens, ArrayList<Integer> current_gold_tokens, ArrayList<Integer> current_silver_tokens, 
			ArrayList<Integer> current_cloth_tokens, ArrayList<Integer> current_spice_tokens, ArrayList<Integer> current_leather_tokens, ArrayList<String> sold_cards,
			int total_cards_remaining, ArrayList<Double> remaining_card_probabilities) {
		
		ArrayList<String> next_move = next_action_minimax(0, all_possible_actions, current_hand_quantities, opposition_hand_quantities, unknown_card_total, 
			current_market_quantities, current_player_score, opposing_player_score, current_diamond_tokens, current_gold_tokens, current_silver_tokens, 
			current_cloth_tokens, current_spice_tokens, current_leather_tokens, sold_cards, total_cards_remaining, remaining_card_probabilities);
		
		return next_move;
	}
	
	//strategy 5 looks as far as three levels down the minimax tree, using pre-selected heuristic parameters (given there are no unknown cards and there
	//are no card type quantities greater than 3 or 7 goods cards in total, if this is the case it looks down the tree one level).
	public ArrayList<String> strategy_5_next_move(ArrayList<ArrayList<String>> all_possible_actions, ArrayList<Double> current_hand_quantities,
			ArrayList<Double> opposition_hand_quantities, int unknown_card_total, ArrayList<Double> current_market_quantities, int current_player_score, 
			int opposing_player_score, ArrayList<Integer> current_diamond_tokens, ArrayList<Integer> current_gold_tokens, ArrayList<Integer> current_silver_tokens, 
			ArrayList<Integer> current_cloth_tokens, ArrayList<Integer> current_spice_tokens, ArrayList<Integer> current_leather_tokens, ArrayList<String> sold_cards,
			int total_cards_remaining, ArrayList<Double> remaining_card_probabilities) {
		
		ArrayList<String> next_move = next_action_minimax(2, all_possible_actions, current_hand_quantities, opposition_hand_quantities, unknown_card_total, 
			current_market_quantities, current_player_score, opposing_player_score, current_diamond_tokens, current_gold_tokens, current_silver_tokens, 
			current_cloth_tokens, current_spice_tokens, current_leather_tokens, sold_cards, total_cards_remaining, remaining_card_probabilities);
		
		return next_move;
	}
	
	//strategy 6 looks as far as five levels down the minimax tree, using pre-selected heuristic parameters (given there are no unknown cards and there
	//are no card type quantities greater than 3 or 7 goods cards in total, if this is the case it looks down the tree one level).
	public ArrayList<String> strategy_6_next_move(ArrayList<ArrayList<String>> all_possible_actions, ArrayList<Double> current_hand_quantities,
			ArrayList<Double> opposition_hand_quantities, int unknown_card_total, ArrayList<Double> current_market_quantities, int current_player_score, 
			int opposing_player_score, ArrayList<Integer> current_diamond_tokens, ArrayList<Integer> current_gold_tokens, ArrayList<Integer> current_silver_tokens, 
			ArrayList<Integer> current_cloth_tokens, ArrayList<Integer> current_spice_tokens, ArrayList<Integer> current_leather_tokens, ArrayList<String> sold_cards,
			int total_cards_remaining, ArrayList<Double> remaining_card_probabilities) {
		
		ArrayList<String> next_move = next_action_minimax(4, all_possible_actions, current_hand_quantities, opposition_hand_quantities, unknown_card_total, 
			current_market_quantities, current_player_score, opposing_player_score, current_diamond_tokens, current_gold_tokens, current_silver_tokens, 
			current_cloth_tokens, current_spice_tokens, current_leather_tokens, sold_cards, total_cards_remaining, remaining_card_probabilities);
		
		return next_move;
	}
}