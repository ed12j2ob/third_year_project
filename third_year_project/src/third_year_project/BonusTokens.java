package third_year_project;

import java.util.ArrayList;
import java.util.Collections;

public class BonusTokens {
	
	private ArrayList<Integer> five_multiplier_tokens;
	private ArrayList<Integer> four_multiplier_tokens;
	private ArrayList<Integer> three_multiplier_tokens;
	
	//constructor
	public BonusTokens() {
		
		five_multiplier_tokens = new ArrayList<Integer>();
		four_multiplier_tokens = new ArrayList<Integer>();
		three_multiplier_tokens = new ArrayList<Integer>();
		
		//adding all relevant points to the tokens, in the correct quantities
		five_multiplier_tokens.add(9);
		
		for (int i = 0; i < 2; i++) {
			five_multiplier_tokens.add(10);
			five_multiplier_tokens.add(8);
			
			four_multiplier_tokens.add(6);
			four_multiplier_tokens.add(5);
			four_multiplier_tokens.add(4);
			
			three_multiplier_tokens.add(3);
			three_multiplier_tokens.add(1);
		}
		
		for (int i = 0; i < 3; i++) {
			three_multiplier_tokens.add(2);
		}
		
		//shuffling the bonus tokens as per required in the initial game setup
		Collections.shuffle(three_multiplier_tokens);
		Collections.shuffle(four_multiplier_tokens);
		Collections.shuffle(five_multiplier_tokens);
	}
	
	//method to output all remaining bonus tokens (not to be used in final game)
	public void print_bonus_tokens() {
		
		System.out.print("(x5): ");
		for (int i = 0; i < five_multiplier_tokens.size(); i++) {
			System.out.print(five_multiplier_tokens.get(i) + " ");
		}
		
		System.out.print("\n(x4): ");
		for (int i = 0; i < four_multiplier_tokens.size(); i++) {
			System.out.print(four_multiplier_tokens.get(i) + " ");
		}
		
		System.out.print("\n(x3): ");
		for (int i = 0; i < three_multiplier_tokens.size(); i++) {
			System.out.print(three_multiplier_tokens.get(i) + " ");
		}
		System.out.print("\n");
	}
	
	//method to output how many bonus tokens are left of each type
	public void print_bonus_token_quantities() {
		
		System.out.println("(x5): " + five_multiplier_tokens.size() + " left");
		System.out.println("(x4): " + four_multiplier_tokens.size() + " left");
		System.out.println("(x3): " + three_multiplier_tokens.size() + " left");
	}
	
	//methods to retrieve all tokens of a specific type
	
	public ArrayList<Integer> get_five_multiplier_tokens() {
		
		return five_multiplier_tokens;
	}
	
	public ArrayList<Integer> get_four_multiplier_tokens() {
		
		return four_multiplier_tokens;
	}
	
	public ArrayList<Integer> get_three_multiplier_tokens() {
		
		return three_multiplier_tokens;
	}

	//methods to take one token of a specific bonus type
	
	public int take_one_five_multiplier() {
		
		//if no five multiplier tokens left, give no extra points
	    if (five_multiplier_tokens.size() == 0) {
	    	return 0;
	    }
	    
	    //else, take the first five multiplier token, and give the extra points
	    else {
			int chosen_token = five_multiplier_tokens.get(0);
			five_multiplier_tokens.remove(0);
			return chosen_token;
	    }
	}
	
	public int take_one_four_multiplier() {
		
		//if no four multiplier tokens left, give no extra points
	    if (four_multiplier_tokens.size() == 0) {
	    	return 0;
	    }
	    
	    //else, take the first four multiplier token, and give the extra points
	    else {
			int chosen_token = four_multiplier_tokens.get(0);
			four_multiplier_tokens.remove(0);
			return chosen_token;
	    }
	}
	
	public int take_one_three_multiplier() {
		
		//if no three multiplier tokens left, give no extra points
	    if (three_multiplier_tokens.size() == 0) {
	    	return 0;
	    }
	    
	    //else, take the first three multiplier token, and give the extra points
	    else {
			int chosen_token = three_multiplier_tokens.get(0);
			three_multiplier_tokens.remove(0);
			return chosen_token;
	    }
	}
}