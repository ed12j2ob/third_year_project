package third_year_project;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class TextInterface {
	
	//default constructor
	public TextInterface() {
	}
	
	//method to avoid bugs when exchanging cards
	public boolean is_valid_card(String card_type) {
		
		//cards only valid if they are of the following types: D, G, Sil, Clo, Spi, L, Cam
		if (card_type.equals("D") || card_type.equals("G") ||card_type.equals("Sil") || card_type.equals("Clo") 
				|| card_type.equals("Spi") || card_type.equals("L") || card_type.equals("Cam")) {
			
			return true;
		}
		
		else {
			return false;
		}
	}
	
	//method to take user input to determine what the next move is
	public String choose_next_move() {
		
		//prompts user to enter their next move
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Enter your next move (type 'h' for help): ");
		String move = scanner.next();

		//returns an appropriate string, depending on action given
		
		if (move.equals("take_c")) {
			return "take_camels";
		}
		
		if (move.equals("take_g")) {
			return "take_one_good";
		}
		
		if (move.equals("trade")) {
			return "trade_cards";
		}
		
		if (move.equals("sell")) {
			return "sell_cards";
		}
		
		if (move.equals("list_sc")) {
			return "list_sold_cards";
		}
		
		if (move.equals("list_pm")) {
			return "list_previous_moves";
		}
		
		if (move.equals("pt")) {
			return "print_table";
		}
		
		if (move.equals("h")) {
			System.out.println("\nINFO:\n'take_c' = take all camels\n"
					+ "'take_g' = take one goods card\n'trade' = trade two or more cards"
					+ "\n'sell' = sell one or more cards\n'list_sc' = list all sold cards"
					+ "\n'list_pm = list all previous moves\n'pt' = print table\n");
			
			return "help";
		}
		
		else {
			
			//returns error string if invalid move was given
			System.out.println("Invalid Action. Type 'h' to find valid actions\n");
			
			return "error";
		}
	}
	
	//method to establish if the 'take all camels' action is possible
	public boolean can_take_all_camels(ArrayList<String> current_market_place) {
		
		int camel_total = 0;
		
		//loops over market to find total number of camels
		for (int i = 0; i < current_market_place.size(); i++) {
			if(current_market_place.get(i).equals("Cam")) {
				camel_total++;
			}
		}
		
		//if there is one or more, the 'take all camels' action is possible
		if (camel_total > 0) {
			return true;
		}
		
		//else an error message is output on the console
		else {
			System.out.println("Currently there are no camels to take from the market place.\n");
			return false;
		}
	}
	
	//method to establish how many camels were taken in the previous move
	public int take_all_camels_total(ArrayList<String> current_market_place) {
		
		if (!this.can_take_all_camels(current_market_place)) {
			return 0;
		}
		
		else {
			
			int camel_total = 0;
			
			//loops over market to find total number of camels
			for (int i = 0; i < current_market_place.size(); i++) {
				if(current_market_place.get(i).equals("Cam")) {
					camel_total++;
				}
			}
			
			return camel_total;
		}
	}
	
	//method to establish if the 'take one goods card' action is possible
	public boolean can_take_one_goods_card(String card, ArrayList<String> current_market_place, 
			ArrayList<String> current_goods_hand) {
		
		//tests if card given is valid
		if (!is_valid_card(card)) {
			System.out.println("ERROR: The card chosen ('" + card + "') is not a valid card. It "
					+ "must be one of the following: D, G, Sil, Clo, Spi, L\n");
			return false;
		}
		
		//tests if maximum hand capacity has been reached
		if (current_goods_hand.size() == 7) {
			System.out.println("ERROR: No more goods cards may be taken. A player can only have up "
					+ "to 7 goods cards at any one given time.\n");
			return false;
		}
		
		//tests if card chosen was a camel card
		if (card.equals("Cam")) {
			System.out.println("ERROR: Camel cards are not goods cards.\n");
			return false;
		}
		
		//else finds card by looping over market
		else {
			for (int i = 0; i < current_market_place.size(); i++) {
				if (current_market_place.get(i).equals(card)) {
					return true;
				}
			}
			
			//otherwise returns error if card cannot be found
			System.out.println("ERROR: The goods card chosen ('" + card + "') cannot be found within the "
					+ "market place.\n");
			return false;
		}
	}
	
	//method to establish if the 'trade_cards' action is possible
	public boolean can_trade_cards(ArrayList<String> selected_cards, 
			ArrayList<String> selected_market_cards, ArrayList<String> current_market_place, 
			ArrayList<String> current_goods_hand, ArrayList<String> current_camel_hand) {
				
		//selected cards must be valid
		for (int i = 0; i < selected_cards.size(); i++) {
			if (!is_valid_card(selected_cards.get(i))) {
				System.out.println("ERROR: One card chosen ('" + selected_cards.get(i) + "') from your "
						+ "current hand is not a valid card. Each card must be one of the following: D, G, "
						+ "Sil, Clo, Spi, L, Cam\n");
				return false;
			}
		}
		
		//cards selected from the market must all be valid
		for (int i = 0; i < selected_market_cards.size(); i++) {
			if (!is_valid_card(selected_market_cards.get(i))) {
				System.out.println("ERROR: One card chosen ('" + selected_market_cards.get(i) + "') from the "
						+ "market place is not a valid card. Each card must be one of the following: D, G, Sil, "
						+ "Clo, Spi, L\n");
				return false;
			}
		}
		
		//selected cards have to be of equal length
		if (selected_cards.size() != selected_market_cards.size()) {
			System.out.println("ERROR: Cards selected from your current hand ('" + selected_cards + "') and from "
					+ "the market place ('" + selected_market_cards + "') must be of equal quantity.\n");
			return false;
		}
		
		//selected cards have to be greater than 1 in quantity
		if (selected_cards.size() < 2 || selected_market_cards.size() < 2) {
			System.out.println("ERROR: Cards selected from both your current hand and the market place must at "
					+ "least have a quantity of 2.\n");
			return false;
		}
				
		//selected market cards cannot be camels
		for (int i = 0; i < selected_market_cards.size(); i++) {
			if (selected_market_cards.get(i).equals("Cam")) {
				System.out.println("ERROR: Cards selected from the market place ('" + selected_market_cards 
						+ "') cannot be camel cards.\n");
				return false;
			}
		}
				
		ArrayList<String> all_current_cards = new ArrayList<String>();
		all_current_cards.addAll(current_goods_hand);
		all_current_cards.addAll(current_camel_hand);		
		
		//selected goods cards must already be in goods hand
		if (!all_current_cards.containsAll(selected_cards)) {
			System.out.println("ERROR: Cards selected from your current hand ('" + selected_cards + "') cannot "
					+ "be found in either your current goods hand ('" + current_goods_hand + "') or your current "
							+ "camels hand ('" + current_camel_hand + "').\n");
			return false;
		}
				
		//selected market cards must already be in market place
		if (!current_market_place.containsAll(selected_market_cards)) {
			System.out.println("ERROR: Cards selected from the market place ('" + selected_market_cards + "') "
					+ "cannot be found in the current market place ('" + current_market_place + "').\n");
			return false;
		}
				
		//selected market cards cannot be of the same type as any of the
		//selected goods cards
		for (int i = 0; i < selected_cards.size(); i++) {
			for (int j = 0; j < selected_market_cards.size(); j++) {
				
				if (selected_cards.get(i).equals(selected_market_cards.get(j))) {
					System.out.println("ERROR: The cards selected from the market place ('" 
							+ selected_market_cards + "') cannot be found within the current market ('"
							+ current_market_place + "').\n");
					return false;
				}
			}
		}
		
		return true;	
	}
		
	//method to establish if the 'sell_cards' action is possible
	public boolean can_sell_goods_cards(ArrayList<String> selected_goods_cards, 
			ArrayList<String> current_goods_hand) {
				
		//selected goods cards must be valid
		for (int i = 0; i < selected_goods_cards.size(); i++) {
			if (!is_valid_card(selected_goods_cards.get(i))) {
				System.out.println("ERROR: One card chosen ('" + selected_goods_cards.get(i) + "') from your "
						+ "current hand is not a valid card. Each card must be one of the following: D, G, "
						+ "Sil, Clo, Spi, L\n");
				return false;
			}
		}
				
		//must have at least one selected card to sell
		if (selected_goods_cards.size() == 0) {
			System.out.println("ERROR: At least one goods card must be chosen.\n");
			return false;
		}
				
		//selected cards cannot be camels
		for (int i = 0; i < selected_goods_cards.size(); i++) {
			if (selected_goods_cards.get(i).equals("Cam")) {
				System.out.println("ERROR: Cards selected from goods hand ('" + selected_goods_cards + "') "
						+ "cannot be camel cards.\n");
				return false;
			}
		}
		
		//add selected goods cards to a set
		Set<String> goods_cards_types = new HashSet<String>();
		goods_cards_types.addAll(selected_goods_cards);
		
		//if set is greater than 1, then more than one card type has been selected which
		//is not allowed
		if (goods_cards_types.size() > 1) {
			System.out.println("ERROR: Only one type of card can be selected for a sale to take place.\n");
			return false;
		}

		int card_found = 0;
		
		//loop over goods hand to find if all cards are there
		for (int i = 0; i < current_goods_hand.size(); i++) {
			if (current_goods_hand.get(i).equals(selected_goods_cards.get(0))) {
				card_found++;
			}
		}
		
		//selected goods cards must already be in goods hand
		if (card_found < selected_goods_cards.size()) {
			System.out.println("ERROR: Cards selected from your current goods hand ('" + selected_goods_cards 
					+ "') cannot be all found in your current goods hand ('" + current_goods_hand + "').\n");
			return false;
		}
		
		//count number of diamond, silver and gold
		int diamond_num = 0, gold_num = 0, silver_num = 0;
				
		for (int i = 0; i < selected_goods_cards.size(); i++) {
			
			if ((selected_goods_cards.get(i)).equals("D")) {
				diamond_num++;
			}
			
			if ((selected_goods_cards.get(i)).equals("G")) {
				gold_num++;
			}

			if ((selected_goods_cards.get(i)).equals("Sil")) {
				silver_num++;
			}
		}
				
		//diamond, gold and silver if selected, has to be at least 2 in quantity
		if (diamond_num == 1 || gold_num == 1 || silver_num == 1) {
			System.out.println("ERROR: At least 2 diamond, gold or silver cards is selected, then at least "
					+ "2 must be selected for a sale to take place.\n");
			return false;
		}
		
		return true;
	} 
	
	//method to evaluate if the names provided are valid
	public boolean acceptable_names(String player_one_name_input, String player_two_name_input) {
		
		//player names can only be the same when two computer players are chosen
		if (player_one_name_input.equals("Com") && player_one_name_input.equals("Com")) {			
			return true;
		}
		
		//human player names cannot be the same
		if (player_one_name_input.equals(player_two_name_input)) {
			System.out.println("Invalid Entry: The names provided must be different from one another. Try again.\n");
			
			return false;
		}
		
		//player one name cannot equal 'Computer', 'Computer_1' or 'Computer_2
		if (player_one_name_input.equals("Computer") || player_one_name_input.equals("Computer_1") || 
				player_one_name_input.equals("Computer_2")) {
			System.out.println("Invalid Entry: Player one can not have the names 'Computer', 'Computer_1' or 'Computer_2'."
					+ " Try again.\n");
			
			return false;
		}
	
		//player two name cannot equal 'Computer', 'Computer_1' or 'Computer_2
		if (player_two_name_input.equals("Computer") || player_two_name_input.equals("Computer_1") || 
				player_two_name_input.equals("Computer_2")) {
			System.out.println("Invalid Entry: Player two can not have the names 'Computer', 'Computer_1' or 'Computer_2'."
					+ " Try again.\n");
			
			return false;
		}
		
		else {
			return true;
		}
	}
	
	
	//method to output first introduction text to console
	public void print_introduction_1() {
		
		//welcome text for the user
		System.out.println("\nWELCOME TO JAIPUR!\n\nTo play the game, first choose two players. If you wish to create a "
				+ "computer player,\nplease type 'Com' below. Otherwise select any other name for any human players "
				+ "\ntaking part (Please note two human players cannot have the same name).\n\n");
	}
	
	
	//method to output first introduction text to console
	public void print_introduction_2() {
		
		//text describing the possible actions to the user
		System.out.println("\nBelow are all the possible actions a player can make in the game:\n\n"
				+ "INFO:\n'take_c' = take all camels\n'take_g' = take one goods card\n'trade' = "
				+ "trade two or more cards\n'sell' = sell one or more cards\n'list_sc' = list all "
				+ "sold cards\n'list_pm = list all previous moves\n'pt' = print table\n\n\nPlease "
				+ "also remember, for the best experience, make sure the terminal\nwindow is fullscreen."
				+ "\n\n\nPress 'ENTER' to start the game...");
		
		   System.out.println();
		   Scanner scanner = new Scanner(System.in);
		   scanner.nextLine();
	}

	//method to list and output all sold cards to the console
	public void print_sold_cards(ArrayList<String> sold_cards) {
		
		//if no cards have been sold, the arraylist is not outputted to the console
		if (sold_cards.size() == 0) {
			System.out.println("\n(No cards have yet been sold)\n");
		}
		
		//if one or more cards have been sold then the arraylist is sorted and printed
		else {
			System.out.println("\nSOLD CARDS: " + sold_cards + "\n");			
		}
	}
	
	//method to list and output all previous moves to the console
	public void print_previous_moves(ArrayList<ArrayList<String>> previous_moves) {
		
		//if no moves have been made, the arraylist is not outputted to the console
		if (previous_moves.size() == 0) {
			System.out.println("\n(No moves have yet been made)\n");
		}
		
		//if one or more moves have been made then the arraylist is printed
		else {
			
			System.out.println("\nPREVIOUS MOVES:");			
			
			for (int i = 0; i < previous_moves.size(); i++) {
				System.out.println(previous_moves.get(i));			
			}
			System.out.println();
		}
	}
	
	//method to output last move from adversary player
	public void print_last_move(ArrayList<ArrayList<String>> previous_moves) {
		
		int last_move_index = previous_moves.size() - 1;
		ArrayList<String> last_move = previous_moves.get(last_move_index);
		System.out.println("\n\nLAST MOVE: " + last_move);
	}


	public String find_player_name(int player_num) {
		
		String player_name = "";
		Scanner scanner = new Scanner(System.in);
				
		//if player number given is 1, it will ask the user to enter the name of player 1
		if (player_num == 1) {
									
			System.out.print("Enter the name of player 1: ");
			player_name = scanner.next();
		}
		
		//if player number given is 2, it will ask the user to enter the name of player 2
		if (player_num == 2) {
									
			System.out.print("Enter the name of player 2: ");
			player_name = scanner.next();
			System.out.print("\n");
		}
		
		//if player number given is neither 1 or 2, an exception is raised
		if (player_num != 1 && player_num != 2) {
			throw new IllegalArgumentException("ERROR: The input parameter 'player_num' must be 1 or 2.");
		}
		
		return player_name;
	}
	
	public int find_computer_player_strategy_num(int player_num) {
		
		String strategy_num_str = "";
		boolean strategy_num_accepted = false;
		int strategy_num;
		Scanner scanner = new Scanner(System.in);
		
		while (!strategy_num_accepted) {
				
			//if player number given is 1, it will ask the user to enter the strategy for player 1
			if (player_num == 1) {
											
				System.out.print("Enter the strategy number you wish player 1 to use: ");
				strategy_num_str = scanner.next();
			}
				
			//if player number given is 2, it will ask the user to enter the strategy for player 2
			if (player_num == 2) {
										
				System.out.print("Enter the strategy number you wish player 2 to use: ");
				strategy_num_str = scanner.next();
			}
			
			if (strategy_num_str.equals("1") || strategy_num_str.equals("2") || strategy_num_str.equals("3") ||
					strategy_num_str.equals("4") || strategy_num_str.equals("5") || strategy_num_str.equals("6")) {
				
				strategy_num_accepted = true;
			}
			
			else {
				System.out.println("\nInvalid Entry: Strategy number must be 1, 2, 3, 4, 5 or 6.\n");
			}
			
		}
		
		strategy_num = Integer.parseInt(strategy_num_str);
		
		//if player number given is neither 1 or 2, an exception is raised
		if (player_num != 1 && player_num != 2) {
			throw new IllegalArgumentException("ERROR: The input parameter 'player_num' must be 1 or 2.");
		}
		
		return strategy_num;

	}
	
	
	//method to alert players  that the round is over
	public void round_is_over(int round_num) {
		
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nROUND " + round_num + " IS OVER!\n");
	}
	
	//method to output who receives the camel token
	public void camel_token_receiver(String player_one_name, String player_two_name, int player_one_camel_total, 
			int player_two_camel_total) {
		
		//player with most camels receives the camel token (+5 points)
		
		if (player_one_camel_total > player_two_camel_total) {
			System.out.println("\n" + player_one_name + " recieves the camel token (+5 points).");
		}
		
		if (player_two_camel_total > player_one_camel_total) {
			System.out.println("\n" + player_two_name + " recieves the camel token (+5 points).");
		}

		//if both players have same amount of camels, neither receives the camel token (+5 points)
		if (player_two_camel_total == player_one_camel_total) {
			System.out.println("\nNeither player recieves the camel token.");
		}
	}
	
	//method to alert players of final scores
	public void final_scores(String player_one_name, String player_two_name, int player_one_score,
			int player_two_score) {
		
		System.out.println("\n\nFINAL SCORES:\n" + player_one_name + ": " + player_one_score + "\n" +
				player_two_name + ": " + player_two_score + "\n");
	}
	
	//method to output the winner of the round
	public void round_winner(String player_one_name, String player_two_name, int player_one_score_total, 
			int player_two_score_total, int player_one_bonus_token_total, int player_two_bonus_token_total, 
			int player_one_goods_cards_total, int player_two_goods_cards_total) {
		
		//firsts determines winner by the highest score
		if (player_one_score_total > player_two_score_total) {
			System.out.println("\n" + player_one_name + " has the most points, therefore wins this round.");
		}
		
		if (player_one_score_total < player_two_score_total) {
			System.out.println("\n" + player_two_name + " has the most points, therefore wins this round.");
		}
		
		//if players have same score, winner is determined by the total amount of bonus tokens they both have
		if (player_one_score_total == player_two_score_total) {
			System.out.println("\nBoth players have the same amount of points.");
			
			if (player_one_bonus_token_total > player_two_bonus_token_total) {
				System.out.println("\n" + player_one_name + " has more bonus tokens, therefore wins this round.");
			}
			
			if (player_one_bonus_token_total < player_two_bonus_token_total) {
				System.out.println("\n" + player_two_name + " has more bonus tokens, therefore wins this round.");
			}
			
			//if players have same amount of bonus tokens, winner is determined by the player with the most goods cards
			if (player_one_bonus_token_total == player_two_bonus_token_total) {
				System.out.println("\nBoth players have the same amount of bonus tokens.");
				
				if (player_one_goods_cards_total > player_two_goods_cards_total) {
					System.out.println("\n" + player_one_name + " has more goods cards, therefore wins this round.");
				}
				
				if (player_one_goods_cards_total < player_two_goods_cards_total) {
					System.out.println("\n" + player_two_name + " has more goods cards, therefore wins this round.");
				}
			}
		}
	}
	
	//method to output the winner of the game and final round scores
	public void game_winner(String player_one_name, String player_two_name, int player_one_wins, int player_two_wins, int player_one_r1_score, 
			int player_one_r2_score, int player_one_r3_score, int player_two_r1_score, int player_two_r2_score, int player_two_r3_score) {
		
		//output who has achieved two round wins
		
		if (player_one_wins == 2) {
			System.out.println("\n\n********** " + player_one_name + " has won the game! **********\n");
		}
		
		if (player_two_wins == 2) {
			System.out.println("\n\n********** " + player_two_name + " has won the game! **********\n");
		}
		
		System.out.println("Player 1 - " + player_one_name + "\nR1: " + player_one_r1_score + "\nR2: " + player_one_r2_score + 
				"\nR3: " + player_one_r3_score + "\n\nPlayer 2 - " + player_two_name + "\nR1: " + player_two_r1_score + "\nR2: " + player_two_r2_score + 
				"\nR3: " + player_two_r3_score);

	}
	
	//method to delay text output between player's turns and rounds
	public void delay_text(String delay_type, String player_one_name, String player_two_name) throws InterruptedException {
		
		boolean player_one_is_computer = false;
		boolean player_two_is_computer = false;

		//finds if player one is a computer player
		if (player_one_name.equals("Computer") || player_one_name.equals("Computer_1") || 
				player_one_name.equals("Computer_2")) {
			
			player_one_is_computer = true;
		}
		
		//finds if player two is a computer player
		if (player_two_name.equals("Computer") || player_two_name.equals("Computer_1") || 
				player_two_name.equals("Computer_2")) {
			
			player_two_is_computer = true;
		}
		
		//more delay between rounds
		if (delay_type.equals("round")) {
			
			//waits 10 seconds before next turn if either players are not synthetic
			if (!player_one_is_computer || !player_two_is_computer) {
			
				System.out.print("\nNext round in: ");
				
				TimeUnit.SECONDS.sleep(1);
				System.out.print(" 10");
	
				TimeUnit.SECONDS.sleep(1);
				System.out.print(" 9");
	
				TimeUnit.SECONDS.sleep(1);
				System.out.print(" 8");
				
				TimeUnit.SECONDS.sleep(1);
				System.out.print(" 7");
	
				TimeUnit.SECONDS.sleep(1);
				System.out.print(" 6");
	
				TimeUnit.SECONDS.sleep(1);
				System.out.print(" 5");
	
				TimeUnit.SECONDS.sleep(1);
				System.out.print(" 4");
				
				TimeUnit.SECONDS.sleep(1);
				System.out.print(" 3");
	
				TimeUnit.SECONDS.sleep(1);
				System.out.print(" 2");
	
				TimeUnit.SECONDS.sleep(1);
				System.out.print(" 1 ...");
	
				TimeUnit.SECONDS.sleep(1);
				
				System.out.println("\n\n");
			}
		}

		//waits 3 seconds before next turn if both players are not synthetic
		if (delay_type.equals("turn")) {

			if (!player_one_is_computer && !player_two_is_computer) {
				
				System.out.print("\nNext player's turn in: ");
				
				TimeUnit.SECONDS.sleep(1);
				System.out.print(" 3");
	
				TimeUnit.SECONDS.sleep(1);
				System.out.print(" 2");
	
				TimeUnit.SECONDS.sleep(1);
				System.out.print(" 1 ...");
	
				TimeUnit.SECONDS.sleep(1);
			}
		}
				
		
		//only allows delay types: 'turn' and 'round'
		if(!delay_type.equals("turn") && !delay_type.equals("round")) {
			throw new IllegalArgumentException("ERROR: Delay type in method is invalid");
		}
	}
		
	//method to evaluate what to do in next move
	public ArrayList<String> evaluate_next_move() {
		
		String next_move_type = choose_next_move(), card;
		
		Scanner scanner = new Scanner(System.in);
		
		ArrayList<String> card_list = new ArrayList<String>();
		
		//if next move is take all camels, list sold cards, list previous moves or
		//print table then only the action is returned
		if (next_move_type.equals("take_camels") || next_move_type.equals("list_sold_cards") ||
				next_move_type.equals("list_previous_moves") || next_move_type.equals("print_table")) {
			
			card_list.add(next_move_type);
			 
			return card_list;
		}
		
		//if next move is take one good, then the action and selected good is returned
		if (next_move_type.equals("take_one_good")) {
			
			System.out.print("Enter the goods card you wish to take: ");
			card = scanner.next();
			
			
			card_list.add(next_move_type);
			card_list.add(card);
			return card_list;
		}
		
		//if next move is trade cards, then the cards from the market and current hand
		//selected are both returned
		if (next_move_type.equals("trade_cards")) {
			
			card_list.add(next_move_type);
									
			while (true) {
				
			    System.out.print("Enter the cards you wish to exchange (one "
			    		+ "at a time, '--' to finish): ");
				card = scanner.next();
				 

				
			    if (card.equals("--")) {
				    card_list.add("--");
				    break;
			    }
			    
			    card_list.add(card);
			}
						
			while (true) {
				
			    System.out.print("Enter the market cards you wish to take: (one "
			    		+ "at a time, '--' to finish): ");
				card = scanner.next();
				 

			    if (card.equals("--")) {
				    break;
			    }

			    card_list.add(card);
			}
			
			return card_list;
		}
		
		//if the next action is F cards then the action and selected cards are returned
		if (next_move_type.equals("sell_cards")) {
			
			card_list.add(next_move_type);
									
			while (true) {
				
			    System.out.print("Enter the goods cards you wish to sell (one "
			    		+ "at a time, '--' to finish): ");
				card = scanner.next();
				 

			    card_list.add(card);
			    
			    if (card.equals("--")) {
				    break;
			    }
			}
			
			return card_list;
		}
		
		else {

			return null;
		}		
	}	
}