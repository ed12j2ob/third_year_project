package third_year_project;

import java.util.ArrayList;

public class JaipurGame {
	
	private JaipurRound round_1;
	private JaipurRound round_2;
	private JaipurRound round_3;	
	
	private int player_one_wins, player_two_wins, player_one_turn_num, player_two_turn_num, player_one_r1_final_score,
				player_one_r2_final_score, player_one_r3_final_score, player_two_r1_final_score,
				player_two_r2_final_score, player_two_r3_final_score, player_one_strategy_num, player_two_strategy_num;
	
	private double p1_selected_hand_k1, p1_selected_hand_k2, p1_selected_hand_k3, p1_selected_market_k1, p1_selected_market_k2, 
					p1_selected_market_k3, p2_selected_hand_k1, p2_selected_hand_k2, p2_selected_hand_k3, p2_selected_market_k1,
					p2_selected_market_k2, p2_selected_market_k3;
	
	private boolean print_console_output;

	
	private ArrayList<ArrayList<String>> previous_moves = new ArrayList<ArrayList<String>>();
			
	//constructor to initialise players and rounds (computer players use "Com" string)
	public JaipurGame(String player_one_name, String player_two_name, boolean print_console_output, int player_one_strategy_num, int player_two_strategy_num,
			double p1_selected_hand_k1, double p1_selected_hand_k2, double p1_selected_hand_k3, double p1_selected_market_k1, double p1_selected_market_k2, 
			double p1_selected_market_k3, double p2_selected_hand_k1, double p2_selected_hand_k2, double p2_selected_hand_k3, double p2_selected_market_k1, 
			double p2_selected_market_k2, double p2_selected_market_k3) {
		
		//setup strategies for player one and two
		this.player_one_strategy_num = player_one_strategy_num;
		this.player_two_strategy_num = player_two_strategy_num;
		
		//set up rounds for the game
		round_1 = new JaipurRound(player_one_name, player_two_name, 1, player_one_strategy_num, player_two_strategy_num);
		round_2 = new JaipurRound(player_one_name, player_two_name, 2, player_one_strategy_num, player_two_strategy_num);
		round_3 = new JaipurRound(player_one_name, player_two_name, 3, player_one_strategy_num, player_two_strategy_num);
		
		//boolean to determine if tables should be printed in each round
		this.print_console_output = print_console_output;
		
		//player turn numbers are defaulted to 1, as they are incremented at 
		//the end of each turn
		player_one_turn_num = 1;
		player_two_turn_num = 1;
		
		//player round victories defaulted to 0
		player_one_wins = 0;
		player_two_wins = 0;
		
		//player round final scores defaulted to 0
		player_one_r1_final_score = 0;
		player_one_r2_final_score = 0;
		player_one_r3_final_score = 0;
		player_two_r1_final_score = 0;
		player_two_r2_final_score = 0;
		player_two_r3_final_score = 0;
		
		//hand heuristic default values for player_one (if synthetic)
		this.p1_selected_hand_k1 = p1_selected_hand_k1;
		this.p1_selected_hand_k2 = p1_selected_hand_k2;
		this.p1_selected_hand_k3 = p1_selected_hand_k3;
		
		//market heuristic default values for player_one (if synthetic)
		this.p1_selected_market_k1 = p1_selected_market_k1;
		this.p1_selected_market_k2 = p1_selected_market_k2;
		this.p1_selected_market_k3 = p1_selected_market_k3;
		
		//hand heuristic default values for player_two (if synthetic)
		this.p2_selected_hand_k1 = p2_selected_hand_k1;
		this.p2_selected_hand_k2 = p2_selected_hand_k2;
		this.p2_selected_hand_k3 = p2_selected_hand_k3;
		
		//market heuristic default values for player_two (if synthetic)
		this.p2_selected_market_k1 = p2_selected_market_k1;
		this.p2_selected_market_k2 = p2_selected_market_k2;
		this.p2_selected_market_k3 = p2_selected_market_k3;
	}
	
	//methods to retrieve the number of rounds won per player
	
	public int get_player_one_wins() {
		
		return player_one_wins;
	}
	
	public int get_player_two_wins() {
		
		return player_two_wins;
	}
	
	//method to return all previous moves
	public ArrayList<ArrayList<String>> get_previous_moves() {
		
		return previous_moves;
	}

	//methods to get player names
	
	public String get_player_one_name() {
		
		//player one's name will be the same in all rounds, so round 1 is chosen
		String player_one_name = round_1.get_player_one().get_name();
		
		return player_one_name;
	}
	
	public String get_player_two_name() {
		
		//player two's name will be the same in all rounds, so round 1 is chosen
		String player_two_name = round_1.get_player_two().get_name();
		
		return player_two_name;
	}
	
	//methods to get round scores from both players
	
	public int get_player_one_r1_final_score() {
		
		return player_one_r1_final_score;
	}
	
	public int get_player_one_r2_final_score() {
		
		return player_one_r2_final_score;
	}
	
	public int get_player_one_r3_final_score() {
		
		return player_one_r3_final_score;
	}

	public int get_player_two_r1_final_score() {
		
		return player_two_r1_final_score;
	}
	
	public int get_player_two_r2_final_score() {
		
		return player_two_r2_final_score;
	}
	
	public int get_player_two_r3_final_score() {
		
		return player_two_r3_final_score;
	}
	
	//methods to append the parameters in the main state value heuristic for either player's 1 or 2
	
	public void set_p1_selected_hand_k1(double new_p1_hand_k1) {
		
		this.p1_selected_hand_k1 = new_p1_hand_k1;
	}
	
	public void set_p1_selected_hand_k2(double new_p1_hand_k2) {
		
		this.p1_selected_hand_k2 = new_p1_hand_k2;
	}

	public void set_p1_selected_hand_k3(double new_p1_hand_k3) {
		
		this.p1_selected_hand_k3 = new_p1_hand_k3;
	}
	
	public void set_p1_selected_market_k1(double new_p1_market_k1) {
		
		this.p1_selected_market_k1 = new_p1_market_k1;
	}

	public void set_p1_selected_market_k2(double new_p1_market_k2) {
		
		this.p1_selected_market_k2 = new_p1_market_k2;
	}

	public void set_p1_selected_market_k3(double new_p1_market_k3) {
		
		this.p1_selected_market_k3 = new_p1_market_k3;
	}
	
	public void set_p2_selected_hand_k1(double new_p2_hand_k1) {
		
		this.p2_selected_hand_k1 = new_p2_hand_k1;
	}
	
	public void set_p2_selected_hand_k2(double new_p2_hand_k2) {
		
		this.p2_selected_hand_k2 = new_p2_hand_k2;
	}

	public void set_p2_selected_hand_k3(double new_p2_hand_k3) {
		
		this.p2_selected_hand_k3 = new_p2_hand_k3;
	}
	
	public void set_p2_selected_market_k1(double new_p2_market_k1) {
		
		this.p2_selected_market_k1 = new_p2_market_k1;
	}

	public void set_p2_selected_market_k2(double new_p2_market_k2) {
		
		this.p2_selected_market_k2 = new_p2_market_k2;
	}

	public void set_p2_selected_market_k3(double new_p2_market_k3) {
		
		this.p2_selected_market_k3 = new_p2_market_k3;
	}
	
	//method to play a round of Jaipur
	public void play_round(int round_num) throws InterruptedException {
		
		TextInterface ti = new TextInterface();
		boolean player_one_turn = true;
		String player_one_name = "", player_two_name = "";
		ArrayList<String> current_market = new ArrayList<String>();

		//tests what round it is, and gives player starting cards and obtains their names
		if (round_num == 1) {
			round_1.give_players_starting_cards();
			player_one_name = round_1.get_player_one().get_name();
			player_two_name = round_1.get_player_two().get_name();
			
			//finds initial camel totals at the start of round 1, if computer player is chosen for player 1
			if (player_one_name.equals("Computer") || player_one_name.equals("Computer_1") ||
					player_one_name.equals("Computer_2")) {
				
				ComputerPlayer com_player = (ComputerPlayer) round_1.get_player_one();
				int opponent_total_goods_cards = round_1.get_player_two().get_current_goods_hand().size();
				boolean opponent_has_camel_cards = round_1.get_player_two().has_camels();

				int initial_camel_total = com_player.get_opposition_initial_camel_total(opponent_total_goods_cards,
						opponent_has_camel_cards);
				
				com_player.set_opposing_player_camel_total(initial_camel_total);
			}
			
			//finds initial camel totals at the start of round 1, if computer player is chosen for player 2
			if (player_two_name.equals("Computer") || player_two_name.equals("Computer_1") ||
					player_two_name.equals("Computer_2")) {
				
				ComputerPlayer com_player = (ComputerPlayer) round_1.get_player_two();
				int opponent_total_goods_cards = round_1.get_player_one().get_current_goods_hand().size();
				boolean opponent_has_camel_cards = round_1.get_player_one().has_camels();

				int initial_camel_total = com_player.get_opposition_initial_camel_total(opponent_total_goods_cards,
						opponent_has_camel_cards);
				
				com_player.set_opposing_player_camel_total(initial_camel_total);
			}
		}
		
		if (round_num == 2) {
			round_2.give_players_starting_cards();
			player_one_name = round_2.get_player_one().get_name();
			player_two_name = round_2.get_player_two().get_name();
			
			//finds initial camel totals at the start of round 2, if computer player is chosen for player 1
			if (player_one_name.equals("Computer") || player_one_name.equals("Computer_1") ||
					player_one_name.equals("Computer_2")) {
				
				ComputerPlayer com_player = (ComputerPlayer) round_2.get_player_one();
				int opponent_total_goods_cards = round_2.get_player_two().get_current_goods_hand().size();
				boolean opponent_has_camel_cards = round_2.get_player_two().has_camels();

				int initial_camel_total = com_player.get_opposition_initial_camel_total(opponent_total_goods_cards,
						opponent_has_camel_cards);
				
				com_player.set_opposing_player_camel_total(initial_camel_total);
			}
			
			//finds initial camel totals at the start of round 2, if computer player is chosen for player 2
			if (player_two_name.equals("Computer") || player_two_name.equals("Computer_1") ||
					player_two_name.equals("Computer_2")) {
				
				ComputerPlayer com_player = (ComputerPlayer) round_2.get_player_two();
				int opponent_total_goods_cards = round_2.get_player_one().get_current_goods_hand().size();
				boolean opponent_has_camel_cards = round_2.get_player_one().has_camels();

				int initial_camel_total = com_player.get_opposition_initial_camel_total(opponent_total_goods_cards,
						opponent_has_camel_cards);
				
				com_player.set_opposing_player_camel_total(initial_camel_total);
			}
		}

		if (round_num == 3) {
			round_3.give_players_starting_cards();
			player_one_name = round_3.get_player_one().get_name();
			player_two_name = round_3.get_player_two().get_name();
			
			//finds initial camel totals at the start of round 3, if computer player is chosen for player 1
			if (player_one_name.equals("Computer") || player_one_name.equals("Computer_1") ||
					player_one_name.equals("Computer_2")) {
				
				ComputerPlayer com_player = (ComputerPlayer) round_3.get_player_one();
				int opponent_total_goods_cards = round_3.get_player_two().get_current_goods_hand().size();
				boolean opponent_has_camel_cards = round_3.get_player_two().has_camels();

				int initial_camel_total = com_player.get_opposition_initial_camel_total(opponent_total_goods_cards,
						opponent_has_camel_cards);
				
				com_player.set_opposing_player_camel_total(initial_camel_total);
			}
			
			//finds initial camel totals at the start of round 3, if computer player is chosen for player 2
			if (player_two_name.equals("Computer") || player_two_name.equals("Computer_1") ||
					player_two_name.equals("Computer_2")) {
				
				ComputerPlayer com_player = (ComputerPlayer) round_3.get_player_two();
				int opponent_total_goods_cards = round_3.get_player_one().get_current_goods_hand().size();
				boolean opponent_has_camel_cards = round_3.get_player_one().has_camels();

				int initial_camel_total = com_player.get_opposition_initial_camel_total(opponent_total_goods_cards,
						opponent_has_camel_cards);
				
				com_player.set_opposing_player_camel_total(initial_camel_total);
			}
		}

		boolean is_round_over = false;
		
		//round will keep looping until certain conditions are met (see is_round_over() method)
		while (!is_round_over) {
			
			//gets the market cards of the current round
			if (round_num == 1) {
				current_market = round_1.get_market_cards();
			}
			
			if (round_num == 2) {
				current_market = round_2.get_market_cards();
			}

			if (round_num == 3) {
				current_market = round_3.get_market_cards();
			}
						
			ArrayList<String> current_goods_hand = null, current_camels_hand = null, next_move = null;
			String action = "", current_player_name = "";
			
			//only triggers if its player one's turn
			if (player_one_turn) {
				
				current_player_name = player_one_name;
				
				//finds the current goods and camels hand for player 1, depending on the round
				if (round_num == 1) {
					current_goods_hand = round_1.get_player_one().get_current_goods_hand();
					current_camels_hand = round_1.get_player_one().get_current_camel_hand();
				}
				
				if (round_num == 2) {
					current_goods_hand = round_2.get_player_one().get_current_goods_hand();
					current_camels_hand = round_2.get_player_one().get_current_camel_hand();
				}

				if (round_num == 3) {
					current_goods_hand = round_3.get_player_one().get_current_goods_hand();
					current_camels_hand = round_3.get_player_one().get_current_camel_hand();
				}
			}
			
			else {
				
				current_player_name = player_two_name;

				//finds the current goods and camels hand for player 2, depending on the round
				if (round_num == 1) {
					current_goods_hand = round_1.get_player_two().get_current_goods_hand();
					current_camels_hand = round_1.get_player_two().get_current_camel_hand();
				}
				
				if (round_num == 2) {
					current_goods_hand = round_2.get_player_two().get_current_goods_hand();
					current_camels_hand = round_2.get_player_two().get_current_camel_hand();
				}

				if (round_num == 3) {
					current_goods_hand = round_3.get_player_two().get_current_goods_hand();
					current_camels_hand = round_3.get_player_two().get_current_camel_hand();
				}
			}
			
			//only prints card table if prompted by the constructor
			if (print_console_output) {
				
				//prints the card table depending on the round and specific player
				if (round_num == 1) {
					round_1.print_card_table(current_player_name, round_num);
				}
				
				if (round_num == 2) {
					round_2.print_card_table(current_player_name, round_num);
				}
	
				if (round_num == 3) {
					round_3.print_card_table(current_player_name, round_num);
				}
			}

			//loop to establish each turn between player's
			while (next_move == null) {					

				if (current_player_name.equals("Computer") || current_player_name.equals("Computer_1") ||
						current_player_name.equals("Computer_2")) {
					
					//establish all default variables
					ComputerPlayer com_player = null;
					ArrayList<Integer> current_diamond_tokens = new ArrayList<Integer>();
					ArrayList<Integer> current_gold_tokens = new ArrayList<Integer>();
					ArrayList<Integer> current_silver_tokens = new ArrayList<Integer>();
					ArrayList<Integer> current_cloth_tokens = new ArrayList<Integer>();
					ArrayList<Integer> current_spice_tokens = new ArrayList<Integer>();
					ArrayList<Integer> current_leather_tokens = new ArrayList<Integer>();
					ArrayList<String> sold_cards = new ArrayList<String>();
					ArrayList<ArrayList<String>> previous_moves = this.get_previous_moves();
					int total_cards_remaining = 55, current_player_score = 0, opposing_player_score = 0, turn_num = 0, strategy_num = 4;
					
					if (round_num == 1) {
						
						//find scores of current and opposing player in round 1 and set opposing goods total
						if (player_one_turn) {
							com_player = (ComputerPlayer) round_1.get_player_one();
							com_player.set_opposing_player_goods_total(round_1.get_player_two().get_current_goods_hand().size());
							current_player_score = round_1.get_player_one().get_current_score();
							opposing_player_score = round_1.get_player_two().get_current_score();
						}
						
						else {
							com_player = (ComputerPlayer) round_1.get_player_two();
							com_player.set_opposing_player_goods_total(round_1.get_player_one().get_current_goods_hand().size());
							current_player_score = round_1.get_player_two().get_current_score();
							opposing_player_score = round_1.get_player_one().get_current_score();
						}

						//find all token quantities, sold cards and total cards remaining in round 1
						current_diamond_tokens.addAll(round_1.get_goods_tokens().get_diamond_tokens());
						current_gold_tokens.addAll(round_1.get_goods_tokens().get_gold_tokens());
						current_silver_tokens.addAll(round_1.get_goods_tokens().get_silver_tokens());
						current_cloth_tokens.addAll(round_1.get_goods_tokens().get_cloth_tokens());
						current_spice_tokens.addAll(round_1.get_goods_tokens().get_spice_tokens());
						current_leather_tokens.addAll(round_1.get_goods_tokens().get_leather_tokens());
						sold_cards.addAll(round_1.get_sold_cards());
						total_cards_remaining = round_1.get_cards().get_remaining_card_num();
					}
			
					if (round_num == 2) {
						
						//find scores of current and opposing player in round 2 and set opposing goods total
						if (player_one_turn) {
							com_player = (ComputerPlayer) round_2.get_player_one();
							com_player.set_opposing_player_goods_total(round_2.get_player_two().get_current_goods_hand().size());
							current_player_score = round_2.get_player_one().get_current_score();
							opposing_player_score = round_2.get_player_two().get_current_score();
						}
						
						else {
							com_player = (ComputerPlayer) round_2.get_player_two();
							com_player.set_opposing_player_goods_total(round_2.get_player_one().get_current_goods_hand().size());
							current_player_score = round_2.get_player_two().get_current_score();
							opposing_player_score = round_2.get_player_one().get_current_score();
						}
						
						//find all token quantities, sold cards and total cards remaining in round 2
						current_diamond_tokens.addAll(round_2.get_goods_tokens().get_diamond_tokens());
						current_gold_tokens.addAll(round_2.get_goods_tokens().get_gold_tokens());
						current_silver_tokens.addAll(round_2.get_goods_tokens().get_silver_tokens());
						current_cloth_tokens.addAll(round_2.get_goods_tokens().get_cloth_tokens());
						current_spice_tokens.addAll(round_2.get_goods_tokens().get_spice_tokens());
						current_leather_tokens.addAll(round_2.get_goods_tokens().get_leather_tokens());
						sold_cards.addAll(round_2.get_sold_cards());
						total_cards_remaining = round_2.get_cards().get_remaining_card_num();
					}

					if (round_num == 3) {
						
						//find scores of current and opposing player in round 3 and set opposing goods total
						if (player_one_turn) {
							com_player = (ComputerPlayer) round_3.get_player_one();
							com_player.set_opposing_player_goods_total(round_3.get_player_two().get_current_goods_hand().size());
							current_player_score = round_3.get_player_one().get_current_score();
							opposing_player_score = round_3.get_player_two().get_current_score();
						}
						
						else {
							com_player = (ComputerPlayer) round_3.get_player_two();
							com_player.set_opposing_player_goods_total(round_3.get_player_one().get_current_goods_hand().size());
							current_player_score = round_3.get_player_two().get_current_score();
							opposing_player_score = round_3.get_player_one().get_current_score();
						}
						
						//find all token quantities, sold cards and total cards remaining in round 3
						current_diamond_tokens.addAll(round_3.get_goods_tokens().get_diamond_tokens());
						current_gold_tokens.addAll(round_3.get_goods_tokens().get_gold_tokens());
						current_silver_tokens.addAll(round_3.get_goods_tokens().get_silver_tokens());
						current_cloth_tokens.addAll(round_3.get_goods_tokens().get_cloth_tokens());
						current_spice_tokens.addAll(round_3.get_goods_tokens().get_spice_tokens());
						current_leather_tokens.addAll(round_3.get_goods_tokens().get_leather_tokens());
						sold_cards.addAll(round_3.get_sold_cards());
						total_cards_remaining = round_3.get_cards().get_remaining_card_num();
					}
					
					//set the pre-chosen heuristic parameters for player one if they are synthetic
					if (player_one_turn) {
						
						com_player.set_selected_hand_k1(p1_selected_hand_k1);
						com_player.set_selected_hand_k2(p1_selected_hand_k2);
						com_player.set_selected_hand_k3(p1_selected_hand_k3);
						com_player.set_selected_market_k1(p1_selected_market_k1);
						com_player.set_selected_market_k2(p1_selected_market_k2);
						com_player.set_selected_market_k3(p1_selected_market_k3);
						turn_num = player_one_turn_num;
						strategy_num = player_one_strategy_num;
					}
					
					//set the pre-chosen heuristic parameters for player two if they are synthetic
					else {
						
						com_player.set_selected_hand_k1(p2_selected_hand_k1);
						com_player.set_selected_hand_k2(p2_selected_hand_k2);
						com_player.set_selected_hand_k3(p2_selected_hand_k3);
						com_player.set_selected_market_k1(p2_selected_market_k1);
						com_player.set_selected_market_k2(p2_selected_market_k2);
						com_player.set_selected_market_k3(p2_selected_market_k3);
						turn_num = player_two_turn_num;
						strategy_num = player_two_strategy_num;
					}

							
					//find all known cards of opposite player
					ArrayList<Double> opposition_hand_quantities = com_player.get_known_opposition_card_quantities(previous_moves, player_one_turn);
					
					//find all opposite player's goods and camels totals
					int opposing_player_goods_total = com_player.get_opposing_player_goods_total();
					
					//find the amount of cards currently unknown of the opposite player
					int unknown_card_total = com_player.get_unknown_goods_cards_total(opposition_hand_quantities, opposing_player_goods_total);
					
					//find all possible actions the player can take
					ArrayList<ArrayList<String>> all_possible_actions = com_player.get_all_possible_moves(current_market, current_goods_hand, current_camels_hand);
					
					//add all goods and camels cards to the same list
					ArrayList<String> all_goods_and_camels = new ArrayList<String>();
					all_goods_and_camels.addAll(current_goods_hand);
					all_goods_and_camels.addAll(current_camels_hand);

					//find the current quantities of the player's hand and the market place
					ArrayList<Double> current_hand_quantities = com_player.get_current_card_quantities(all_goods_and_camels);
					ArrayList<Double> current_market_quantities = com_player.get_current_card_quantities(current_market);

					//find all known cards currently outside of the deck
					ArrayList<String> all_known_cards = com_player.get_all_known_cards(current_goods_hand, current_camels_hand, current_market, sold_cards);
					
					//retrieve a list of all cards the player knows are still in the deck
					ArrayList<Double> remaining_card_quantities = com_player.get_remaining_card_quantities(all_known_cards);

					//find all remaining card probabilities
					ArrayList<Double> remaining_card_probabilities = com_player.get_remaining_card_probabilities(remaining_card_quantities);
					
					//picks next move based on what strategy has been pre-selected by the user
										
					if (strategy_num == 1) {
						
						next_move = com_player.strategy_1_next_move(all_possible_actions);
					}
					
					if (strategy_num == 2) {
						
						next_move = com_player.strategy_2_next_move(all_possible_actions, current_diamond_tokens, current_gold_tokens, current_silver_tokens, 
								current_cloth_tokens, current_spice_tokens, current_leather_tokens);
					}

					if (strategy_num == 3) {
						
						next_move = com_player.strategy_3_next_move(current_hand_quantities, current_market_quantities, all_possible_actions, 
								current_diamond_tokens, current_gold_tokens, current_silver_tokens, current_cloth_tokens, current_spice_tokens, 
								current_leather_tokens, turn_num);
					}

					if (strategy_num == 4) {
						
						next_move = com_player.strategy_4_next_move(all_possible_actions, current_hand_quantities, opposition_hand_quantities, unknown_card_total, 
								current_market_quantities, current_player_score, opposing_player_score, current_diamond_tokens, current_gold_tokens, current_silver_tokens, 
								current_cloth_tokens, current_spice_tokens, current_leather_tokens, sold_cards, total_cards_remaining, remaining_card_probabilities);
					}

					if (strategy_num == 5) {
						
						next_move = com_player.strategy_5_next_move(all_possible_actions, current_hand_quantities, opposition_hand_quantities, unknown_card_total, 
								current_market_quantities, current_player_score, opposing_player_score, current_diamond_tokens, current_gold_tokens, current_silver_tokens, 
								current_cloth_tokens, current_spice_tokens, current_leather_tokens, sold_cards, total_cards_remaining, remaining_card_probabilities);
					}

					if (strategy_num == 6) {
						
						next_move = com_player.strategy_6_next_move(all_possible_actions, current_hand_quantities, opposition_hand_quantities, unknown_card_total, 
								current_market_quantities, current_player_score, opposing_player_score, current_diamond_tokens, current_gold_tokens, current_silver_tokens, 
								current_cloth_tokens, current_spice_tokens, current_leather_tokens, sold_cards, total_cards_remaining, remaining_card_probabilities);
					}
						
				}	
		
				//if player is human, they select their own move
				else {
					//evaluates move and determines if it is valid from human player
					next_move = ti.evaluate_next_move();
					
				}
				
				//move will only be 'null' if invalid
				if (next_move != null) {
					
					//obtains move type
					action = next_move.get(0);
						
					//action to take all the camels
					if (action.equals("take_camels")) {
												
						//determines the action requested is valid
						if (ti.can_take_all_camels(current_market)) {
							
							//find total number of camels
							int camel_total = ti.take_all_camels_total(current_market);
							
							//loop over camel total, and add it to next_move, so it can
							//be displayed more clearly in the previous moves
							for (int i = 0; i < camel_total; i++) {
								next_move.add("Cam");
							}
							
							//adds next move to previous moves list
							previous_moves.add(next_move);
							
							//implements move depending on round and player
							if (round_num == 1) {
								round_1.take_all_camel_cards(current_player_name);
							}

							if (round_num == 2) {
								round_2.take_all_camel_cards(current_player_name);
							}

							if (round_num == 3) {
								round_3.take_all_camel_cards(current_player_name);
							}							
						}
						
						else {
							
							//if invalid move was chosen, it loops again and allows player to choose again
							next_move = null;
						}
					}
								
					//action to take one good
					if (action.equals("take_one_good")) {
												
						//determines the action requested is valid
						if (ti.can_take_one_goods_card(next_move.get(1), current_market, current_goods_hand)) {
							
							//adds next move to previous moves list
							previous_moves.add(next_move);
								
							//implements move depending on round and player
							if (round_num == 1) {
								round_1.take_one_goods_card(current_player_name, next_move.get(1));
							}

							if (round_num == 2) {
								round_2.take_one_goods_card(current_player_name, next_move.get(1));
							}

							if (round_num == 3) {
								round_3.take_one_goods_card(current_player_name, next_move.get(1));
							}
						}
						
						else {
							//if invalid move was chosen, it loops again and allows player to choose again
							next_move = null;
						}
					}

					
					//action to trade goods
					if (action.equals("trade_cards")) {
						
						boolean separator_reached = false;
							
						ArrayList<String> selected_goods_cards = new ArrayList<String>();
						ArrayList<String> selected_market_cards = new ArrayList<String>();
												
						//splits cards chosen into two containers: selected_goods_cards and selected_market_cards
						for (int i = 1; i < next_move.size(); i++) {
																
							if (next_move.get(i).equals("--")) {
								separator_reached = true;
								continue;
							}
								
							if (!separator_reached) {
								selected_goods_cards.add(next_move.get(i));
							}
							
							if (separator_reached) {
								selected_market_cards.add(next_move.get(i));
							}
						}
						
						//determines the action requested is valid
						if (ti.can_trade_cards(selected_goods_cards, selected_market_cards, current_market, 
								current_goods_hand, current_camels_hand)) {
							
							//adds next move to previous moves list
							previous_moves.add(next_move);
							
							//implements move depending on round and player
							if (round_num == 1) {
								round_1.trade_cards(current_player_name, selected_goods_cards, selected_market_cards);
							}

							if (round_num == 2) {
								round_2.trade_cards(current_player_name, selected_goods_cards, selected_market_cards);
							}
							
							if (round_num == 3) {
								round_3.trade_cards(current_player_name, selected_goods_cards, selected_market_cards);
							}
						}
						
						else {
							//if invalid move was chosen, it loops again and allows player to choose again
							next_move = null;
						}
						
					}
					
					//action to sell goods
					if (action.equals("sell_cards")) {
							
						boolean separator_reached = false;
							
						ArrayList<String> selected_goods_cards = new ArrayList<String>();
							
						//puts cards chosen into one container: selected_goods_cards
						for (int i = 1; i < next_move.size(); i++) {
																
							if (next_move.get(i).equals("--")) {
								separator_reached = true;
								continue;
							}
								
							if (!separator_reached) {
								selected_goods_cards.add(next_move.get(i));
							}
								
							if (separator_reached) {
								break;
							}
						}
							
						//determines the action requested is valid
						if (ti.can_sell_goods_cards(selected_goods_cards, current_goods_hand)) {
							
							//adds next move to previous moves list
							previous_moves.add(next_move);
			
							//implements move depending on round and player
							if (round_num == 1) {
								round_1.sell_goods_cards(current_player_name, selected_goods_cards);								
							}

							if (round_num == 2) {
								round_2.sell_goods_cards(current_player_name, selected_goods_cards);
							}

							if (round_num == 3) {
								round_3.sell_goods_cards(current_player_name, selected_goods_cards);
							}
						}
							
						else {
							//if invalid move was chosen, it loops again and allows player to choose again
							next_move = null;
						}
					}
					
					//action to list every card that has been sold
					if (action.equals("list_sold_cards")) {
								
						ArrayList<String> sold_cards = new ArrayList<String>();
						
						//implements move depending on round
						if (round_num == 1) {
							 sold_cards = round_1.get_sold_cards();
						}

						if (round_num == 2) {
							sold_cards = round_2.get_sold_cards();
						}

						if (round_num == 3) {
							sold_cards = round_3.get_sold_cards();
						}
						
						ti.print_sold_cards(sold_cards);
						
						//it loops again and allows player to choose another move
						next_move = null;	
					}
					
					//action to list every card that has been sold
					if (action.equals("list_previous_moves")) {
						
						ArrayList<ArrayList<String>> previous_moves = new ArrayList<ArrayList<String>>();
																				
						previous_moves = this.get_previous_moves();
						
						ti.print_previous_moves(previous_moves);
						
						//it loops again and allows player to choose another move
						next_move = null;	
					}
					
					//action to reprint the table upon user request
					if (action.equals("print_table")) {
																											
						//implements move depending on round
						if (round_num == 1) {
							round_1.print_card_table(current_player_name, round_num);
						}

						if (round_num == 2) {
							round_2.print_card_table(current_player_name, round_num);
						}

						if (round_num == 3) {
							round_3.print_card_table(current_player_name, round_num);
						}
						
						//it loops again and allows player to choose another move
						next_move = null;	
					}
				}
			}
									
			//switches players turns, once a valid move has previously been made	
			if (player_one_turn) {
				
				//add the total player one turn number by 1
				player_one_turn_num += 1;
				player_one_turn = false;
			}
			
			else {
				
				//add the total player two turn number by 1
				player_two_turn_num += 1;
				player_one_turn = true;
			}
			
			//boolean methods to determine if round is over
			if (round_num == 1) {
				is_round_over = round_1.is_round_over();
			}
			
			if (round_num == 2) {
				is_round_over = round_2.is_round_over();
			}

			if (round_num == 3) {
				is_round_over = round_3.is_round_over();
			}

			//delay text output between each turn
			ti.delay_text("turn", player_one_name, player_two_name);	
			
			if (print_console_output) {
				
				//outputs the last move of the adversary player just made
				ti.print_last_move(previous_moves);
			}
		}
		
		int player_one_final_score = 0, player_two_final_score = 0;
		int player_one_bonus_token_total = 0, player_two_bonus_token_total = 0;
		int player_one_goods_total = 0, player_two_goods_total = 0;
		int player_one_camel_total = 0, player_two_camel_total = 0;
		
		//calculate the winner of round one 
		if (round_num == 1) {
			
			if (print_console_output) {
				ti.round_is_over(1);
			}
			
			//give camel token to player with most points before points are obtained
			round_1.give_camel_token();
			
			player_one_final_score = round_1.get_player_one().get_current_score();
			player_two_final_score = round_1.get_player_two().get_current_score();
			
			player_one_bonus_token_total = round_1.get_player_one().get_bonus_token_total();
			player_two_bonus_token_total = round_1.get_player_two().get_bonus_token_total();
			
			player_one_goods_total = round_1.get_player_one().get_current_goods_hand().size();
			player_two_goods_total = round_1.get_player_two().get_current_goods_hand().size();
			
			player_one_camel_total = round_1.get_player_one().get_current_camel_hand().size();
			player_two_camel_total = round_1.get_player_two().get_current_camel_hand().size();
			
			if (print_console_output) {
				
				ti.camel_token_receiver(player_one_name, player_two_name, player_one_camel_total, 
						player_two_camel_total);
			
				//print final scores
				ti.final_scores(round_1.get_player_one().get_name(), round_1.get_player_two().get_name(), 
						round_1.get_player_one().get_current_score(), round_1.get_player_two().get_current_score());
			}
			
			//save final scores
			player_one_r1_final_score = player_one_final_score;
			player_two_r1_final_score = player_two_final_score;

			//calculate winner and add to overall wins total
			if (round_1.calculate_winner() == 1) {
				player_one_wins += 1;
			}		
			
			if (round_1.calculate_winner() == 2) {
				player_two_wins += 1;
			}		

			if (print_console_output) {

				ti.round_winner(player_one_name, player_two_name, player_one_final_score, player_two_final_score, 
							player_one_bonus_token_total, player_two_bonus_token_total, player_one_goods_total, 
							player_two_goods_total);
			}
		}

		//calculate the winner of round two 
		if (round_num == 2) {
			
			if (print_console_output) {
				ti.round_is_over(2);
			}
			
			//give camel token to player with most points before points are obtained
			round_2.give_camel_token();
			
			player_one_final_score = round_2.get_player_one().get_current_score();
			player_two_final_score = round_2.get_player_two().get_current_score();
			
			player_one_bonus_token_total = round_2.get_player_one().get_bonus_token_total();
			player_two_bonus_token_total = round_2.get_player_two().get_bonus_token_total();
			
			player_one_goods_total = round_2.get_player_one().get_current_goods_hand().size();
			player_two_goods_total = round_2.get_player_two().get_current_goods_hand().size();
			
			player_one_camel_total = round_2.get_player_one().get_current_camel_hand().size();
			player_two_camel_total = round_2.get_player_two().get_current_camel_hand().size();
			
			if (print_console_output) {
				
				ti.camel_token_receiver(player_one_name, player_two_name, player_one_camel_total, 
						player_two_camel_total);
			
				//print final scores
				ti.final_scores(round_2.get_player_one().get_name(), round_2.get_player_two().get_name(), 
						round_2.get_player_one().get_current_score(), round_2.get_player_two().get_current_score());
			}

			//save final scores
			player_one_r2_final_score = player_one_final_score;
			player_two_r2_final_score = player_two_final_score;
			
			//calculate winner and add to overall wins total
			if (round_2.calculate_winner() == 1) {
				player_one_wins += 1;
			}		
			
			if (round_2.calculate_winner() == 2) {
				player_two_wins += 1;
			}		
			
			if (print_console_output) {

				ti.round_winner(player_one_name, player_two_name, player_one_final_score, player_two_final_score, 
						player_one_bonus_token_total, player_two_bonus_token_total, player_one_goods_total, 
						player_two_goods_total);
			}
		}
		
		//calculate the winner of round three
		if (round_num == 3) {
			
			if (print_console_output) {
				ti.round_is_over(3);
			}
			
			//give camel token to player with most points before points are obtained
			round_3.give_camel_token();
			
			player_one_final_score = round_3.get_player_one().get_current_score();
			player_two_final_score = round_3.get_player_two().get_current_score();
			
			player_one_bonus_token_total = round_3.get_player_one().get_bonus_token_total();
			player_two_bonus_token_total = round_3.get_player_two().get_bonus_token_total();
			
			player_one_goods_total = round_3.get_player_one().get_current_goods_hand().size();
			player_two_goods_total = round_3.get_player_two().get_current_goods_hand().size();
			
			player_one_camel_total = round_3.get_player_one().get_current_camel_hand().size();
			player_two_camel_total = round_3.get_player_two().get_current_camel_hand().size();
			
			if (print_console_output) {

				ti.camel_token_receiver(player_one_name, player_two_name, player_one_camel_total, 
						player_two_camel_total);
							
				//print final scores
				ti.final_scores(round_3.get_player_one().get_name(), round_3.get_player_two().get_name(), 
						round_3.get_player_one().get_current_score(), round_3.get_player_two().get_current_score());
			}
			
			//save final scores
			player_one_r3_final_score = player_one_final_score;
			player_two_r3_final_score = player_two_final_score;

			//calculate winner and add to overall wins total
			if (round_3.calculate_winner() == 1) {
				player_one_wins += 1;
			}		
			
			if (round_3.calculate_winner() == 2) {
				player_two_wins += 1;
			}		
			
			if (print_console_output) {

				ti.round_winner(player_one_name, player_two_name, player_one_final_score, player_two_final_score, 
						player_one_bonus_token_total, player_two_bonus_token_total, player_one_goods_total, 
						player_two_goods_total);
			}
		}
				
		//clear previous_moves arraylist for next round
		previous_moves.clear();
		
		//resets turn numbers back to 0
		player_one_turn_num = 1;
		player_two_turn_num = 1;
			
		//will not have the delay if a player has won the game
		if (!(player_one_wins == 2 || player_two_wins == 2)) {
			
			//delay text output between each round
			ti.delay_text("round", player_one_name, player_two_name);
		}
	}		
}