package third_year_project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;

public class JaipurRound {
	
	private MainDeck cards;
	
	private ArrayList<String> sold_cards;
	private ArrayList<String> market_cards;

	private GoodsTokens goods_tokens;
	private BonusTokens bonus_tokens;
	
	private Player player_one;
	private Player player_two;
		
	//constructor (initialises two players)
	public JaipurRound(String name_one, String name_two, int round_num, int player_one_strategy_num, int player_two_strategy_num) {
		
		cards = new MainDeck();
		sold_cards = new ArrayList<String>();
		market_cards = new ArrayList<String>();
		goods_tokens = new GoodsTokens();
		bonus_tokens = new BonusTokens();
		
		//raises exception if round number isn't 1, 2, or 3
		if (!((round_num == 1) || (round_num == 2) || (round_num == 3))) {
			throw new IllegalArgumentException("ERROR: Round number can only be 1, 2, or 3.");
		}
				
		//initialises two computer places if two "Com" stings are given as arguments
		if ((name_one.equals("Com")) && (name_two.equals("Com"))) {
			player_one = new ComputerPlayer("Computer_1", player_one_strategy_num);
			player_two = new ComputerPlayer("Computer_2", player_two_strategy_num);
		}
		
		else {

			//raises exception if two human players names are the same (doesn't apply to "Com" strings)
			if ((name_one.equals(name_two)) && (!(name_one.equals("Com")) && !(name_two.equals("Com")))) {
				throw new IllegalArgumentException("ERROR: Human player names cannot be the same.");
			}
					
			//selects one computer player and one human player if only one argument is "Com"
			if (name_one.equals("Com")) {
				
				player_one = new ComputerPlayer("Computer", player_one_strategy_num);
				player_two = new HumanPlayer(name_two);
			}
			
			else {
			
				if (name_two.equals("Com")) {
					
					player_one = new HumanPlayer(name_one);
					player_two = new ComputerPlayer("Computer", player_two_strategy_num);
				}
				
				//if "Com" argument is not found, constructor will assume both names are human players
				else {
				
					player_one = new HumanPlayer(name_one);
					player_two = new HumanPlayer(name_two);
				}
			}
		}
		
		//setting up the market place
		for (int i = 0; i < 5; i++) {
			if (i < 3) {
				market_cards.add("Cam"); //add three camels
			}
			
			else {
				market_cards.add(cards.take_card()); //add two random goods cards
			}	
		}
	}
	
	//method to give players first five cards
	public void give_players_starting_cards() {
		
		for (int i = 0; i < 5; i++) {
			player_one.add_card(cards.take_card());
			player_two.add_card(cards.take_card());
		}
	}
	
	
	//methods to retrieve information of a specific round

	public ArrayList<String> get_sold_cards() {
		
		return sold_cards;
	}
	
	public ArrayList<String> get_market_cards() {
		
		return market_cards;
	}
	
	public Player get_player_one() {
		
		return player_one;
	}
	
	public Player get_player_two() {
		
		return player_two;
	}
	
	public GoodsTokens get_goods_tokens() {
		
		return goods_tokens;
	}
	
	public MainDeck get_cards() {
		
		return cards;
	}

	
	//method to avoid bugs when exchanging cards by checking if a card is valid
	public boolean is_valid_card(String card_type) {
		
		//cards only valid if they are of the following types: D, G, Sil, Clo, Spi, L, Cam
		if (card_type.equals("D") || card_type.equals("G") ||card_type.equals("Sil") || card_type.equals("Clo") 
				|| card_type.equals("Spi") || card_type.equals("L") || card_type.equals("Cam")) {
			
			return true;
		}
		
		else {
			return false;
		}
	}

	//method to avoid bugs when exchanging cards by checking if a card is a camel card
	public boolean is_camel(String card_type) {
		
		//cards only valid if they are of type: Cam
		if (card_type.equals("Cam")) {
			
			return true;
		}
		
		else {
			return false;
		}
	}

	//method to determine if round is over
	public boolean is_round_over() {
		
		if (cards.is_deck_empty() || goods_tokens.is_three_goods_type_sold()) {
			return true;
		}
		
		else {
			return false;
		}
	}
	
	//method to determine which player gets the camel token (most camels in herd)
	public void give_camel_token() {
		
		int player_one_camel_total = player_one.current_camel_hand.size();
		int player_two_camel_total = player_two.current_camel_hand.size();
		
		//if player one has more camels they receive the camel token
		if (player_one_camel_total > player_two_camel_total) {
			player_one.add_points(5); //camel token goes to player one
		}
		
		//if player two has more camels they receive the camel token
		if (player_one_camel_total < player_two_camel_total) {
			player_two.add_points(5); //camel token goes to player two
		}
	}
	
	//method to retrieve the bonus token total of a specific player (needed in case of a score tie)
	public int get_bonus_token_total(String player_name) {
		
		int bonus_token_total = 0; 

		//gets player one bonus token total
		if (player_one.get_name().equals(player_name)) {
			bonus_token_total = player_one.get_bonus_token_total();
		}
		
		//gets player two bonus token total
		if (player_two.get_name().equals(player_name)) {
			bonus_token_total = player_two.get_bonus_token_total();
		}
		
		//if player name does not exist, the error is caught
		if (!(player_one.get_name().equals(player_name)) && (player_two.get_name() != player_name)) {
			throw new IllegalArgumentException("ERROR: Player name given is invalid.");
		}			

		return bonus_token_total;
	}
	
	//method to establish which player wins the round (returns 1 if player one wins, returns 2 
	//if player two wins, returns 0 if ultimate draw)
	public int calculate_winner() {
		
		int player_one_final_score = player_one.get_current_score();
		int player_two_final_score = player_two.get_current_score();
		
		int player_one_bonus_token_total = player_one.get_bonus_token_total();
		int player_two_bonus_token_total = player_two.get_bonus_token_total();
		
		int player_one_goods_total = player_one.get_current_goods_hand().size();
		int player_two_goods_total = player_two.get_current_goods_hand().size();

		//boolean methods to determine which player has the highest score
		if (player_one_final_score > player_two_final_score) {
			return 1;
		}
		
		if (player_one_final_score < player_two_final_score) {
			return 2;
		}
		
		if (player_one_final_score == player_two_final_score) {
						
			//boolean methods to determine which player has the most bonus tokens
			if (player_one_bonus_token_total > player_two_bonus_token_total) {
				return 1;
			}
			
			if (player_one_bonus_token_total < player_two_bonus_token_total) {
				return 2;
			}
			
			if (player_one_bonus_token_total == player_two_bonus_token_total) {
								
				//boolean methods to determine which player has the most goods cards
				if (player_one_goods_total > player_two_goods_total) {
					return 1;
				}
				
				if (player_one_goods_total < player_two_goods_total) {
					return 2;
				}
			}
		}
		
		//returns 0 if both players have the same score, same amount of bonus tokens
		//and same amount of goods cards (extremely unlikely scenario)
		return 0;
	}

	
	//method to take one goods card from the market place
	public void take_one_goods_card(String player_name, String card_selected) {
		
		//throws exception if card selected is invalid
		if (!is_valid_card(card_selected)) {
			throw new IllegalArgumentException("ERROR: Card selected does not exist. "
					+ "(Must be one of the following: D, G, Sil, Clo, Spi, L)");
		}
		
		//throws exception if card selected is a camel card
		if (is_camel(card_selected)) {
			throw new IllegalArgumentException("ERROR: Card selected is not a goods card.");
		}
				
		boolean found_card = false;
		int found_card_index = -1;
		
		//loops over current market cards to find card to remove
		for (int i = 0; i < market_cards.size(); i++) {
			if (market_cards.get(i).equals(card_selected)) {
				found_card = true;
				found_card_index = i;
				break;
			}
		}
		
		//if card found in market place, it is placed in the correct players hand 
		//and a card is then added to the market from the main deck	
		if (found_card) {			
			if (player_one.get_name().equals(player_name)) {
				player_one.add_card(market_cards.get(found_card_index));
			}
				
			if (player_two.get_name().equals(player_name)) {
				player_two.add_card(market_cards.get(found_card_index));
			}
			
			//if player name does not exist, the error is caught
			if ((player_one.get_name() != player_name) && 
					(player_two.get_name() != player_name)) {
				throw new IllegalArgumentException("ERROR: Player name given is invalid.");
			}
			
			market_cards.remove(found_card_index);
			market_cards.add(cards.take_card());
		}
			
		else {
			throw new IllegalArgumentException("ERROR: Current selected good card is not in "
					+ "market place.");
		}
	}
	
	//method to take all_camel_cards from the market place
	public void take_all_camel_cards(String player_name) {
		
		ArrayList<Integer> camel_cards_indexes = new ArrayList<Integer>();
		int camels_in_market = 0;
		
		//loops over current market cards to find all camel cards to remove
		for (int i = 0; i < market_cards.size(); i++) {
			if (market_cards.get(i).equals("Cam")) {
				camel_cards_indexes.add(i);
				camels_in_market++;
			}
		}
		
		//indexes sorted in descending order so they can be removed accordingly
		Collections.sort(camel_cards_indexes);
		Collections.reverse(camel_cards_indexes);

		//only takes camels if one or more are currently in the market place
		if (camels_in_market > 0) {
			
			//remove camels from market place
			for (int i = 0; i < camel_cards_indexes.size(); i++) {
				int index = camel_cards_indexes.get(i);
				market_cards.remove(index);
			}
			
			//replace camels with cards from the main deck
			for (int i = 0; i < camels_in_market; i++) {
				market_cards.add(cards.take_card());
			}
			
			//add camels taken from market to players current camel cards
			if (player_one.get_name().equals(player_name)) {
				for (int i = 0; i < camels_in_market; i++) {
					player_one.add_card("Cam");
				}
			}
				
			if (player_two.get_name().equals(player_name)) {
				for (int i = 0; i < camels_in_market; i++) {
					player_two.add_card("Cam");
				}
			}
			
			//if player name does not exist, the error is caught
			if ((player_one.get_name() != player_name) && 
					(player_two.get_name() != player_name)) {
				throw new IllegalArgumentException("ERROR: Player name given is invalid.");
			}
		}
		
		else {
			throw new IllegalArgumentException("ERROR: No camels are currently within the "
					+ "market place.");
		}
	}
	
	
	//method to trade cards from current hand with cards from the market place
	public void trade_cards(String player_name, ArrayList<String> cards_selected, 
			ArrayList<String> market_cards_selected) {
		
		//exception to capture trades of unequal sizes
		if (cards_selected.size() != market_cards_selected.size()) {
			throw new IllegalArgumentException("ERROR: Cards selected from the current hand and "
					+ "market place must be the same amount.");
		}
		
		//exception to ensure at least 2 cards from both the current hand and market 
		//place are selected for trading
		if (cards_selected.size() < 2 || market_cards_selected.size() < 2) {
			throw new IllegalArgumentException("ERROR: Two or more cards must be selected "
					+ "from both the current hand and the market for trading.");
		}
		
		boolean cards_of_same_type = false;
		
		for (int i = 0; i < cards_selected.size(); i++) {
			
			//loop over selected goods cards to check if they are all valid types
			if (!is_valid_card(cards_selected.get(i))) {
				throw new IllegalArgumentException("ERROR: Cards selected from current hand "
						+ "do not exist. (Must be one of the following: D, G, Sil, Clo, "
						+ "Spi, L)");
			}
			
			//loop over selected market cards to check if they are all valid types
			for (int j = 0; j < market_cards_selected.size(); j++) {
				
				if (!is_valid_card(market_cards_selected.get(j))) {
					throw new IllegalArgumentException("ERROR: Cards selected from market place "
							+ "do not exist. (Must be one of the following: D, G, Sil, "
							+ "Clo, Spi, L)");
				}
				
				//exception to check if any market cards selected are camels as this is 
				//not allowed
				if (is_camel(market_cards_selected.get(j))) {
					throw new IllegalArgumentException("ERROR: Cannot select camel cards from "
							+ "the market to trade.");
				}

				if (cards_selected.get(i).equals(market_cards_selected.get(j)) && !cards_selected.get(i).equals("Cam")) {
					cards_of_same_type = true;
				}
			}
		}
		
		//exception to check if cards selected from current hand are not the same as 
		//cards from market place as this is not allowed
		if (cards_of_same_type) {
			throw new IllegalArgumentException("ERROR: Cards selected from the current hand cannot"
					+ " be of the same type as the cards selected from the market place.");
		}
		
		else {
			
			//sets to establish indexes of current selected cards (sets are chosen 
			//as duplication does not occur)
			Set<Integer> goods_cards_indexes = new HashSet<Integer>();
			Set<Integer> camel_cards_indexes = new HashSet<Integer>();
			Set<Integer> market_cards_indexes = new HashSet<Integer>();
			
			ArrayList<String> current_goods_hand = null;
			ArrayList<String> current_camels_hand = null;

			//obtaining player's current hand of goods cards
			if (player_one.get_name().equals(player_name)) {		
				current_goods_hand =  player_one.get_current_goods_hand();
				current_camels_hand =  player_one.get_current_camel_hand();
			}
			
			if (player_two.get_name().equals(player_name)) {		
				current_goods_hand =  player_two.get_current_goods_hand();
				current_camels_hand =  player_two.get_current_camel_hand();
			}
			
			//if player name does not exist, the error is caught
			if ((player_one.get_name() != player_name) && 
					(player_two.get_name() != player_name)) {
				throw new IllegalArgumentException("ERROR: Player name given is invalid.");
			}
			
			int camel_index_num = 0;
			
			//selected cards need copies to extract information from, as the copies
			//will be shortened in the search process below
			ArrayList<String> cards_selected_temp = new ArrayList<String>();
			cards_selected_temp.addAll(cards_selected);
			
			ArrayList<String> market_cards_selected_temp = new ArrayList<String>();
			market_cards_selected_temp.addAll(market_cards_selected);

			//looping over goods cards to find where each index is
			for (int i = 0; i <= cards_selected_temp.size(); i++) {
				
				i = 0;
				
				//if camel card is found it adds the next index sequentially as 
				//all camel cards in the camel herd will be the same
				if (cards_selected_temp.get(i).equals("Cam")) {
					
					camel_cards_indexes.add(camel_index_num);
					camel_index_num++;						
					cards_selected_temp.remove(i);
				}
				
				//if a goods card is found it loops over all other cards until it is found, but
				//ensures it does not add the same card as found before
				else {
									
					for (int j = 0; j < current_goods_hand.size(); j++) {
						
						if (cards_selected_temp.size() > 0) {
						
							if (cards_selected_temp.get(i).equals(current_goods_hand.get(j)) &&
									!(goods_cards_indexes.contains(j))) {
								goods_cards_indexes.add(j);
								cards_selected_temp.remove(i);
							}
						}
					}
				}
			}
				
			//looping over market cards to find where each index is
			for (int i = 0; i <= market_cards_selected_temp.size(); i++) {
				
				i = 0;
								
				//if a market card is found it loops over all other cards until it is found, but
				//ensures it does not add the same card as found before
				for (int j = 0; j < market_cards.size(); j++) {
										
					if (market_cards_selected_temp.size() > 0) {
					
						if (market_cards_selected_temp.get(i).equals(market_cards.get(j)) &&
								!(market_cards_indexes.contains(j))) {
							market_cards_indexes.add(j);
							market_cards_selected_temp.remove(i);
						}
					}
				}
			}
			
			//exception to check if all goods cards that are selected exist in 
			//current goods hand
			if (cards_selected.size() != (goods_cards_indexes.size() + camel_cards_indexes.size())) {
				throw new IllegalArgumentException("ERROR: One or more cards selected are "
						+ "not present within current goods hand.");
			}
			
			//exception to check if all market cards that are selected exist in 
			//market place
			if (market_cards_selected.size() != market_cards_indexes.size()) {
				throw new IllegalArgumentException("ERROR: One or more cards selected are not "
						+ "present within the current market.");
			}
			
			//removing selected goods cards from current hand
			for (int i = 6; i >= 0; i--) {
				if (goods_cards_indexes.contains(i)) {
					current_goods_hand.remove(i);
				}
			}
			
			//removing selected camel cards from current hand
			for (int i = 6; i >= 0; i--) {
				if (camel_cards_indexes.contains(i)) {
					current_camels_hand.remove(i);
				}
			}

			//removing market cards from market place
			for (int i = 6; i >= 0; i--) {
				if (market_cards_indexes.contains(i)) {
					market_cards.remove(i);
				}
			}
			
			//add selected goods cards to market place
			for (int i = 0; i < cards_selected.size(); i++) {
				market_cards.add(cards_selected.get(i));
			}
			
			//add selected market cards to current goods hand
			for (int i = 0; i < market_cards_selected.size(); i++) {
				current_goods_hand.add(market_cards_selected.get(i));
			}
		}
	}
	
	//method to sell cards from current hand
	public void sell_goods_cards(String player_name, ArrayList<String> goods_cards_selected) {
		
		//exception is thrown if no cards have been selected
		if (goods_cards_selected.size() < 1) {
			throw new IllegalArgumentException("ERROR: There must be at least one card selected "
					+ "to sell.");
		}
				
		int diamond_num = 0, gold_num = 0, silver_num = 0;
		Set<String> card_types_within_selected = new HashSet<String>();
		
		//loop over selected cards for error checking
		for (int i = 0; i < goods_cards_selected.size(); i++) {
			
			//exception to check if cards selected are valid types
			if (!is_valid_card(goods_cards_selected.get(i))) {
				throw new IllegalArgumentException("ERROR: Card selected does not exist. "
						+ "(Must be one of the following: D, G, Sil, Clo, Spi, L)");
			}
			
			//exception to check if any cards selected are camels
			if (is_camel(goods_cards_selected.get(i))) {
				throw new IllegalArgumentException("ERROR: Camel cards cannot be sold, only "
						+ "traded with other goods cards.");
			}
			
			//counting number of diamond, gold and silver cards 
			if (goods_cards_selected.get(i).equals("D")) {
				diamond_num++;
			}
			
			if (goods_cards_selected.get(i).equals("G")) {
				gold_num++;
			}

			if (goods_cards_selected.get(i).equals("Sil")) {
				silver_num++;
			}
		}
		
		//adding all cards selected to set, if size of set is greater than 1 an exception 
		//is raised
		card_types_within_selected.addAll(goods_cards_selected);
		
		if (card_types_within_selected.size() > 1) {
			throw new IllegalArgumentException("ERROR: No more than one goods type can be "
					+ "selected.");			
		}
		
		//exception to check if number of diamond, gold and silver cards selected are 
		//greater than 1, if selected
		if (diamond_num == 1 || gold_num == 1 || silver_num == 1) {
			throw new IllegalArgumentException("ERROR: If diamond, gold or silver cards are "
					+ "selected, have to be greater than 1 in quantity.");
		}
		
		//set to establish indexes of current selected goods cards (sets are chosen 
		//as duplication does not occur)
		Set<Integer> goods_cards_indexes = new HashSet<Integer>();
		
		ArrayList<String> current_goods_hand = null;
		
		//obtaining player's current hand of goods cards
		if (player_one.get_name().equals(player_name)) {		
			current_goods_hand =  player_one.get_current_goods_hand();
		}
		
		if (player_two.get_name().equals(player_name)) {		
			current_goods_hand =  player_two.get_current_goods_hand();
		}
		
		//if player name does not exist, the error is caught
		if ((player_one.get_name() != player_name) && 
				(player_two.get_name() != player_name)) {
			throw new IllegalArgumentException("ERROR: Player name given is invalid.");
		}

		//looping over goods cards to find where each index is
		for (int i = 0; i < goods_cards_selected.size(); i++) {
								
			for (int j = 0; j < current_goods_hand.size(); j++) {
				
				if (goods_cards_selected.get(i).equals(current_goods_hand.get(j)) &&
						!(goods_cards_indexes.contains(j))) {
					goods_cards_indexes.add(j);
				}
			}
		}
		
		//exception to check if all goods cards that are selected exist in 
		//current goods hand
		if (goods_cards_selected.size() > goods_cards_indexes.size()) {
			throw new IllegalArgumentException("ERROR: One or more cards selected are "
					+ "not present within current goods hand.");
		}

		//removing selected goods cards from current hand
		for (int i = 6; i >= 0; i--) {
			if (goods_cards_indexes.contains(i)) {
				current_goods_hand.remove(i);
			}
		}

		//loop over goods cards to add points and remove goods tokens
		for (int i = 0; i < goods_cards_selected.size(); i++) {
			
			//test to see if its player one's turn
			if (player_one.get_name().equals(player_name)) {
				
				if (goods_cards_selected.get(i).equals("D")) {
					player_one.add_points(goods_tokens.take_one_diamond());
				}
				
				if (goods_cards_selected.get(i).equals("G")) {
					player_one.add_points(goods_tokens.take_one_gold());
				}
				
				if (goods_cards_selected.get(i).equals("Sil")) {
					player_one.add_points(goods_tokens.take_one_silver());
				}
				
				if (goods_cards_selected.get(i).equals("Clo")) {
					player_one.add_points(goods_tokens.take_one_cloth());
				}
				
				if (goods_cards_selected.get(i).equals("Spi")) {
					player_one.add_points(goods_tokens.take_one_spice());
				}
				
				if (goods_cards_selected.get(i).equals("L")) {
					player_one.add_points(goods_tokens.take_one_leather());
				}
				
				//bonus tokens are added at the end
				if (i == goods_cards_selected.size() - 1) {
					
					int cards_sold = goods_cards_selected.size();
					
					//if three cards are sold, the player also takes the three multiplier token
					if (cards_sold == 3) {
						player_one.add_points(bonus_tokens.take_one_three_multiplier());
						player_one.add_to_bonus_token_total();
					}
					
					//if four cards are sold, the player also takes the four multiplier token
					if (cards_sold == 4) {
						player_one.add_points(bonus_tokens.take_one_four_multiplier());
						player_one.add_to_bonus_token_total();
					}
					
					//if five or more cards are sold, the player also takes the five multiplier token
					if (cards_sold >= 5) {
						player_one.add_points(bonus_tokens.take_one_five_multiplier());
						player_one.add_to_bonus_token_total();
					}
				}
			}
				
			if (player_two.get_name().equals(player_name)) {
				
				if (goods_cards_selected.get(i).equals("D")) {
					player_two.add_points(goods_tokens.take_one_diamond());
				}
				
				if (goods_cards_selected.get(i).equals("G")) {
					player_two.add_points(goods_tokens.take_one_gold());
				}
				
				if (goods_cards_selected.get(i).equals("Sil")) {
					player_two.add_points(goods_tokens.take_one_silver());
				}
				
				if (goods_cards_selected.get(i).equals("Clo")) {
					player_two.add_points(goods_tokens.take_one_cloth());
				}
				
				if (goods_cards_selected.get(i).equals("Spi")) {
					player_two.add_points(goods_tokens.take_one_spice());
				}
				
				if (goods_cards_selected.get(i).equals("L")) {
					player_two.add_points(goods_tokens.take_one_leather());
				}
				
				if (i == goods_cards_selected.size() - 1) {
					
					int cards_sold = goods_cards_selected.size();
					
					if (cards_sold == 3) {
						player_two.add_points(bonus_tokens.take_one_three_multiplier());
						player_two.add_to_bonus_token_total();
					}
					
					if (cards_sold == 4) {
						player_two.add_points(bonus_tokens.take_one_four_multiplier());
						player_two.add_to_bonus_token_total();
					}
					
					if (cards_sold >= 5) {
						player_two.add_points(bonus_tokens.take_one_five_multiplier());
						player_two.add_to_bonus_token_total();
					}
				}
			}
			
			//add each card to the sold_cards arraylist
			sold_cards.add(goods_cards_selected.get(i));
			
			//the sold_cards arraylist is then sorted for easy readability
			Collections.sort(sold_cards);
		}
	}
	
	//method to print table display for a specific player
	public void print_card_table(String player_name, int round_num) {
		
		//this will output the main title and round number
				
		System.out.println("\n\n\n\n\n\n*********************** JAIPUR - ROUND " + round_num +
				" ***********************");
		
		//this will output the number of cards the other player has and if they have camel cards or not
		
		String has_camels;
		
		if (player_one.get_name().equals(player_name)) {
			
			if (player_two.has_camels()) {
				has_camels = "Yes";
			}
			
			else {
				has_camels = "No";
			}
			
			System.out.print("\nPLAYER 2 - GOODS CARDS TOTAL: " + player_two.get_current_goods_hand().size() + 
					"       HAS CAMEL CARDS?: " + has_camels);
		}
		
		if (player_two.get_name().equals(player_name)) {
			
			if (player_one.has_camels()) {
				has_camels = "Yes";
			}
			
			else {
				has_camels = "No";
			}
			
			System.out.print("\nPLAYER 1 - GOODS CARDS TOTAL: " + player_one.get_current_goods_hand().size() + 
					"       HAS CAMEL CARDS?: " + has_camels);
		}

		//this will output the number of cards remaining, goods tokens, quantity of 
		//bonus tokens and the market place, all of which is the same for both players
		
		System.out.println("\n\nCards left in deck: " + cards.get_remaining_card_num());
		
		System.out.println("\n\nGOODS TOKENS: ");
		goods_tokens.print_goods_tokens();

		System.out.println("\nBONUS TOKENS: ");
		bonus_tokens.print_bonus_token_quantities();
		
		System.out.println("\n\nMARKET PLACE: " + market_cards);
		
		//this will output the player's name, current score, current goods hand and current camel
		//herd, all of which is individual for both players

		if (player_one.get_name().equals(player_name)) {
			
			System.out.println("\n\nPLAYER: " + player_one.get_name() 
					+ "     CURRENT SCORE: " + player_one.get_current_score());
			
			System.out.println("\nCURRENT GOODS HAND: " + player_one.get_current_goods_hand());
			System.out.println("CURRENT CAMEL HERD: " + player_one.get_current_camel_hand() + "\n");
		}
		
		if (player_two.get_name().equals(player_name)) {
			
			System.out.println("\n\nCURRENT PLAYER: " + player_two.get_name() 
					+ "     CURRENT SCORE: " + player_two.get_current_score());
			
			System.out.println("\nCURRENT GOODS HAND: " + player_two.get_current_goods_hand());
			System.out.println("CURRENT CAMEL HERD: " + player_two.get_current_camel_hand() + "\n");	
		}
		
		//if player name does not exist, the error is caught
		if (!(player_one.get_name().equals(player_name)) && (player_two.get_name() != player_name)) {
			throw new IllegalArgumentException("ERROR: Player name given is invalid.");
		}			
	}
}
