package third_year_project;

import java.util.ArrayList;
import java.util.Collections;

public class GoodsTokens {
	
	private ArrayList<Integer> diamond_tokens;
	private ArrayList<Integer> gold_tokens;
	private ArrayList<Integer> silver_tokens;
	private ArrayList<Integer> cloth_tokens;
	private ArrayList<Integer> spice_tokens;
	private ArrayList<Integer> leather_tokens;

	//constructor
	public GoodsTokens() {
		
		diamond_tokens = new ArrayList<Integer>();
		gold_tokens = new ArrayList<Integer>();
		silver_tokens = new ArrayList<Integer>();
		cloth_tokens = new ArrayList<Integer>();
		spice_tokens = new ArrayList<Integer>();
		leather_tokens = new ArrayList<Integer>();
		
		//adding all relevant points to the tokens, in the correct quantities
		cloth_tokens.add(5);
		spice_tokens.add(5);
		leather_tokens.add(4);
		leather_tokens.add(3);
		leather_tokens.add(2);
		
		for (int i = 0; i < 2; i++) {
			
			diamond_tokens.add(7);
			gold_tokens.add(6);
			cloth_tokens.add(3);
			cloth_tokens.add(2);
			cloth_tokens.add(1);
			spice_tokens.add(3);
			spice_tokens.add(2);
			spice_tokens.add(1);
		}
		
		for (int i = 0; i < 3; i++) {
			diamond_tokens.add(5);
			gold_tokens.add(5);
		}

		for (int i = 0; i < 5; i++) {
			silver_tokens.add(5);
		}
		
		for (int i = 0; i < 6; i++) {
			leather_tokens.add(1);
		}
		
		//all tokens need to be sorted in descending order for gameplay
		Collections.sort(cloth_tokens);
		Collections.sort(spice_tokens);
		Collections.reverse(cloth_tokens);
		Collections.reverse(spice_tokens);
	}
	
	//method to output all remaining goods tokens 
	public void print_goods_tokens() {
		
		System.out.print("Dia: ");
		for (int i = 0; i < diamond_tokens.size(); i++) {
			System.out.print(diamond_tokens.get(i) + " ");
		}
		
		System.out.print("\nGol: ");
		for (int i = 0; i < gold_tokens.size(); i++) {
			System.out.print(gold_tokens.get(i) + " ");
		}
		
		System.out.print("\nSil: ");
		for (int i = 0; i < silver_tokens.size(); i++) {
			System.out.print(silver_tokens.get(i) + " ");
		}
		
		System.out.print("\nClo: ");
		for (int i = 0; i < cloth_tokens.size(); i++) {
			System.out.print(cloth_tokens.get(i) + " ");
		}
		
		System.out.print("\nSpi: ");
		for (int i = 0; i < spice_tokens.size(); i++) {
			System.out.print(spice_tokens.get(i) + " ");
		}
		
		System.out.print("\nLea: ");
		for (int i = 0; i < leather_tokens.size(); i++) {
			System.out.print(leather_tokens.get(i) + " ");
		}
		System.out.print("\n");
	}
	
	//methods to retrieve all tokens of a specific type
	
	public ArrayList<Integer> get_diamond_tokens() {
		
		return diamond_tokens;
	}
	
	public ArrayList<Integer> get_gold_tokens() {
		
		return gold_tokens;
	}
	
	public ArrayList<Integer> get_silver_tokens() {
		
		return silver_tokens;
	}
	
	public ArrayList<Integer> get_cloth_tokens() {
		
		return cloth_tokens;
	}
	
	public ArrayList<Integer> get_spice_tokens() {
		
		return spice_tokens;
	}
	
	public ArrayList<Integer> get_leather_tokens() {
		
		return leather_tokens;
	}
	
	//methods to determine if there any tokens left of a specific type
	
	public boolean no_diamonds_left() {
		
		if (diamond_tokens.size() == 0) {
			return true;
		}
		
		return false;
	}
	
	public boolean no_gold_left() {
		
		if (gold_tokens.size() == 0) {
			return true;
		}
		
		return false;
	}
	
	public boolean no_silver_left() {
		
		if (silver_tokens.size() == 0) {
			return true;
		}
		
		return false;
	}
	
	public boolean no_cloth_left() {
		
		if (cloth_tokens.size() == 0) {
			return true;
		}
		
		return false;
	}
	
	public boolean no_spice_left() {
		
		if (spice_tokens.size() == 0) {
			return true;
		}
		
		return false;
	}
	
	public boolean no_leather_left() {
		
		if (leather_tokens.size() == 0) {
			return true;
		}
		
		return false;
	}

	//methods to take one token of a specific type

	public int take_one_diamond() {
		
	    if (no_diamonds_left()) {
	    	return 0;
	    }
	    
	    else {
			int chosen_token = diamond_tokens.get(0);
			diamond_tokens.remove(0);
			return chosen_token;
	    }
	}
	
	public int take_one_gold() {
		
	    if (no_gold_left()) {
	    	return 0;
	    }
	    
	    else {
			int chosen_token = gold_tokens.get(0);
			gold_tokens.remove(0);
			return chosen_token;
	    }
	}
	
	public int take_one_silver() {
		
	    if (no_silver_left()) {
	    	return 0;
	    }
	    
	    else {
			int chosen_token = silver_tokens.get(0);
			silver_tokens.remove(0);
			return chosen_token;
	    }
	}
	
	public int take_one_cloth() {
		
	    if (no_cloth_left()) {
	    	return 0;
	    }
	    
	    else {
			int chosen_token = cloth_tokens.get(0);
			cloth_tokens.remove(0);
			return chosen_token;
	    }
	}
	
	public int take_one_spice() {
		
	    if (no_spice_left()) {
	    	return 0;
	    }
	    
	    else {
			int chosen_token = spice_tokens.get(0);
			spice_tokens.remove(0);
			return chosen_token;
	    }
	}
	
	public int take_one_leather() {
		
	    if (no_leather_left()) {
	    	return 0;
	    }
	    
	    else {
			int chosen_token = leather_tokens.get(0);
			leather_tokens.remove(0);
			return chosen_token;
	    }
	}

	//method to check if three goods type has been sold (needed to see if round is over)
	public boolean is_three_goods_type_sold() {
		
		int empty_tokens = 0;
		
		if (no_diamonds_left()) {
			empty_tokens++;
		}
		
		if (no_gold_left()) {
			empty_tokens++;
		}

		if (no_silver_left()) {
			empty_tokens++;
		}
		
		if (no_cloth_left()) {
			empty_tokens++;
		}
		
		if (no_spice_left()) {
			empty_tokens++;
		}
		
		if (no_leather_left()) {
			empty_tokens++;
		}
		
		if (empty_tokens > 2) {
			return true;
		}
		
		else {
			return false;
		}
	}
	
	
	public ArrayList<ArrayList<Integer>> get_tokens() {
		
		ArrayList<ArrayList<Integer>> tokens = new ArrayList<ArrayList<Integer>>();
		
		tokens.add(diamond_tokens);
		tokens.add(gold_tokens);
		tokens.add(silver_tokens);
		tokens.add(cloth_tokens);
		tokens.add(spice_tokens);
		tokens.add(leather_tokens);

		return tokens;
	}
}