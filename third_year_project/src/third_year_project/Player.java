package third_year_project;

import java.util.ArrayList;

public class Player {
	
	protected String name;
	protected ArrayList<String> current_goods_hand;
	protected ArrayList<String> current_camel_hand;
	protected int current_score;
	protected int bonus_token_total;
	
	//constructor
	public Player(String name) {
		
		//initialise player
		this.name = name;
		
		current_goods_hand = new ArrayList<String>();
		current_camel_hand = new ArrayList<String>();
		
		current_score = 0;
		bonus_token_total = 0;
	}
	
	//methods to retrieve information of a specific player
	
	public String get_name() {
		
		return name;
	}
	
	public ArrayList<String> get_current_goods_hand() {
		
		return current_goods_hand;
	}

	public ArrayList<String> get_current_camel_hand() {
		
		return current_camel_hand;
	}
	
	public int get_current_score() {
		
		return current_score;
	}
	
	public int get_bonus_token_total() {
		
		return bonus_token_total;
	}
	
	//method to determine if player has camel cards
	public boolean has_camels() {
		if (current_camel_hand.size() > 0) {
			return true;
		}
		
		else {
			return false;
		}
	}
	
	//method to determine if the player has reached seven goods cards
	public boolean seven_cards_reached() {
						
		if (current_goods_hand.size() == 7) {
			
			return true;
		}
		
		else {
			return false;
		}
	}
	
	//method to avoid bugs when exchanging cards
	public boolean is_valid_card(String card_type) {
		
		//cards only valid if they are of the following types: D, G, Sil, Clo, Spi, L, Cam
		if (card_type.equals("D") || card_type.equals("G") ||card_type.equals("Sil") || card_type.equals("Clo") 
				|| card_type.equals("Spi") || card_type.equals("L") || card_type.equals("Cam")) {
			
			return true;
		}
		
		else {
			return false;
		}
	}
	
	//method to add points to player's current score
	public void add_points(int points) {
		
		if (points < 0) {
			throw new IllegalArgumentException("ERROR: Points given need to be equal to 1 or "
					+ "greater.");
		}
		
		current_score += points;
	}	
	
	//method to add to the bonus token total
	public void add_to_bonus_token_total() {
		
		bonus_token_total += 1;
	}
	
	//method to add a card to the player's current hand
	public void add_card(String card_type) {
		
		if (!is_valid_card(card_type)) {
			throw new IllegalArgumentException("ERROR: Card type does not exist.");
		}
		
		//camels and goods cards are handled separately as they are held in separate piles
		if (card_type.equals("Cam")) {
			current_camel_hand.add(card_type);
		}
		
		else {
			if (seven_cards_reached()) {
				//players can only have up to seven goods cards at any given time
				throw new IllegalArgumentException("ERROR: Card limit reached! No more goods "
						+ "cards can be taken.");
			}
			
			else {
				current_goods_hand.add(card_type);
			}
		}
		
	}
	
	//method to take a card from the player's current hand
	public void remove_card(String card_type) {
		
		if (!is_valid_card(card_type)) {
			throw new IllegalArgumentException("ERROR: Card type does not exist.");
		}
		
		if (card_type.equals("Cam")) {
			if (current_camel_hand.size() == 0) {
				throw new IllegalArgumentException("ERROR: There are no more camels left to "
						+ "remove.");
			}
			
			else {
				current_camel_hand.remove(0);
			}
		}
			
		else {
			if (current_goods_hand.size() == 0) {
				throw new IllegalArgumentException("ERROR: There are no more goods left to "
						+ "remove.");
			}
			
			boolean found_card = false;
			int found_card_index = -1;
			
			//loops over current goods hand to find card to remove
			for (int i = 0; i < current_goods_hand.size(); i++) {

				if (current_goods_hand.get(i).equals(card_type)) {
					found_card = true;
					found_card_index = i;
					break;
				}
			}
			
			if (found_card) {
				current_goods_hand.remove(found_card_index);
			}
			
			else {
				throw new IllegalArgumentException("ERROR: Current selected good card is not "
						+ "in hand.");
			}
		}
	}
}