package third_year_project;

import java.util.ArrayList;
import java.util.Collections;

public class JaipurMain {

	public static void main(String[] args) throws InterruptedException {
		
		play_jaipur();
	}		

	//method to play the game of jaipur between human and computer players
	public static void play_jaipur() throws InterruptedException {
		String player_one_name, player_two_name, player_one_name_input = "", player_two_name_input = "";
		int player_one_wins, player_two_wins, player_one_strategy_num = 1, player_two_strategy_num = 1;
		boolean names_valid = false;
		
		//create instance of TextInterface class
		TextInterface ti = new TextInterface();
		
		//output first introduction text to the console
		ti.print_introduction_1();
		
		while (!names_valid) { 
		
			//finding the names of player's one and two, inputed by player
			player_one_name_input = ti.find_player_name(1);
			player_two_name_input = ti.find_player_name(2);
			
			names_valid = ti.acceptable_names(player_one_name_input, player_two_name_input);
			
			if (player_one_name_input.equals("Com")) {
				player_one_strategy_num = ti.find_computer_player_strategy_num(1);
			}
			
			if (player_two_name_input.equals("Com")) {
				player_two_strategy_num = ti.find_computer_player_strategy_num(2);
			}
		}
		
		//output second introduction text to the console
		ti.print_introduction_2();

		//create instance of JaipurGame class
		JaipurGame jg = new JaipurGame(player_one_name_input, player_two_name_input, true, player_one_strategy_num, 
				player_two_strategy_num, 0.58, 0.38, 1.02, 0.58, 0.38, 1.02, 0.58, 0.38, 1.02, 0.58, 0.38, 1.02);
		
		//play first two rounds of Jaipur
		jg.play_round(1);
		jg.play_round(2);
		
		//get number of wins from both players
		player_one_wins = jg.get_player_one_wins();
		player_two_wins = jg.get_player_two_wins();
		
		//get player names
		player_one_name = jg.get_player_one_name();
		player_two_name = jg.get_player_two_name();

		//if player one or two already has two wins, they have won the game
		if (player_one_wins > 1) {
			ti.game_winner(player_one_name, player_two_name, player_one_wins, player_two_wins, jg.get_player_one_r1_final_score(),
					jg.get_player_one_r2_final_score(), jg.get_player_one_r3_final_score(), jg.get_player_two_r1_final_score(), 
					jg.get_player_two_r2_final_score(), jg.get_player_two_r3_final_score());
		}
		
		if (player_two_wins > 1) {
			ti.game_winner(player_one_name, player_two_name, player_one_wins, player_two_wins, jg.get_player_one_r1_final_score(),
					jg.get_player_one_r2_final_score(), jg.get_player_one_r3_final_score(), jg.get_player_two_r1_final_score(), 
					jg.get_player_two_r2_final_score(), jg.get_player_two_r3_final_score());
		}
		
		//else round three is played, and a winner is determined
		if (player_one_wins == 1 && player_two_wins == 1) {
			
			jg.play_round(3);
			
			//get number of wins from both players
			player_one_wins = jg.get_player_one_wins();
			player_two_wins = jg.get_player_two_wins();
			
			ti.game_winner(player_one_name, player_two_name, player_one_wins, player_two_wins, jg.get_player_one_r1_final_score(),
					jg.get_player_one_r2_final_score(), jg.get_player_one_r3_final_score(), jg.get_player_two_r1_final_score(), 
					jg.get_player_two_r2_final_score(), jg.get_player_two_r3_final_score());
		}
	}
	
	//method to find the score difference in a round between two computer players
	public static int find_score_difference(int player_one_strategy_num, int player_two_strategy_num, double p1_selected_hand_k1, double p1_selected_hand_k2, 
			double p1_selected_hand_k3, double p1_selected_market_k1, double p1_selected_market_k2, double p1_selected_market_k3, double p2_selected_hand_k1, 
			double p2_selected_hand_k2, double p2_selected_hand_k3, double p2_selected_market_k1, double p2_selected_market_k2, 
			double p2_selected_market) throws InterruptedException {
		
		//creates a JaipurGame object with two computer players, no printing screen and pre-selected heuristic parameters
		JaipurGame jg = new JaipurGame("Com", "Com", false, player_one_strategy_num, player_two_strategy_num, p1_selected_hand_k1, 
				p1_selected_hand_k2, p1_selected_hand_k3, p1_selected_market_k1, p1_selected_market_k2, p1_selected_market_k3, p2_selected_hand_k1, 
				p2_selected_hand_k2, p2_selected_hand_k3, p2_selected_market_k1, p2_selected_market_k2, p2_selected_market);

		//plays a round of Jaipur
		jg.play_round(1);
		
		int player_one_end_score, player_two_end_score, score_difference;
		
		//collects player's scores after the round is over
		player_one_end_score = jg.get_player_one_r1_final_score();
		player_two_end_score = jg.get_player_two_r1_final_score();

		//score difference is calculated by subtracting player one's score by player two's score
		score_difference = player_one_end_score - player_two_end_score;
		
		return score_difference;
	}
		
	//method to find the score differences, over a pre-selected number of rounds
	public static ArrayList<Double> compare_score_differences(int number_of_rounds, int player_one_strategy_num, 
			int player_two_strategy_num, double p1_selected_hand_k1, double p1_selected_hand_k2, double p1_selected_hand_k3, 
			double p1_selected_market_k1, double p1_selected_market_k2, double p1_selected_market_k3, double p2_selected_hand_k1, 
			double p2_selected_hand_k2, double p2_selected_hand_k3, double p2_selected_market_k1, double p2_selected_market_k2, 
			double p2_selected_market) throws InterruptedException {
		
		double score_difference;
		
		ArrayList<Double> comparison_results = new ArrayList<Double>();

		//loop over how many rounds were pre-selected
		for (int i = 0; i < number_of_rounds; i++) {
			
			//add score difference to the total
			score_difference = find_score_difference(player_one_strategy_num, player_two_strategy_num, p1_selected_hand_k1, 
					p1_selected_hand_k2, p1_selected_hand_k3, p1_selected_market_k1, p1_selected_market_k2, p1_selected_market_k3, 
					p2_selected_hand_k1, p2_selected_hand_k2, p2_selected_hand_k3, p2_selected_market_k1, p2_selected_market_k2, 
					p2_selected_market);
												
			comparison_results.add(score_difference);
		}
		
		//results are sorted in ascending order to clearly see peaks and/or troughs
		Collections.sort(comparison_results);
						
		return comparison_results;
	}
	
	//method to find the total average score difference, over a pre-selected number of rounds
	public static double find_total_average_score_difference(int number_of_rounds, int player_one_strategy_num, int player_two_strategy_num, 
			double p1_selected_hand_k1, double p1_selected_hand_k2, double p1_selected_hand_k3, double p1_selected_market_k1, double p1_selected_market_k2, 
			double p1_selected_market_k3, double p2_selected_hand_k1, double p2_selected_hand_k2, double p2_selected_hand_k3, double p2_selected_market_k1, 
			double p2_selected_market_k2, double p2_selected_market) throws InterruptedException {
		
		double total_average_score_difference, total_score_difference = 0;
		
		//find all score differences, over a pre-selected number of rounds
		ArrayList<Double> score_differences = compare_score_differences(number_of_rounds, player_one_strategy_num, 
				player_two_strategy_num, p1_selected_hand_k1, p1_selected_hand_k2, p1_selected_hand_k3, p1_selected_market_k1, 
				p1_selected_market_k2, p1_selected_market_k3, p2_selected_hand_k1, p2_selected_hand_k2, p2_selected_hand_k3, 
				p2_selected_market_k1, p2_selected_market_k2, p2_selected_market);
			
		//loop over all score differences and add to the total score difference
		for (int i = 0; i < score_differences.size(); i++) {
			
			total_score_difference += score_differences.get(i);
		}
		
		//find the average of the total by dividing the sum of all differences by the number of rounds
		total_average_score_difference = total_score_difference / (double) number_of_rounds;
		
		return total_average_score_difference;
	}


	//method to test the heuristic parameters in strategies 4, 5 and 6 against the basic strategies (1, 2 and 3)
	//(please note, the testing assumes the same parameters are used in both calculating the value of the current player's
	//hand and the market place)
	public static ArrayList<ArrayList<Double>> test_heuristic_parameters(int p1_heuristic_strategy, int p2_basic_strategy, int number_of_steps, 
			int number_of_rounds, double lower_bound_k1, double upper_bound_k1, double lower_bound_k2, double upper_bound_k2, 
			double k3) throws InterruptedException {
		
		double bound_difference_k1, bound_difference_k2, step_size_k1, step_size_k2;
		
		//finding the bound differences and step sizes in parameters k1 and k2
		
		bound_difference_k1 = upper_bound_k1 - lower_bound_k1;
		step_size_k1 = bound_difference_k1 / ((double) number_of_steps - 1);

		bound_difference_k2 = upper_bound_k2 - lower_bound_k2;
		step_size_k2 = bound_difference_k2 / ((double) number_of_steps - 1);

		//calculating the total of iterations per percentage to output when testing 
		double total_iterations = Math.pow(number_of_steps, 3);
		double one_percent = total_iterations / (double) 100;
		double total_percentage = one_percent;
		int iteration_count = 0, percentage_count = 0;
		
		//container to hold all results
		ArrayList<ArrayList<Double>> parameter_results = new ArrayList<ArrayList<Double>>();
				
		//k1 and k2 all start on their lower bounds
		double k1 = lower_bound_k1;
		double k2 = lower_bound_k2;
		
		//flags to raise once the parameters have to be set back to their lower bounds
		boolean k1_flag = false, k2_flag = false;

		//nested looping to find every possible combination given the step size
		for (int i = 0; i < number_of_steps; i++) {
			
			for (int j = 0; j < number_of_steps; j++) {
					
				//outputs the percentage to inform the user how much testing still needs to be done
				if (iteration_count >= total_percentage) {
						
					total_percentage += one_percent;
					percentage_count++;
					System.out.println(percentage_count + "% Complete");
				}
					
				//if k1 flag is raised, it has reached its upper bound and is reset to its lower bound
				if (k1_flag) {
					k1 = lower_bound_k1;
					k1_flag = false;
				}
					
				//iteration count incremented
				iteration_count++;

				//average score difference calculated given the specific parameters in this iteration, the strategies
				//chosen and the number of rounds to find the average
				double average_score_diff = find_total_average_score_difference(number_of_rounds, p1_heuristic_strategy, 
						p2_basic_strategy, k1, k2, k3, k1, k2, k3, 0, 0, 0, 0, 0, 0);
					
				//add information found to a result container
				ArrayList<Double> result = new ArrayList<Double>();
				result.add((double) iteration_count);
				result.add(k1);
				result.add(k2);
				result.add(k3);
				result.add(average_score_diff);
					
				//add result to main list of results
				parameter_results.add(result);

				//k1 step size incremented
				k1 += step_size_k1;
			}
				
			k1_flag = true;
			
			//if k2 flag is raised, it has reached its upper bound and is reset to its lower bound
			if (k2_flag) {
				k2 = lower_bound_k2;
				k2_flag = false;				
			}
				
			//k2 step size incremented
			k2 += step_size_k2;
		}
			
		k2_flag = true;
			
		System.out.println(100 + "% Complete");
		
		return parameter_results;

	}
}